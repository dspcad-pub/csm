Imported/redistributed packages from other sources (outside
the DSPCAD Group).


1. MDPSolve:
This package was downloaded from https://github.com/PaulFackler/MDPSolve/ on Sep
 27 2021.

Set up IXCLAPACK variable `export IXMDPSOLVE=<path to here>/MDPSolve
You only need mdptools/ and mdputils/.
To update the source code to the latest version, run

No need to build the source code.

2. jsonlab:
This package was downloaded from https://github.com/fangq/jsonlab/ on Sep
 27 2021.
Set up IXCLAPACK variable `export IXJSONLAB=<path to here>/IXJSONLAB
To update the source code to the latest version, run

No need to build the source code.
