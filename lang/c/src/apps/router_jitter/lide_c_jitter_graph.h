#ifndef _lide_c_jitter_graph_h
#define _lide_c_jitter_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
/* Overview: This is a graph for jitter application. It creates each actor/fifo
and connect them. Also user can have scheduler for this graph in scheduler
method.
*/
#include "lide_c_fifo.h"
#include "router.h"
#include "lide_c_actor.h"
#include "lide_c_graph.h"
#include "lide_c_jitter_file_source.h"
#include "lide_c_double_file_sink.h"
#include "lide_c_router_double_file_sink.h"
#include "lide_c_FSM_s.h"
#include "lide_c_DVL.h"
#include "lide_c_STR.h"
#include "lide_c_TRT.h"
#include "lide_c_RE.h"
#include "lide_c_RRE.h"
#include "lide_c_CRE.h"
#include "lide_c_Phase.h"
#include "lide_c_util.h"
#include "lide_c_util.h"

#define BUFFER_ARRAY_CAPACITY 262144
#define BUFFER_SCALAR_CAPACITY 262144

/* An enumeration of the actors in this application. */
#define ACTOR_XSOURCE   0
#define ACTOR_DVL       1
#define ACTOR_STR       2
#define ACTOR_FSM_s     3
#define ACTOR_TRT       4
#define ACTOR_RE        5
#define ACTOR_RRE       6
#define ACTOR_CRE       7
#define ACTOR_PHASE     8
#define ACTOR_SINK1     9
#define ACTOR_SINK2    10

/* The total number of actors in the application. */
#define ACTOR_COUNT    11

/* The total number of parameter files in the application. */
#define PARA_COUNT      1

#define ARG_COUNT       5


/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/
/* Structure and pointer types associated with add objects. */
struct _lide_c_jitter_graph_context_struct;
typedef struct _lide_c_jitter_graph_context_struct
        lide_c_jitter_graph_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

lide_c_jitter_graph_context_type* lide_c_jitter_graph_new(char *x_file,char
*out1_file, char *out2_file, char *para_file);

void lide_c_jitter_graph_scheduler(
        lide_c_jitter_graph_context_type *context);

void lide_c_jitter_graph_terminate(
        lide_c_jitter_graph_context_type *context);


#endif
