﻿/******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include "lide_c_jitter_graph.h"
#define MILLION 1000000

struct _lide_c_jitter_graph_context_struct {
#include "lide_c_graph_context_type_common.h"
    char *x_file;
    char *out1_file;
    char *out2_file;
    char *para_file;
    int NUM ;
    int Residue;
    int Ws;
    float Op;
    int trall_num;
    int length;
    int Th;
};

lide_c_jitter_graph_context_type* lide_c_jitter_graph_new(char *x_file,char
*out1_file, char *out2_file, char *para_file) {
    int token_size, i;
    lide_c_jitter_graph_context_type *context = NULL;

    context = lide_c_util_malloc(sizeof(lide_c_jitter_graph_context_type));
    context->actor_count = ACTOR_COUNT;
    context->actors = (lide_c_actor_context_type **) lide_c_util_malloc(
            context->actor_count * sizeof(lide_c_actor_context_type*));

    context->descriptors = (char **) lide_c_util_malloc(
            context->actor_count * sizeof(char *));
    for (i = 0; i < context->actor_count; i++) {
        context->descriptors[i] = (char *) lide_c_util_malloc(
                context->actor_count * sizeof(char));
    }

    context->x_file = x_file;
    context->out1_file = out1_file;
    context->out2_file = out2_file;
    context->para_file = para_file;

    FILE *fp = NULL;
    int input_port_count;
    int output_port_count;
    int index;
    i = 0;
    int j = 0;
    int k = 0;
    context->Ws = 0;
    context->Op = 0.0;
    context->Th=0;

    /*Parameter of windowing*/
    fp=fopen(context->para_file, "r");
    if (!fp) {
        fprintf(stderr, "Parameter txt file could not be opened");
        exit(1);
    }

    if (!fscanf(fp,"%d",&context->length)){
        fprintf(stderr, "Cannot read data length from Parameter.txt\n");
        exit(1);
    }

    if (!fscanf(fp,"%d",&context->Ws)){
        fprintf(stderr, "Cannot read Window size from Parameter.txt\n");
        exit(1);
    }

    if (!fscanf(fp,"%f",&context->Op)){
        fprintf(stderr, "Cannot read overlap of the window from  Parameter"
                ".txt\n");
        exit(1);
    }

    if (!fscanf(fp,"%d",&context->Th)){
        fprintf(stderr, "Cannot read threshold from Parameter.txt\n");
        exit(1);
    }
    fclose(fp);

    /* Connectivity:
                    router1: input data
                    port1i: SRC
                    port10: SRC->DVL: input data
                    port11: SRC->STR: input data
                    port12: SRC->TRT: input data
                    
                    router2: medium
                    port2i: DVL
                    port20: DVL->STR: medium
                    port21: DVL->TRT: medium
                    
                    fifo30: STR->FSM: state array
                    
                    router4:trt_num
                    port4i: FSM
                    port40: FSM->TRT: trt_num
                    port41: FSM->RE: trt_num
                    port42: FSM->RRE: trt_num
                    port43: FSM->CRE: trt_num
                    
                    router5: TRinex array
                    port5i: FSM
                    port50: FSM->TRT: TRindex array
                    port51: FSM->CRE: TRindex array
                    
                    fifo60: TRT->CRE: transition time array
                    
                    router7: TRall array
                    port7i: TRT
                    port70: TRT->RE: TRall array
                    port71: TRT->RRE: TRall array
                    port72: TRT->CRE: TRall array
                    port73: TRT->PHS: TRall array
                    
                    router8: TRall_num
                    port8i: TRT
                    port80: TRT->RE: TRall_num
                    port81: TRT->RRE: TRall_num
                    port82: TRT->CRE: TRall_num
                    port83: TRT->PHS: TRall_num
                    
                    fifo90: RE->RRE: REtime
                    
                    fifo100: RE->RRE: difference array
                    
                    fifo110: RRE->CRE: RREtime
                    
                    router12: CREtime
                    port12i: CRE
                    port120: CRE->PHS: CREtime
                    port121: CRE->SNK1: CREtime
                    
                    fifo130: CRE->PHS: coefficient a

                    fifo140: PHS->SNK2: TIE array
    */

    lide_c_fifo_pointer fifo30 = NULL;
    lide_c_fifo_pointer fifo60 = NULL;
    lide_c_fifo_pointer fifo90 = NULL;
    lide_c_fifo_pointer fifo100 = NULL;
    lide_c_fifo_pointer fifo110 = NULL;
    lide_c_fifo_pointer fifo130 = NULL;
    lide_c_fifo_pointer fifo140 = NULL;

    router_context_pointer router1 = NULL;
    router_port_pointer port1i = NULL;
    router_port_pointer port10 = NULL, port11 = NULL, port12 = NULL;

    router_context_pointer router2 = NULL;
    router_port_pointer port2i = NULL;
    router_port_pointer port20 = NULL, port21 = NULL;

    router_context_pointer router4 = NULL;
    router_port_pointer port4i = NULL;
    router_port_pointer port40 = NULL, port41 = NULL, port42 = NULL, port43 =
            NULL;

    router_context_pointer router5 = NULL;
    router_port_pointer port5i = NULL;
    router_port_pointer port50 = NULL, port51 = NULL;

    router_context_pointer router7 = NULL;
    router_port_pointer port7i = NULL;
    router_port_pointer port70 = NULL, port71 = NULL, port72 = NULL, port73 =
            NULL;

    router_context_pointer router8 = NULL;
    router_port_pointer port8i = NULL;
    router_port_pointer port80 = NULL, port81 = NULL, port82 = NULL, port83 =
            NULL;

    router_context_pointer router12 = NULL;
    router_port_pointer port12i = NULL;
    router_port_pointer port120 = NULL, port121 = NULL;


    double value;

    /* actor descriptors (for diagnostic output) */
    for(i = 0; i < context->actor_count; i++){
        context->descriptors[i] = (char *)lide_c_util_malloc(
                context->actor_count * sizeof(char));
    }
    strcpy(context->descriptors[ACTOR_XSOURCE], "xsource");
    strcpy(context->descriptors[ACTOR_DVL],"dvl");
    strcpy(context->descriptors[ACTOR_STR],"str");
    strcpy(context->descriptors[ACTOR_FSM_s],"fsm");
    strcpy(context->descriptors[ACTOR_TRT],"trt");
    strcpy(context->descriptors[ACTOR_RE],"re");
    strcpy(context->descriptors[ACTOR_RRE],"rre");
    strcpy(context->descriptors[ACTOR_CRE],"cre");
    strcpy(context->descriptors[ACTOR_PHASE],"phase");
    strcpy(context->descriptors[ACTOR_SINK1],"sink1");
    strcpy(context->descriptors[ACTOR_SINK2],"sink2");


    /* Number of windows*/
    context->NUM = (int) ((context->length - context->Ws) / context->Ws /  (1
            - context->Op)) + 1;
    context->Residue = context->length - context->NUM * context->Ws;

    token_size = sizeof(int);
    fifo30 = lide_c_fifo_new(BUFFER_ARRAY_CAPACITY, token_size);

    token_size = sizeof(double);
    fifo60 = lide_c_fifo_new(BUFFER_ARRAY_CAPACITY, token_size);
    token_size = sizeof(double);
    fifo90 = lide_c_fifo_new(BUFFER_SCALAR_CAPACITY, token_size);
    token_size = sizeof(double);
    fifo100 = lide_c_fifo_new(BUFFER_ARRAY_CAPACITY, token_size);
    token_size = sizeof(double);
    fifo110 = lide_c_fifo_new(BUFFER_SCALAR_CAPACITY, token_size);

    token_size = sizeof(double);
    fifo130 = lide_c_fifo_new(BUFFER_SCALAR_CAPACITY, token_size);
    token_size = sizeof(double);
    fifo140 = lide_c_fifo_new(BUFFER_ARRAY_CAPACITY, token_size);

    token_size = sizeof(double);
    input_port_count = 1;
    output_port_count = 3;
    index = 10;
    router1 = router_context_new(BUFFER_ARRAY_CAPACITY, token_size,
            input_port_count, output_port_count, index);
    port1i = router_port_get_router_port_pointer(router1, 0);
    port10 = router_port_get_router_port_pointer(router1, 1);
    port11 = router_port_get_router_port_pointer(router1, 2);
    port12 = router_port_get_router_port_pointer(router1, 3);
    ///////////////////////////////////////////////////////////////////////////
    //printf("11\n");
    token_size = sizeof(double);
    input_port_count = 1;
    output_port_count = 2;
    index = 20;
    router2 = router_context_new(BUFFER_SCALAR_CAPACITY, token_size,
            input_port_count, output_port_count, index);
    port2i = router_port_get_router_port_pointer(router2, 0);
    port20 = router_port_get_router_port_pointer(router2, 1);
    port21 = router_port_get_router_port_pointer(router2, 2);
    ///////////////////////////////////////////////////////////////////////////
    //printf("12\n");
    token_size = sizeof(int);
    input_port_count = 1;
    output_port_count = 4;
    index = 40;
    router4 = router_context_new(BUFFER_SCALAR_CAPACITY, token_size,
            input_port_count, output_port_count, index);
    port4i = router_port_get_router_port_pointer(router4, 0);
    port40 = router_port_get_router_port_pointer(router4, 1);
    port41 = router_port_get_router_port_pointer(router4, 2);
    port42 = router_port_get_router_port_pointer(router4, 3);
    port43 = router_port_get_router_port_pointer(router4, 4);
    ///////////////////////////////////////////////////////////////////////////
    //printf("13\n");
    token_size = sizeof(int);
    input_port_count = 1;
    output_port_count = 2;
    index = 50;
    router5 = router_context_new(BUFFER_ARRAY_CAPACITY, token_size,
            input_port_count, output_port_count, index);
    port5i = router_port_get_router_port_pointer(router5, 0);
    port50 = router_port_get_router_port_pointer(router5, 1);
    port51 = router_port_get_router_port_pointer(router5, 2);
    ///////////////////////////////////////////////////////////////////////////
    //printf("14\n");
    token_size = sizeof(double);
    input_port_count = 1;
    output_port_count = 4;
    index = 70;
    router7 = router_context_new(BUFFER_ARRAY_CAPACITY, token_size,
            input_port_count, output_port_count, index);

    port7i = router_port_get_router_port_pointer(router7, 0);
    port70 = router_port_get_router_port_pointer(router7, 1);
    port71 = router_port_get_router_port_pointer(router7, 2);
    port72 = router_port_get_router_port_pointer(router7, 3);
    port73 = router_port_get_router_port_pointer(router7, 4);
    ///////////////////////////////////////////////////////////////////////////
    printf("15\n");
    token_size = sizeof(int);
    input_port_count = 1;
    output_port_count = 4;
    index = 80;
    router8 = router_context_new(BUFFER_SCALAR_CAPACITY, token_size,
            input_port_count, output_port_count, index);
    //printf("151\n");
    port8i = router_port_get_router_port_pointer(router8, 0);
    port80 = router_port_get_router_port_pointer(router8, 1);
    port81 = router_port_get_router_port_pointer(router8, 2);
    port82 = router_port_get_router_port_pointer(router8, 3);
    port83 = router_port_get_router_port_pointer(router8, 4);
    ///////////////////////////////////////////////////////////////////////////
    printf("16\n");
    token_size = sizeof(double);
    input_port_count = 1;
    output_port_count = 2;
    index = 120;
    router12 = router_context_new(BUFFER_SCALAR_CAPACITY, token_size,
            input_port_count, output_port_count, index);
    port12i = router_port_get_router_port_pointer(router12, 0);
    port120 = router_port_get_router_port_pointer(router12, 1);
    port121 = router_port_get_router_port_pointer(router12, 2);

    ///////////////////////////////////////////////////////////////////////////
    //printf("2\n");
    /* Create and connect the actors. */
    k = 0;
    context->actors[ACTOR_XSOURCE] = (lide_c_actor_context_type* )(
            lide_c_jitter_file_source_new(context->x_file, port1i));
    printf("20\n");
    context->actors[ACTOR_DVL] = (lide_c_actor_context_type* )(
            lide_c_DVL_new(context->Ws, context->Op, context->Th, port10,
            port2i));
    //printf("21\n");
    context->actors[ACTOR_STR] = (lide_c_actor_context_type* )(
            lide_c_STR_new(context->Ws, port11, port20, fifo30));
    //printf("22\n");
    context->actors[ACTOR_FSM_s] = (lide_c_actor_context_type* )(
            lide_c_FSM_s_new(context->Ws, fifo30, port4i, port5i));
    //printf("23\n");
    context->actors[ACTOR_TRT] = (lide_c_actor_context_type* )(
            lide_c_TRT_new(context->Ws, port12, port21, port40, port50, fifo60,
                    port7i, port8i));
    //printf("24\n");
    context->actors[ACTOR_RE] = (lide_c_actor_context_type* )(
            lide_c_RE_new(context->Ws, port41, port70, port80, fifo90, fifo100)
                    );
    //printf("25\n");
    context->actors[ACTOR_RRE] = (lide_c_actor_context_type* )(
            lide_c_RRE_new(context->Ws, port42, port71, port81, fifo90, fifo100,
                    fifo110));
    //printf("26\n");
    context->actors[ACTOR_CRE] = (lide_c_actor_context_type* )(
            lide_c_CRE_new(context->Ws, k, port43, port51, fifo60, port72,
                    port82, fifo110, port12i, fifo130));
    //printf("27\n");
    context->actors[ACTOR_PHASE] = (lide_c_actor_context_type* )(
            lide_c_Phase_new(context->Ws, context->NUM, port73, port83, port120,
                    fifo130, fifo140));
    //printf("28\n");
    context->actors[ACTOR_SINK1] = (lide_c_actor_context_type* )
            (lide_c_router_double_file_sink_new(context->out1_file, port121));
    //printf("29\n");
    context->actors[ACTOR_SINK2] = (lide_c_actor_context_type * )
            (lide_c_double_file_sink_new(context->out2_file, fifo140));

    return context;
}

void lide_c_jitter_graph_scheduler(
        lide_c_jitter_graph_context_type *context){
    int k,i,j;
    for (k = 0; k<context->NUM; k++) {

        i = ACTOR_XSOURCE;
        for (j = 0; j < (int) (context->Ws * (1 - context->Op)); j++) {
            lide_c_util_guarded_execution(context->actors[i],
                    context->descriptors[i]);
        }
        i = ACTOR_DVL;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        i = ACTOR_STR;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        i = ACTOR_FSM_s;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        i = ACTOR_TRT;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        i = ACTOR_RE;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        i = ACTOR_RRE;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        i = ACTOR_CRE;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        i = ACTOR_PHASE;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);
        i = ACTOR_SINK1;
        lide_c_util_guarded_execution(context->actors[i],
                context->descriptors[i]);

        context->trall_num = lide_c_TRT_get_trall_num(
                (lide_c_TRT_context_type *) context->actors[ACTOR_TRT]);

        i = ACTOR_SINK2;
        for (j = 0; j < context->trall_num; j++) {
            lide_c_util_guarded_execution(context->actors[i],
                    context->descriptors[i]);
        }
    }
        return;
}

void lide_c_jitter_graph_terminate(
        lide_c_jitter_graph_context_type *context){
    /* Terminate*/
    lide_c_jitter_file_source_terminate(
            (lide_c_jitter_file_source_context_type *)
            (context->actors[ACTOR_XSOURCE]));
    lide_c_DVL_terminate(
            (lide_c_DVL_context_type *)(context->actors[ACTOR_DVL]));
    lide_c_STR_terminate(
            (lide_c_STR_context_type *)(context->actors[ACTOR_STR]));
    lide_c_FSM_s_terminate(
            (lide_c_FSM_s_context_type *)(context->actors[ACTOR_FSM_s]));
    lide_c_TRT_terminate(
            (lide_c_TRT_context_type *)(context->actors[ACTOR_TRT]));
    lide_c_RE_terminate(
            (lide_c_RE_context_type *)(context->actors[ACTOR_RE]));
    lide_c_RRE_terminate(
            (lide_c_RRE_context_type *)(context->actors[ACTOR_RRE]));
    lide_c_CRE_terminate(
            (lide_c_CRE_context_type *)(context->actors[ACTOR_CRE]));
    lide_c_Phase_terminate(
            (lide_c_Phase_context_type *)(context->actors[ACTOR_PHASE]));
    lide_c_double_file_sink_terminate(
            (lide_c_double_file_sink_context_type *)
            (context->actors[ACTOR_SINK1]));
    lide_c_double_file_sink_terminate(
            (lide_c_double_file_sink_context_type *)
            (context->actors[ACTOR_SINK2]));
    return;
}
