#ifndef _buffer_h
#define _buffer_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include "welt_basic.h"
#include "router.h"
#include "welt_c_fifo.h"
#include "welt_c_actor.h"
#include <stdbool.h>

typedef struct _router_buffer_struct router_buffer_type;

struct _router_buffer_struct {
    
    void *buffer_start;
    void *buffer_end ;   //points to the last byte in the buffer
    void *read_pointer ;
    void *write_pointer ;
    int capacity ;
    int token_size ;
    int population ;
    int buffer_size;
    // router_port_pointer port;
    
};


typedef router_buffer_type *router_buffer_pointer;

typedef int (*router_port_get_index_ftype) (router_port_pointer port);
     
typedef void (*router_bufwrite_ftype)
(router_buffer_pointer buffer, void *d);

typedef void (*router_bufread_ftype) (router_buffer_pointer buffer, void *d);

typedef void (*router_bufpeek_ftype) (router_buffer_pointer buffer);

router_buffer_pointer router_buffer_new
( int capacity, int token_size);

void * router_bufpeek( router_buffer_pointer buffer);
void router_bufread( router_buffer_pointer buffer , void * d);
void router_bufread_block( router_buffer_pointer buffer,int size,
        void * d);
void router_bufwrite( router_buffer_pointer buffer, void * d);
void router_bufwrite_block( router_buffer_pointer buffer,
        int size, void * d);
void router_bufadvance (router_buffer_pointer buffer);

#endif
