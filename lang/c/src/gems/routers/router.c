/****************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "welt_c_util.h"
#include "router.h"

/*****************************************************************************
Public functions.
*****************************************************************************/

/* router port construct function */
/*  input port label:INPUT_PORT --> 0; 
    output port label: OUTPUT_PORT --> 1 */
router_port_pointer router_port_new(
                                        router_context_pointer router,
                                        int group_label, int group_idx, 
                                        int index){
    router_port_type *port = NULL;

    port = welt_c_util_malloc(sizeof(router_port_type));
    port->group_label = group_label;
    port->group_idx = group_idx;
    port->population = 0;
    port->capacity = router->capacity;
    port->index = index + router->input_ports_count * port->group_label + 
                    port->group_idx;
    port->router_context = router;
    port->get_context = router_port_get_router_context;
    port->get_port_pop = router_port_population;
    // port-> pop1=router_port_population;
    port->get_port_index = router_port_get_index;
    return port;
}


router_context_pointer router_context_new(int capacity,
                                                        int token_size,
                                                        int input_ports_count,
                                                        int output_ports_count,
                                                        int index){
    int i;
    int tmp;
    
    router_context_pointer context = NULL;
    
    if (sizeof(char) != 1) {
        fprintf(stderr, "incompatible target platform\n");
    }

    if (capacity < 1) {
        return NULL;
    }
    if (token_size <= 0) {
        return NULL;
    }

    context = (router_context_pointer)welt_c_util_malloc(
                sizeof(router_context_type));
    context->population = 0;
    context->capacity = capacity;
    context->token_size = token_size;
    context->input_ports_count = input_ports_count;
    context->output_ports_count = output_ports_count;
    context->ports_count = context->input_ports_count + 
                            context->output_ports_count;
    context->index = index;
    
    /* Memory initialization*/
    context->buffer_start = NULL;
    context->buffer_start = welt_c_util_malloc((size_t)(context->capacity *
                                                context->token_size));
    if (context->buffer_start == NULL){
        free(context);
        return NULL;
    }
    context->buffer_end = ((char*)context->buffer_start) + 
            ((context->capacity - 1) * context->token_size);
    
    /*Read_pointer initialization*/
    context->read_pointer = NULL;
    /* all ports would read data in the router*/
    context->read_pointer = welt_c_util_malloc(sizeof(void *) *
                            context->output_ports_count);
    //context->read_pointer = welt_c_util_malloc(sizeof(void *) *
    //                        context->ports_count);
    
    /*port initialization*/
    context->ports = NULL;
    context->ports = welt_c_util_malloc(sizeof(router_port_pointer)*
                                        context->ports_count);
    /* input port initialization*/
    for (i = 0; i < context->input_ports_count; i++){
        context->ports[i] = router_port_new(context, INPUT_PORT, i,
                                                context->index);
    }
    /* output port initialization*/
    for (i = context->input_ports_count; i < context->ports_count; i++){
        tmp = i - context->input_ports_count;
        context->ports[i] = router_port_new(context, OUTPUT_PORT, tmp,
                                                context->index);
    }

    /* methods */
    context->print = NULL;
    context->read = router_read;
    context->peek = router_peek;
    context->read_advance = router_read_advance;
    context->read_block = router_read_block;
    context->write = router_write;
    context->write_block = router_write_block;
    context->get_pop = router_population;
    //context->get_port_pop = router_port_population;
    context->get_cap = router_capacity;
    context->get_token_size = router_token_size;
    context->reset = router_reset;
    context->free = router_free;
    //context->get_port_index = router_port_get_index;
    context->get_port_withidx = router_port_get_router_port_pointer;
    context->get_port = router_get_port;
    //context->get_context = router_port_get_router_context;
    
    context->reset(context);
    return context;
}


void router_reset(router_context_pointer context){
    int i = 0;
    context->population = 0;
    context->write_pointer = context->buffer_start;
    //for (i = 0; i<context->ports_count; i++){
    for (i = 0; i<context->ports_count - context->input_ports_count; i++){
        context->read_pointer[i] = context->buffer_start;
    }
    return;
}

void router_set_token_printing(router_context_pointer router,
        router_f_token_printing f) {
    router->print = f;
    return;
}

void router_free(router_context_pointer router) {
    int i;
    if (router == NULL) {
        return;
    }
    if (router->buffer_start != NULL) {
        free(router->buffer_start);
    }
    if (router->read_pointer != NULL) {
        free(router->read_pointer);
    }

    if (router->ports != NULL) {
        for (i = 0; i < router->ports_count; i++){
            if(router->ports[i] != NULL)
                free(router->ports[i]);
        }
        free(router->ports);
    }   
    free(router);
    return;
}


int router_population(router_context_pointer router) {
    return router->population;
}

int router_capacity(router_context_pointer router) {
    return router->capacity;
}

int router_token_size(router_context_pointer router) {
    return router->token_size;
}


void router_read(router_port_pointer port, void *data){
    
    int index;
    int i,pop;
    router_context_pointer router;

    router = port->router_context;
    
    /*check whether it is an output port*/
    if (port->group_label == 0) {
        fprintf(stderr, "router read err: It is not output port in the \
                        router.\n");
        return;
    }  
    
    /* Make sure the router's related port is not empty. */
    if (router->population == 0 || port->population == 0) {
        fprintf(stderr, "router read err: population in the router is 0.\n");
        return;
    }

    /* index in the ports*/
    index = port->group_idx;
    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, router->read_pointer[index], router->token_size);
    /* Advance the read pointer, using a circular buffering convention. */
    if (router->read_pointer[index] == router->buffer_end) {
        router->read_pointer[index] = router->buffer_start;
    } else {
        router->read_pointer[index] = 
                ((char*)router->read_pointer[index]) + router->token_size;
    }

    /* Update the token count. */
    port->population--; 
    pop = 0;
    for(i = router->input_ports_count; i < router->ports_count; i++){
        if (router->ports[i]->population > pop){
            pop = router->ports[i]->population;
        }
    }
    if(router->population != pop){
        router->population = pop;
        for(i = 0; i < router->input_ports_count; i++){
            router->ports[i]->population = pop;
        }
    }
    return;
}

void router_read_block(router_port_pointer port, void *data,
                            int size){
    int index;
    int i,pop;
    /* Room (number of token spaces) "in front of the read pointer" within
    the buffer. */
    int room_in_front = 0;

    /* Breakdown in bytes of the amounts of data to read before and after,
    respectively, the read pointer wraps around the end of the buffer. */
    int part1_size = 0;
    int part2_size = 0;

    /* A cache of the FIFO token size. */
    int token_size = 0;
    
    router_context_pointer router;
    router = port->router_context;
    
    /*check whether it is an output port*/
    if (port->group_label == 0) {
        fprintf(stderr, "router read err: It is not output port.\n");
        return;
    } 
    
    /* Make sure the router's related port is not empty. */
    //if (router->population < size || port->population < size) {
    if (port->population < size) {
        //printf("router->population %d ,size:%d", router->population,size);
        fprintf(stderr, "router read err: population in the router is far here\
        from enough.\n");
        return;
    }

     
    
    /* index in the ports*/
    index = port->group_idx;

    token_size = router->token_size;
    room_in_front = ((((char*) (router->buffer_end)) - 
            ((char*)(router->read_pointer[index]))) / token_size) + 1;
    if (room_in_front >= size) {
        part1_size = size * token_size;
        part2_size = 0;
    } else {
        part1_size = room_in_front * token_size;
        part2_size = (size - room_in_front) * token_size; 
    }
    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, router->read_pointer[index], part1_size);
    if (part2_size > 0) {
        data = ((char*)data) + part1_size;
        memcpy(data, router->buffer_start, part2_size);
        router->read_pointer[index] = ((char*)router->buffer_start) + 
                                        part2_size;
    } else {
        if (size == room_in_front) {
            router->read_pointer[index] = router->buffer_start;
        } else {
            router->read_pointer[index] = 
                    ((char*)router->read_pointer[index]) + part1_size;
        }
    }
    /* Update Population for port and router */
    port->population -= size;
    //printf("router pop decreased size %d \n",port->population);
    pop = 0;
    for(i = router->input_ports_count; i < router->ports_count; i++){
        if (router->ports[index]->population > pop){
            pop = router->ports[index]->population;
        }
    }
    if(router->population != pop){
        router->population = pop;
        for(i = 0; i < router->input_ports_count; i++){
            router->ports[index]->population = pop;
        }
    }
    return;
}

void router_peek(router_port_pointer port, void *data){
    
    int index;
    router_context_pointer router;

    router = port->router_context;
    
    /*check whether it is an output port*/
    if (port->group_label == 0) {
        fprintf(stderr, "router read err: It is not output port in the \
                        router.\n");
        return;
    }  
    
    /* Make sure the router's related port is not empty. */
    if (router->population == 0 || port->population == 0) {
        fprintf(stderr, "router read err: population in the router is 0.\n");
        return;
    }

    /* index in the ports*/
    index = port->group_idx;
    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, router->read_pointer[index], router->token_size);
    
    return;
}


void router_read_advance(router_port_pointer port){
    
    int index;
    int i,pop;
    router_context_pointer router;

    router = port->router_context;
    
    /*check whether it is an output port*/
    if (port->group_label == 0) {
        fprintf(stderr, "router read err: It is not output port in the \
                        router.\n");
        return;
    }  
    
    /* Make sure the router's related port is not empty. */
    if (router->population == 0 || port->population == 0) {
        fprintf(stderr, "router read err: population in the router is 0.\n");
        return;
    }

    /* index in the ports*/
    index = port->group_idx;
    
    /* Advance the read pointer, using a circular buffering convention. */
    if (router->read_pointer[index] == router->buffer_end) {
        router->read_pointer[index] = router->buffer_start;
    } else {
        router->read_pointer[index] = 
                ((char*)router->read_pointer[index]) + router->token_size;
    }

    /* Update the token count. */
    port->population--; 
    pop = 0;
    for(i = router->input_ports_count; i < router->ports_count; i++){
        if (router->ports[i]->population > pop){
            pop = router->ports[i]->population;
        }
    }
    if(router->population != pop){
        router->population = pop;
        for(i = 0; i < router->input_ports_count; i++){
            router->ports[i]->population = pop;
        }
    }
    return;
}

void router_write(router_port_pointer port, void *data){
    
    int i;
    router_context_pointer router;
    router = port->router_context;
    
    /*check whether it is an input port*/
    if (port->group_label == 1) {
        fprintf(stderr, "router read err: It is not input port.\n");
        return;
    } 

    /* Make sure the router is not full. */
    if (router->population >= router->capacity || 
        port->population >= router->capacity) {
        return;
    } 
    
    /* The following assumes that sizeof(char) = 1. */
    memcpy(router->write_pointer, data, router->token_size);
    //printf("%d is written to router\n",*(int *)data);
    /* Advance the read pointer, using a circular buffering convention. */
    if (router->write_pointer == router->buffer_end) {
        router->write_pointer = router->buffer_start;
    } else {
        router->write_pointer = 
                ((char*)router->write_pointer) + router->token_size;
    }
    /* Update the token count. */
    router->population++;  
    for(i = 0; i<router->ports_count; i++){
        router->ports[i]->population++;
    }    
}

void router_write_block(router_port_pointer port, void *data,
                            int size){

    int i;
    void *tmp_data;
    /* Room (number of token spaces) "in front of the write pointer" within 
    the buffer.*/
    int room_in_front = 0;

    /* Breakdown in bytes of the amounts of data to write before and after,
    respectively, the write pointer wraps around the end of the buffer.*/
    int part1_size = 0;
    int part2_size = 0;
    /* A cache of the FIFO token size. */
    int token_size = 0;
    
    router_context_pointer router;
    router = port->router_context;
    token_size = router->token_size;
    
    /*check whether it is an input port*/
    if (port->group_label == 1) {
        fprintf(stderr, "router read err: It is not input port.\n");
        return;
    } 
    
    /* Make sure the router is not full. */
    if ((router->population + size > router->capacity) || 
        (port->population +  size > router->capacity)) {
        return;
    } 
    
      
    /* The following assumes that sizeof(char) = 1. */
    room_in_front = ((((char*) (router->buffer_end)) - 
            ((char*)(router->write_pointer))) / token_size) + 1;

    if (room_in_front >= size) {
        part1_size = size * router->token_size;
        part2_size = 0;
    } else {
        part1_size = room_in_front * router->token_size;
        part2_size = (size - room_in_front) * router->token_size; 
    }
    /* The following assumes that sizeof(char) = 1. */
    memcpy(router->write_pointer, data, part1_size);
    
    if (part2_size > 0) {
        tmp_data = ((char*)data) + part1_size;
        memcpy(router->buffer_start, tmp_data, part2_size);
        router->write_pointer = ((char*)router->buffer_start) + part2_size;
    } else {
        if (size == room_in_front) {
            router->write_pointer = router->buffer_start;
        } else {
            router->write_pointer = ((char*)router->write_pointer) + part1_size;
        }
    }

    /* Update the token count. */
    router->population += size;  
    for(i = 0; i<router->ports_count; i++){
        router->ports[i]->population += size;
    }    
    
}



router_port_pointer router_get_port(
                        router_context_pointer router,
                        int group_label, int group_idx){
    int port_index;
    port_index = router->input_ports_count * group_label + group_idx;
    return router->ports[port_index];
}

int router_port_population(router_port_pointer port){
    return port->population;
}

router_context_pointer router_port_get_router_context(
                                            router_port_pointer port){
    return port->router_context;
}

router_port_pointer router_port_get_router_port_pointer(
                                        router_context_pointer router,
                                        int index){
    if(index < router -> ports_count && index >= 0){
        return router->ports[index];
    }else{
        fprintf(stderr, "port index is out of range.\n");
        return NULL;
    }
        
}

int router_port_get_index(router_port_pointer port){
    return port->index;
}
