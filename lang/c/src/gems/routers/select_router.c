/******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "welt_c_util.h"
#include "select_router.h"
#include <math.h>

#define min(a, b) ((a) < (b)) ? (a) : (b)

#define POINTER_TRUE    0
#define POINTER_FALSE    1
#define DATA    2

#define SELECT_INPUT_PORT 0
#define SELECT_CONTROL_PORT 0
#define SELECT_TRUE_PORT 1
#define SELECT_FALSE_PORT 2
#define SELECT_OUTPUT_PORT 3


struct _select_router_context_struct {
    /* The number of tokens that all the buffers can hold. */
    int capacity;
  
    router_buffer_pointer data[DATA];
    
    router_buffer_pointer output_buffer;
    
    router_buffer_pointer control_buffer;

    /*  The number of bytes in a single token. */
    int token_size;

    /* router port array*/
    select_router_port_pointer *ports;

    /* input_ports_count*/
    int input_ports_count;

    /* output_ports_count*/
    int output_ports_count;

    /* port counts*/
    int ports_count;

    /* index of the router in a dataflow graph. It is flexible and set by the 
    designer*/
    int index;
    
    /* methods in router*/
    select_router_read_ftype read;

    select_router_read_block_ftype read_block;
    select_router_write_ftype write;
    select_router_free_ftype free;
    select_router_get_select_router_port_ftype get_port;

};

struct _select_router_port_struct {
 
    /* label for input or output port group*/
    int group_label;    
    /* index in the group*/
    int group_idx;    
    /* index in the router */    
    int index;   
    /* Port's current "population" for every port*/    
    int population;
    /* Router's current "capacity" for every port*/    
    int capacity;   
    
    /* Router context */
    select_router_context_pointer  select_router_context;
    
    /* Get context*/
    select_router_port_get_select_router_context_ftype get_context;
    select_router_population_ftype pop;
    select_router_port_get_index_ftype get_port_index;
    select_router_port_freespace_ftype fsp;
    
};



/*****************************************************************************
Public functions.
*****************************************************************************/

/* select_router port construct function */
/*  input port label:INPUT_PORT --> 0; 
output port label: OUTPUT_PORT --> 1 */
select_router_port_pointer select_router_port_new(
        select_router_context_pointer select_router,
        int group_label, int group_idx, int index){
    select_router_port_type *port = NULL;

    port = welt_c_util_malloc(sizeof(select_router_port_type));
    port->group_label = group_label;
    port->group_idx = group_idx;
    port->population = 0;
    port->capacity = select_router->capacity;
    port->index = index + select_router->input_ports_count * port->group_label + 
            port->group_idx;
    port->select_router_context = select_router;
    port->get_context = select_router_port_get_select_router_context;
    //port->pop = select_router_port_population;
    port->get_port_index = select_router_port_get_index;
    port->fsp = select_router_freespace;

    return port;
}

select_router_context_pointer select_router_context_new(
        int capacity, int token_size,int input_ports_count,
        int output_ports_count, int index){
    int i;
    int tmp;
    
    select_router_context_pointer context = NULL;
    
    if (capacity < 1) {
        return NULL;
    }
    if (token_size <= 0) {
        return NULL;
    }

    context = (select_router_context_pointer)welt_c_util_malloc(
            sizeof(select_router_context_type));
    
    context->capacity = capacity;
    
    context->token_size = token_size;
    
    context->input_ports_count = input_ports_count;
    context->output_ports_count = output_ports_count;
    context->ports_count = context->input_ports_count + 
            context->output_ports_count;
    context->index = index;
    
    /*port initialization*/
    context->ports = NULL;
    context->ports = welt_c_util_malloc(sizeof
            (select_router_port_pointer)* context->ports_count);
    
    /* input port initialization*/
    /* control port */
    i = 0;
    context->ports[i] = select_router_port_new(context, SELECT_INPUT_PORT, i,
            context->index );
    
    for (i = 1; i < context->input_ports_count; i++){
        context->ports[i] = select_router_port_new(context,
                SELECT_INPUT_PORT + i, i, context->index);
    }
    
    /* output port initialization*/
    for (i = context->input_ports_count; i < context->ports_count; i++){
        tmp = i - context->input_ports_count;
        context->ports[i] = select_router_port_new(context,SELECT_OUTPUT_PORT,
                tmp, context->index);
    }
    
    
    context->data[POINTER_TRUE] = router_buffer_new (
            context->capacity, context-> token_size);
    
    context->data[POINTER_FALSE] = router_buffer_new (
            context->capacity, context-> token_size);

    context->output_buffer = router_buffer_new (
            context->capacity, context-> token_size);
    
  
    context->control_buffer = router_buffer_new (
            context->capacity, context-> token_size);
    
    /* methods */

    context->read = (select_router_read_ftype)select_router_read;

    context->write = (select_router_write_ftype)select_router_write;
 
    return context;
}


void select_router_read(select_router_context_pointer select_router ,
        select_router_port_pointer port, void *d){
    // printf("\n (SELECT READ)\n");
    router_bufread(select_router->output_buffer, d);
}

void process_control_tokens(select_router_context_pointer select_router){
    void * v;
    void * w;
    void * z;
    void * d;
    //printf("\n Processing control tokens\n");
    /*Check if there is available control token*/
    v = router_bufpeek(select_router->control_buffer);
    //printf("\n Processing control tokens :  control\n");
    if ( v != NULL){
        /*if there is available control token, see if there is available token
        in t/f buffer*/
        //printf("\n Processing control tokens :  v notnull\n");
    w = router_bufpeek(select_router->data[(*(int*)v)]);
        //printf("\n Processing control tokens :peek\n");
    }
    while(( v != NULL) && w!= NULL) {
        /* move pointer in control buffer*/
        //printf("\n Processing control tokens :ad\n");

        router_bufadvance(select_router->control_buffer);
        /* read the data from the t/f buffer*/
        router_bufread(select_router->data[(*(int*)v)], &d);
        //printf("\n Processing control tokens :read\n");
        /* write the data to the output buffer*/
        router_bufwrite(select_router->output_buffer, &d);
        select_router->ports[select_router->input_ports_count]->population++;
        //printf("\n Processing control tokens :write\n");
        /*check next available control token*/
        v = router_bufpeek(select_router->control_buffer);
        if (v != NULL){
            //printf("\n v null \n");
            w = router_bufpeek(select_router->data[(*(int*)v)]);
        }
    }
    return;
}


void select_router_write( select_router_context_pointer select_router,
        select_router_port_pointer port, void *d){
   
    int z= 0;
    z = port->group_idx; /* Input index */
    int p1 ; //value
    int * v =NULL; //peek
    int * w = NULL;
    //printf("\n (SELECT WRITE) \n");
    //printf("select_router->output_buffer %p",select_router->output_buffer );
    if (port->group_idx!=0){    //when it is not a control
        z = z - 1;
        v = router_bufpeek(select_router->control_buffer);
        if ( v  == NULL ){
            /* if there is no control token available, store it to t/f buffer */
            router_bufwrite( select_router->data[z] , d);
            //printf("\n no control available , written to %d buffer!!\n",z);
        }else{
            /* control available */
            router_bufread(select_router->control_buffer, &p1);
        
            if ( (z) == (p1) ){
                /* write data to directly to the output buffer */
                router_bufwrite(select_router->output_buffer , d);
                select_router->ports[select_router->input_ports_count]
                        ->population++;

            } else{
                /* if there is control token available, but different, put the
                input in t/f buffer */
                router_bufwrite(select_router-> data[z] , d);
            }
            
        }
        
        process_control_tokens(select_router);
        //printf("\n PROCESSING END \n");
        
    }else{
        /* write to control input */
        router_bufwrite(select_router->control_buffer, d);

    }
    
    process_control_tokens(select_router);
    //printf("\n PROCESSING END2 \n");
    
    return;
    
}

select_router_port_pointer select_router_get_port(
        select_router_context_pointer select_router, int group_label, int
        group_idx){
    int port_index;
    port_index = select_router->input_ports_count * group_label + group_idx;
    return select_router->ports[port_index];
}

select_router_context_pointer
        select_router_port_get_select_router_context(
        select_router_port_pointer port){
            
    return port->select_router_context;
}

/* get router port from router context index==0 control, index==1 true port,
index==2 false port, index==3 output buffer */

select_router_port_pointer
        select_router_port_get_select_router_port_pointer(
        select_router_context_pointer select_router, int index){
    if(index < select_router -> ports_count && index >= 0){
        return select_router->ports[index];
    }else{
        fprintf(stderr, "port index is out of range.\n");
        return NULL;
    }
        
}

int select_router_port_population (
        select_router_port_pointer port){
    select_router_context_pointer select_router;
    select_router = port->select_router_context;
    return select_router->output_buffer->population ;
}

int select_router_port_get_index(select_router_port_pointer port){
    return port->index;
}

int select_router_freespace(select_router_port_pointer port){
    
    if (port->group_idx==1){
        if (port->select_router_context->data[SELECT_TRUE_PORT-1]->capacity
                - (port->select_router_context)-> data[SELECT_TRUE_PORT-1]
                ->population > port->select_router_context->
                output_buffer->population){
            return port->select_router_context->
                    output_buffer->population;
        }else{
            return port->select_router_context->data[SELECT_TRUE_PORT-1]
                    ->capacity- (port->select_router_context)->
                    data[SELECT_TRUE_PORT-1]->population;
        }
        /*return min(port->select_router_context->data[SELECT_TRUE_PORT]->
        capacity
        - (port->select_router_context)-> data[SELECT_TRUE_PORT]
        ->population, port->select_router_context->
        output_buffer->population); */
    }else{
        if (((port->select_router_context)->data[SELECT_TRUE_PORT-1])->capacity
                - (port->select_router_context)-> data[SELECT_TRUE_PORT-1]
                ->population > port->select_router_context->
                output_buffer->population){
            return port->select_router_context->
                    output_buffer->population;
        }else{
            return ((port->select_router_context)->data[SELECT_FALSE_PORT-1])
                    -> capacity
                    - (port->select_router_context)-> data[SELECT_FALSE_PORT-1]
                    ->population;
        }
        /*return min(port->select_router_context->data[SELECT_FALSE_PORT]->
        capacity - (port->select_router_context)-> data[SELECT_FALSE_PORT]
        ->population, port->select_router_context->
        output_buffer->population);*/
    }
    
}
int select_router_capacity (select_router_context_pointer select_router) {
    return select_router->capacity;
}
