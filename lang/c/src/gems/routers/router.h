#ifndef _router_h
#define _router_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include "welt_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_actor.h"

#define INPUT_PORT      0
#define OUTPUT_PORT     1


/*****************************************************************************
Each data item in the router is referred to as a "token". Tokens can have
arbitrary types associated with them. For a given router instance, there
is a fixed token size (number of bytes per token). Tokens have arbitrary
types --- e.g., they can be integers, floating point values (float or
double), characters, or pointers (to any kind of data). This organization
allows for flexibility in storing different kinds of data values, 
and efficiency in storing the data values directly (without being
encapsulated in any sort of higher-level "token" object).

actor-->input port--->|_|_|_|_|_|_|-->output port-->actor

*****************************************************************************/


/*****************************************************************************
* Router as a connector
*****************************************************************************/
// Router context
typedef struct _router_context_struct router_context_type;
// A pointer to a router context.
typedef router_context_type *router_context_pointer;

// Router port
typedef struct _router_port_struct router_port_type;

// A pointer to a router port.
typedef router_port_type *router_port_pointer;

/*****************************************************************************
A pointer to a "router_f_token_printing", which is a function
that prints router's basic information. 
*****************************************************************************/
typedef void (*router_f_token_printing)(FILE *output, void *token);

/*****************************************************************************
A pointer to a "router_read", which is a function that reads
data from router.
*****************************************************************************/
typedef void (*router_read_ftype)
        (router_port_pointer port, void *data);

/*****************************************************************************
A pointer to a "router_peek", which is a function that peek
data from router.
*****************************************************************************/
typedef void (*router_peek_ftype)
        (router_port_pointer port, void *data);


/*****************************************************************************
A pointer to a "router_read_advance", which is a function that update
data population without reading data in router.
*****************************************************************************/
typedef void (*router_read_advance_ftype)
        (router_port_pointer  port);
        
/*****************************************************************************
A pointer to a "router_read_block", which is a function that reads a
block of data from router
*****************************************************************************/
typedef void (*router_read_block_ftype)
        (router_port_pointer port, void *data, int size);
        
/*****************************************************************************
A pointer to a "router_write_function", which is a function that writes
data to the router
*****************************************************************************/
typedef void (*router_write_ftype)
        (router_port_pointer port, void *data);

/*****************************************************************************
A pointer to a "router_write_advance", which is a function that writes
data to the router 
*****************************************************************************/
typedef void (*router_write_advance_ftype)
        (router_port_pointer port);

/*****************************************************************************
A pointer to a "router_write_block", which is a function that reads a
block of data to the router
*****************************************************************************/
typedef void (*router_write_block_ftype)
        (router_port_pointer port, void *data, int size);

/*****************************************************************************
A pointer to a "router_population", which is a function that gets the
population of the router
*****************************************************************************/
typedef int (*router_population_ftype)
        (router_context_pointer router);
        
        
/*****************************************************************************
A pointer to a "router_context_population", which is a function that gets the
population of the router
*****************************************************************************/
typedef int (*router_context_population_ftype)
        (router_context_pointer router);

/*****************************************************************************
A pointer to a "router_port_population", which is a function that
returns the number of tokens available that are currently for the port in the 
router.
*****************************************************************************/
typedef int (*router_port_population_ftype)
        (router_port_pointer port);

        
/*****************************************************************************
A pointer to a "router_capacity", which is a function that gets the
capacity of the router
*****************************************************************************/
typedef int (*router_capacity_ftype)
        (router_context_pointer router);
        
/*****************************************************************************
A pointer to a "router_context_capacity", which is a function that gets the
capacity of the router
*****************************************************************************/
typedef int (*router_context_capacity_ftype)
        (router_context_pointer router);
        
/*****************************************************************************
A pointer to a "router_port_capacity", which is a function that gets the
capacity of the router
*****************************************************************************/
typedef int (*router_port_capacity_ftype)
        (router_port_pointer port);
 
/*****************************************************************************
A pointer to a "router_token_size", which is a function that gets the
token size
*****************************************************************************/
typedef int (*router_token_size_ftype)
        (router_context_pointer router);
        
/*****************************************************************************
A pointer to a "router_reset", which is a function that resets the router
*****************************************************************************/
typedef void (*router_reset_ftype)
        (router_context_pointer router);
        
 /*****************************************************************************
Deallocate the storage associated with the given router.
*****************************************************************************/
typedef void (*router_free_ftype)
        (router_context_pointer router);
        
/*****************************************************************************
A pointer to a "router_set_index", which is a function that set index of
router in the dataflow graph
*****************************************************************************/
typedef void (*router_set_index_ftype)
        (router_context_pointer router, int index);

/*****************************************************************************
A pointer to a "router_get_index", which is a function that get index of
router in the dataflow graph
*****************************************************************************/
typedef int (*router_get_index_ftype)
        (router_context_pointer router);

/*****************************************************************************
A pointer to a "router_get_port_with_index", which is a function that
get port in the router in the dataflow graph given index of the port
*****************************************************************************/
typedef router_port_pointer (*
        router_get_router_port_with_index_ftype)
        (router_context_pointer router, int index);

/*****************************************************************************
A pointer to a "router_get_port", which is a function that get
port in the router in the dataflow graph given group index and group label
*****************************************************************************/
typedef router_port_pointer (*router_get_router_port_ftype)
        (router_context_pointer router, int group_label,
        int group_idx);

/*****************************************************************************
A pointer to a "router_get_port_pointer", which is a function that get
index of router in the dataflow graph
*****************************************************************************/      
typedef router_context_pointer (*
        router_port_get_router_context_ftype)
        (router_port_pointer port);

/*****************************************************************************
A pointer to a "router_port_get_index", which is a function that get
index of port in the dataflow graph 
*****************************************************************************/
typedef int (*router_port_get_index_ftype)
            (router_port_pointer port);
     

// Function declaration
/*****************************************************************************
Create router port pointer 
*****************************************************************************/
router_port_pointer router_port_new(
                                        router_context_pointer router,
                                        int group_label, int group_idx, 
                                        int index);

/*****************************************************************************
Create router context pointer 
*****************************************************************************/
router_context_pointer router_context_new(int capacity,
                                                        int token_size,
                                                        int input_port_count,
                                                        int output_port_count,
                                                        int index);

/*****************************************************************************
Read method
*****************************************************************************/
void router_read(router_port_pointer port, void *data);

/*****************************************************************************
Read method
*****************************************************************************/
void router_read(router_port_pointer port, void *data);

/*****************************************************************************
Peek method
*****************************************************************************/
void router_peek(router_port_pointer port, void *data);

/*****************************************************************************
Read advance method
*****************************************************************************/
void router_read_advance(router_port_pointer port);
  
/*****************************************************************************
Block Read method
*****************************************************************************/
void router_read_block(router_port_pointer port, void *data,
                            int size);                        
  
/*****************************************************************************
Write method
*****************************************************************************/
void router_write(router_port_pointer port, void *data);

/*****************************************************************************
Write method
*****************************************************************************/
void router_write_block(router_port_pointer port, void *data,
                            int size);

/*****************************************************************************
Return the number of tokens that are currently in the router.
*****************************************************************************/
int router_population(router_context_pointer router);

/*****************************************************************************
Return the capacity of the router.
*****************************************************************************/
int router_capacity(router_context_pointer router);

/*****************************************************************************
Return the number of bytes that are used to store a single token in the router.
*****************************************************************************/
int router_token_size(router_context_pointer router);

/*****************************************************************************
Reset the router so that it contains no tokens --- that is, reset to an
empty router.
*****************************************************************************/
void router_reset(router_context_pointer router);

/*****************************************************************************
Deallocate the storage associated with the given router.
*****************************************************************************/
void router_free(router_context_pointer router);

/*****************************************************************************
Deallocate the storage associated with the given router.
*****************************************************************************/
router_port_pointer router_get_port(
                        router_context_pointer router,
                        int group_label, int group_idx);

/*****************************************************************************
Return the number of tokens available that are currently for the port in the 
router.
*****************************************************************************/
int router_port_population(router_port_pointer port);

/*****************************************************************************
Get router context pointer associated with the router port given
*****************************************************************************/
router_context_pointer router_port_get_router_context(
                                            router_port_pointer port);

/*****************************************************************************
Get router port pointer associated with the router port given
*****************************************************************************/
router_port_pointer router_port_get_router_port_pointer(
                                        router_context_pointer router,
                                        int index);        
                 
/*****************************************************************************
Set index of the router port
*****************************************************************************/
int router_port_get_index(router_port_pointer port);

        
        
struct _router_context_struct {
#include "router_context_type_common.h"
};

struct _router_port_struct {
#include "router_port_type_common.h"
};

/*****************************************************************************
* Router as an actor
*****************************************************************************/







#endif
