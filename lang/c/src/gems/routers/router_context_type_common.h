/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/* Common elements across context type of routers. */

/* The number of tokens that the buffer can hold. */
int capacity;

/* Pointer to the first token placeholider in the buffer. */
void *buffer_start;

/* Pointer to the last token placeholider in the buffer. This member
is here for convenience (it can also be derived from the
buffer start, capacity, and token_size).*/
void *buffer_end;

/* The number of tokens that are currently in the router. */
int population;

/* Pointer to the next token placeholider to be read from. */
void **read_pointer;

/* Pointer to the next token placeholider to be written into. */
void *write_pointer;

/* The number of bytes in a single token. */
int token_size;

/* router port array*/
router_port_pointer *ports;

/* input_ports_count*/
int input_ports_count;

/* output_ports_count*/
int output_ports_count;

/* port counts*/
int ports_count;

/* index of the router in a dataflow graph. It is flexible and set by the 
designer*/
int index;

/* methods in router*/
router_read_ftype read;
router_peek_ftype peek;
router_read_advance_ftype read_advance;
router_read_block_ftype read_block;
router_write_ftype write;
//router_write_advance_ftype write_advance;
router_write_block_ftype write_block;

router_context_population_ftype get_cpop;
router_port_population_ftype get_ppop;
router_population_ftype get_pop;

router_capacity_ftype get_cap;
router_context_capacity_ftype get_ccap;
router_port_capacity_ftype get_pcap;

router_token_size_ftype  get_token_size;
router_reset_ftype reset;
router_free_ftype free;


router_get_router_port_with_index_ftype get_port_withidx;
router_get_router_port_ftype get_port;


/* The token printing function (if any) associated with this router. */
router_f_token_printing print;
