/******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "welt_c_util.h"
#include "switch_router.h"
//#include <welt_buffer.c>

#define POINTER_TRUE    0
#define POINTER_FALSE    1
#define DATA    2

#define CONTROL_PORT 0
#define TRUE_PORT 2
#define FALSE_PORT 3
//#define INPUT_PORT 1
#define IN_PORT 0
#define OUT_PORT 1

struct _switch_router_context_struct {
    /* The number of tokens that all the buffers can hold. */
    int capacity;
    
    router_buffer_pointer outbufs[DATA];
    
    router_buffer_pointer input_buffer;
    
    router_buffer_pointer control_buffer;
    
    /*  The number of bytes in a single token. */
    int token_size;
    
    /* router port array*/
    switch_router_port_pointer *ports;
    
    /* input_ports_count*/
    int input_ports_count;
    
    /* output_ports_count*/
    int output_ports_count;
    
    /* port counts*/
    int ports_count;
    
    /* index of the router in a dataflow graph. It is flexible and set by the
    designer*/
    int index;
    
    /* methods in router*/
    switch_router_read_ftype read;
    
    //switch_router_read_block_ftype read_block;
    switch_router_write_ftype write;
    //switch_router_free_ftype free;
    switch_router_get_switch_router_port_ftype get_port;
    
};

struct _switch_router_port_struct {

    /* label for input or output port group*/
    int group_label;
    /* index in the group*/
    int group_idx;
    /* index in the router */
    int index;
    /* Port's current "population" for every port*/
    int population;
    /* Router's current "capacity" for every port*/
    int capacity;
    
    /* Router context */
    switch_router_context_pointer  switch_router_context;
    
    /* Get context*/
    switch_router_port_get_switch_router_context_ftype get_context;
    switch_router_port_population_ftype pop;
    switch_router_port_get_index_ftype get_port_index;
    switch_router_port_freespace_ftype fsp;
    
};



/*****************************************************************************
Public functions.
*****************************************************************************/

/* switch_router port construct function */
/*  input port label:INPUT_PORT --> 0;
output port label: OUTPUT_PORT --> 1 */

switch_router_port_pointer switch_router_port_new(
        switch_router_context_pointer switch_router,
        int group_label, int group_idx, int index){
    
    switch_router_port_type *port = NULL;
    
    port = welt_c_util_malloc(sizeof(switch_router_port_type));
    port->group_label = group_label;
    port->group_idx = group_idx;
    port->population = 0;
    port->capacity = switch_router->capacity;
    port->index = index + switch_router->input_ports_count * port->group_label +
    port->group_idx;
    port->switch_router_context = switch_router;
    port->get_context = switch_router_port_get_switch_router_context;
    port->pop = switch_router_port_population;
    port->get_port_index = switch_router_port_get_index;
    port->fsp = switch_router_freespace;
    
    return port;
}

switch_router_context_pointer switch_router_context_new(
        int capacity, int token_size,int input_ports_count,
        int output_ports_count, int index){
    
    int i;
    int tmp;
    
    switch_router_context_pointer context = NULL;
   
    if (capacity < 1) {
        return NULL;
    }
    if (token_size <= 0) {
        return NULL;
    }
    
    context = (switch_router_context_pointer)welt_c_util_malloc(
            sizeof(switch_router_context_type));
   
    context->capacity = capacity;
    context->token_size = token_size;
    context->input_ports_count = input_ports_count;
    context->output_ports_count = output_ports_count;
    context->ports_count = context->input_ports_count +
    context->output_ports_count;
    context->index = index;
    
    /*port initialization*/
    context->ports = NULL;
    context->ports = welt_c_util_malloc(sizeof
            (switch_router_port_pointer)* context->ports_count);

    /* input port initialization*/
    /*control port*/
    i = 0;
    context->ports[i] = switch_router_port_new(context, 0, i,
            context->index );
 
    for (i = 1; i < context->input_ports_count; i++){
        context->ports[i] = switch_router_port_new(context, 0,
                i, context->index);
       
    }
 
    /* output port initialization*/
    for (i = context->input_ports_count; i < context->ports_count; i++){
        tmp = i - context->input_ports_count;
        context->ports[i] = switch_router_port_new(context,1, tmp,
                context->index);
    }
 
    context->outbufs[POINTER_TRUE] = router_buffer_new ( context->capacity,
            context-> token_size);

    context->outbufs[POINTER_FALSE] = router_buffer_new ( context->capacity,
            context-> token_size);

    context->input_buffer = router_buffer_new (context->capacity, context->
            token_size);
    
    context->control_buffer = router_buffer_new ( context->capacity, context->
            token_size);
    /* methods */
    context->read = (switch_router_read_ftype) switch_router_read;
    context->write = (switch_router_write_ftype)switch_router_write;
    return context;
}


void switch_router_read
        (switch_router_context_pointer switch_router,
        switch_router_port_pointer port, void *d){
    
    int z = port->group_idx;
    /* read from t/fbuffer*/
    router_bufread(switch_router->outbufs[z],d);
    return;
}


void switch_router_write( switch_router_context_pointer
        switch_router, switch_router_port_pointer port, void *d){
    
    int z = port->group_idx; /* Input index */
    int inval =0 ; //value
    void * cval = NULL; //peek
    void * dval = NULL;
    if (z!=0){    //when it is not a control
        cval = router_bufpeek(switch_router->control_buffer);
        //see if there is available control token
        if ( cval  == NULL ){
            /* if there is no control token available, store it to input buffer */
            router_bufwrite( switch_router->input_buffer , d);

        }else{
            /* control available, so move pointer in control buffer and write
            the input into t/f buffer*/
            router_bufadvance(switch_router->control_buffer);
            router_bufwrite(switch_router-> outbufs[*(int*)cval], d);
            //printf("\n control available , write %d to %d buffer.\n", *
            // (int*)d,*(int*)cval);
        }
        
    }else{              /*when it is a control*/
        // printf("\n This is a control for switch.\n");
        dval=router_bufpeek(switch_router->input_buffer);
        /* see if there is available input token. */
        if (dval == NULL){
            // printf("\n no input token available in switch.\n");
            /*if there is no input token available, write it into cbuf*/
            router_bufwrite(switch_router->control_buffer, d);
        }
        else{

            /*if there was a input token, write to one of out buffer*/
            // router_bufadvance(switch_router->input_buffer);
            router_bufwrite(switch_router-> outbufs[*(int*)d], dval);
            // router_bufadvance(switch_router->input_buffer);
        }
    }
    return;
}

switch_router_port_pointer switch_router_get_port(
        switch_router_context_pointer switch_router,
        int group_label, int group_idx){
    
    int port_index;
    port_index = switch_router->input_ports_count * group_label + group_idx;
    return switch_router->ports[port_index];
}


int switch_router_port_population(switch_router_port_pointer port)
{
    return port->population;
}

switch_router_context_pointer
switch_router_port_get_switch_router_context(
        switch_router_port_pointer port){
    
    return port->switch_router_context;
}

switch_router_port_pointer
switch_router_port_get_switch_router_port_pointer(
        switch_router_context_pointer switch_router, int index){
    if(index < switch_router -> ports_count && index >= 0){
        return switch_router->ports[index];
    }else{
        fprintf(stderr, "port index is out of range.\n");
        return NULL;
    }
    
}

/*int switch_router_population
(switch_router_context_pointer switch_router){
    
    int z= 0;
    z = port->group_idx;
    return switch_router->data[z]->population ;
    
}*/
/*int switch_router_port_population
(switch_router_port_pointer port){
    
    int z= 0;
    z = port->group_idx; 
    return switch_router->data[z-2]->population ;
    
}*/


int switch_router_port_get_index(switch_router_port_pointer port){
    return port->index;
}

int switch_router_freespace(switch_router_port_pointer port){
    return port->switch_router_context->input_buffer->capacity -
            port->switch_router_context->input_buffer->population;
  
}

