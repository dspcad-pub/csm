#ifndef _select_router_h
#define _select_router_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include "welt_basic.h"
#include "buffer.h"
#include "welt_c_fifo.h"
#include "welt_c_actor.h"


/*****************************************************************************
Each data item in the select_router is referred to as a "token". Tokens can have
arbitrary types associated with them. For a given select_router instance, there
is a fixed token size (number of bytes per token). Tokens have arbitrary
types --- e.g., they can be integers, floating point values (float or
double), characters, or pointers (to any kind of data). This organization
allows for flexibility in storing different kinds of data values, 
and efficiency in storing the data values directly (without being
encapsulated in any sort of higher-level "token" object).

actor-->input port--->|_|_|_|_|_|_|-->output port-->actor

*****************************************************************************/


/*****************************************************************************
* Router as a connector
*****************************************************************************/
// Router context
typedef struct _select_router_context_struct
        select_router_context_type;
// A pointer to a select_router context.
typedef select_router_context_type *select_router_context_pointer;

// Router port
typedef struct _select_router_port_struct select_router_port_type;
// A pointer to a select_router port.
typedef select_router_port_type *select_router_port_pointer;

/*****************************************************************************
A pointer to a "select_router_f_token_printing", which is a function
that prints select_router's basic information. 
*****************************************************************************/
//typedef void (*select_router_f_token_printing)(FILE *output, void *token);

/*****************************************************************************
A pointer to a "select_router_read", which is a function that reads
data from select_router.
*****************************************************************************/
typedef void (*select_router_read_ftype)
        (select_router_port_pointer port, void *data);

/*****************************************************************************
A pointer to a "select_router_peek", which is a function that peek
data from select_router.
*****************************************************************************/
//typedef void (*select_router_peek_ftype)
//      (select_router_port_pointer port, void *data);


/*****************************************************************************
A pointer to a "select_router_read_advance", which is a function that update
data population without reading data in select_router.
*****************************************************************************/
//typedef void (*select_router_read_advance_ftype)
//       (select_router_port_pointer  port);
        
/*****************************************************************************
A pointer to a "select_router_read_block", which is a function that reads a
block of data from select_router
*****************************************************************************/
typedef void (*select_router_read_block_ftype)
        (select_router_port_pointer port, void *data, int size);
        
/*****************************************************************************
A pointer to a "select_router_write_function", which is a function that writes
data to the select_router
*****************************************************************************/
typedef void (*select_router_write_ftype)
        (select_router_port_pointer port, void *data);

/*****************************************************************************
A pointer to a "select_router_write_advance", which is a function that writes
data to the select_router 
*****************************************************************************/
//typedef void (*select_router_write_advance_ftype)
//        (select_router_port_pointer port);

/*****************************************************************************
A pointer to a "select_router_write_block", which is a function that reads a
block of data to the select_router
*****************************************************************************/
//typedef void (*select_router_write_block_ftype)
//        (select_router_port_pointer port, void *data, int size);

/*****************************************************************************
A pointer to a "select_router_population", which is a function that gets the
population of the select_router
*****************************************************************************/
typedef int (*select_router_population_ftype)
        (select_router_port_pointer port);

typedef int (*select_router_port_freespace_ftype)
        (select_router_port_pointer port);

/*****************************************************************************
A pointer to a "select_router_context_population", which is a function that gets
the population of the select_router
*****************************************************************************/
typedef int (*select_router_context_population_ftype)
        (select_router_context_pointer select_router);

/*****************************************************************************
A pointer to a "select_router_port_population", which is a function that
returns the number of tokens available that are currently for the port in the 
select_router.
*****************************************************************************/
typedef int (*select_router_port_population_ftype)
        (select_router_port_pointer port);

/*****************************************************************************
A pointer to a "select_router_capacity", which is a function that gets the
capacity of the select_router
*****************************************************************************/
//typedef int (*select_router_capacity_ftype)
//        (select_router_context_pointer select_router);
        
/*****************************************************************************
A pointer to a "select_router_context_capacity", which is a function that gets
the capacity of the select_router
*****************************************************************************/
//typedef int (*select_router_context_capacity_ftype)
//        (select_router_context_pointer select_router);
        
/*****************************************************************************
A pointer to a "select_router_port_capacity", which is a function that gets the
capacity of the select_router
*****************************************************************************/
//typedef int (*select_router_port_capacity_ftype)
//        (select_router_port_pointer port);
 
/*****************************************************************************
A pointer to a "select_router_token_size", which is a function that gets the
token size
*****************************************************************************/
typedef int (*select_router_token_size_ftype)
        (select_router_context_pointer select_router);
        
/*****************************************************************************
A pointer to a "select_router_reset", which is a function that resets the
select_router
*****************************************************************************/
//typedef void (*select_router_reset_ftype)
//        (select_router_context_pointer select_router);
        
/*****************************************************************************
Deallocate the storage associated with the given select_router.
*****************************************************************************/
typedef void (*select_router_free_ftype)
        (select_router_context_pointer select_router);
        
/*****************************************************************************
A pointer to a "select_router_set_index", which is a function that set index of
select_router in the dataflow graph
*****************************************************************************/
typedef void (*select_router_set_index_ftype)
        (select_router_context_pointer select_router, int index);

/*****************************************************************************
A pointer to a "select_router_get_index", which is a function that get index of
select_router in the dataflow graph
*****************************************************************************/
typedef int (*select_router_get_index_ftype)
        (select_router_context_pointer select_router);

/*****************************************************************************
A pointer to a "select_router_get_port_with_index", which is a function that
get port in the select_router in the dataflow graph given index of the port
*****************************************************************************/
typedef select_router_port_pointer (*
        select_router_get_select_router_port_with_index_ftype)
        (select_router_context_pointer select_router, int index);

/*****************************************************************************
A pointer to a "select_router_get_port", which is a function that get
port in the select_router in the dataflow graph given group index and group
label
*****************************************************************************/
typedef select_router_port_pointer (*select_router_get_select_router_port_ftype)
        (select_router_context_pointer select_router, int group_label,
        int group_idx);

/*****************************************************************************
A pointer to a "select_router_get_port_pointer", which is a function that get
index of select_router in the dataflow graph
*****************************************************************************/
typedef select_router_context_pointer (*
        select_router_port_get_select_router_context_ftype)
        (select_router_port_pointer port);

/*****************************************************************************
A pointer to a "select_router_port_get_index", which is a function that get
index of port in the dataflow graph 
*****************************************************************************/
typedef int (*select_router_port_get_index_ftype) (select_router_port_pointer
        port);
     
        
        
// Function declaration
/*****************************************************************************
Create select_router port pointer 
*****************************************************************************/
select_router_port_pointer select_router_port_new(
        select_router_context_pointer select_router, int group_label, int
        group_idx, int index);

/*****************************************************************************
Create select_router context pointer 
*****************************************************************************/
select_router_context_pointer select_router_context_new(int capacity,
        int token_size, int input_port_count, int output_port_count,
        int index);


void process_control_tokens(select_router_context_pointer select_router);

int select_router_port_population
        (select_router_port_pointer port);
/*****************************************************************************
Read method
*****************************************************************************/
void select_router_read(select_router_context_pointer select_router,
        select_router_port_pointer port, void *data);


/*****************************************************************************
Peek method
*****************************************************************************/
void select_router_peek(select_router_port_pointer port,
        void *data);

/*****************************************************************************
Read advance method
*****************************************************************************/
void select_router_read_advance(select_router_port_pointer port);
  
/*****************************************************************************
Block Read method
*****************************************************************************/
void select_router_read_block(select_router_port_pointer port,
        void *data, int size);
  
/*****************************************************************************
Write method
*****************************************************************************/
void select_router_write(select_router_context_pointer
        select_router, select_router_port_pointer port, void *data);

/*****************************************************************************
Write method
*****************************************************************************/
void select_router_write_block(select_router_port_pointer port, void *data,
        int size);

/*****************************************************************************
Return the number of tokens that are currently in the select_router.
*****************************************************************************/
//int select_router_population
//(select_router_context_pointer select_router);

/*****************************************************************************
Return the capacity of the select_router.
*****************************************************************************/
int select_router_capacity(select_router_context_pointer select_router);

/*****************************************************************************
Return the number of bytes that are used to store a single token in the
select_router.
*****************************************************************************/
int select_router_token_size(select_router_context_pointer select_router);

/*****************************************************************************
Reset the select_router so that it contains no tokens --- that is, reset to an
empty select_router.
*****************************************************************************/
void select_router_reset(select_router_context_pointer select_router);

/*****************************************************************************
Deallocate the storage associated with the given select_router.
*****************************************************************************/
void select_router_free(select_router_context_pointer select_router);

/*****************************************************************************
Deallocate the storage associated with the given select_router.
*****************************************************************************/
select_router_port_pointer select_router_get_port(
        select_router_context_pointer select_router,
        int group_label, int group_idx);

/*****************************************************************************
Return the number of tokens available that are currently for the port in the 
select_router.
*****************************************************************************/
int select_router_port_population(select_router_port_pointer port);

/*****************************************************************************
Get select_router context pointer associated with the select_router port given
*****************************************************************************/
select_router_context_pointer select_router_port_get_select_router_context(
        select_router_port_pointer port);

/*****************************************************************************
Get select_router port pointer associated with the select_router port given
*****************************************************************************/
select_router_port_pointer select_router_port_get_select_router_port_pointer(
        select_router_context_pointer select_router, int index);
                 
/*****************************************************************************
Set index of the select_router port
*****************************************************************************/
int select_router_port_get_index(select_router_port_pointer port);

/* struct _select_router_context_struct {
#include "select_router_context_type_common.h"
};

struct _select_router_port_struct {
#include "select_router_port_type_common.h"
};*/
/*****************************************************************************
* Router as an actor
*****************************************************************************/
int select_router_capacity(select_router_context_pointer select_router);
int select_router_freespace(select_router_port_pointer port);


#endif
