#ifndef _fifo_router_h
#define _fifo_router_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include "welt_basic.h"
#include "buffer.h"
#include "welt_c_fifo.h"
#include "welt_c_actor.h"

#include <stdbool.h>


/*****************************************************************************
* Router as a connector
*****************************************************************************/
// Router context
typedef struct _fifo_router_context_struct
        fifo_router_context_type;
// A pointer to a fifo_router context.
typedef fifo_router_context_type *fifo_router_context_pointer;

// Router port
typedef struct _fifo_router_port_struct fifo_router_port_type;

// A pointer to a fifo_router port.
typedef fifo_router_port_type *fifo_router_port_pointer;


/*****************************************************************************
A pointer to a "fifo_router_f_token_printing", which is a function
that prints fifo_router's basic information.
*****************************************************************************/
//typedef void (*fifo_router_f_token_printing)(FILE *output, void *token);

/*****************************************************************************
A pointer to a "fifo_router_read", which is a function that reads
data from fifo_router.
*****************************************************************************/
typedef void (*fifo_router_read_ftype)
        (fifo_router_port_pointer port, void *data);

/*****************************************************************************
A pointer to a "fifo_router_peek", which is a function that peek
data from fifo_router.
*****************************************************************************/
typedef void (*fifo_router_peek_ftype)
        (fifo_router_port_pointer port, void *data);


/*****************************************************************************
A pointer to a "fifo_router_read_advance", which is a function that update
data population without reading data in fifo_router.
*****************************************************************************/
//typedef void (*fifo_router_read_advance_ftype)
//        (fifo_router_port_pointer  port);
        
/*****************************************************************************
A pointer to a "fifo_router_read_block", which is a function that reads a
block of data from fifo_router
*****************************************************************************/
//typedef void (*fifo_router_read_block_ftype)
//        (fifo_router_port_pointer port, void *data, int size);
        
/*****************************************************************************
A pointer to a "fifo_router_write_function", which is a function that writes
data to the fifo_router
*****************************************************************************/
typedef void (*fifo_router_write_ftype)
        (fifo_router_port_pointer port, void *data);

/*****************************************************************************
A pointer to a "fifo_router_write_advance", which is a function that writes
data to the fifo_router
*****************************************************************************/
//typedef void (*fifo_router_write_advance_ftype)
//        (fifo_router_port_pointer port);

/*****************************************************************************
A pointer to a "fifo_router_write_block", which is a function that reads a
block of data to the fifo_router
*****************************************************************************/
//typedef void (*fifo_router_write_block_ftype)
//        (fifo_router_port_pointer port, void *data, int size);

/*****************************************************************************
A pointer to a "fifo_router_population", which is a function that gets the
population of the fifo_router
*****************************************************************************/
typedef int (*fifo_router_port_population_ftype)
        (fifo_router_port_pointer port);
        
        
/*****************************************************************************
A pointer to a "fifo_router_context_population", which is a function that gets
the population of the fifo_router
*****************************************************************************/
//typedef int (*fifo_router_context_population_ftype)
//        (fifo_router_context_pointer fifo_router);

/*****************************************************************************
A pointer to a "fifo_router_port_population", which is a function that
returns the number of tokens available that are currently for the port in the 
fifo_router.
*****************************************************************************/
typedef int (*fifo_router_port_population_ftype)
        (fifo_router_port_pointer port);

typedef int (*fifo_router_port_freespace_ftype)
        (fifo_router_port_pointer port);

/*****************************************************************************
A pointer to a "fifo_router_capacity", which is a function that gets the
capacity of the fifo_router
*****************************************************************************/
//typedef int (*fifo_router_capacity_ftype)
//       (fifo_router_context_pointer fifo_router);
        
/*****************************************************************************
A pointer to a "fifo_router_context_capacity", which is a function that gets the
capacity of the fifo_router
*****************************************************************************/
//typedef int (*fifo_router_context_capacity_ftype)
//        (fifo_router_context_pointer fifo_router);
        
/*****************************************************************************
A pointer to a "fifo_router_port_capacity", which is a function that gets the
capacity of the fifo_router
*****************************************************************************/
//typedef int (*fifo_router_port_capacity_ftype)
//        (fifo_router_port_pointer port);
 
/*****************************************************************************
A pointer to a "fifo_router_token_size", which is a function that gets the
token size
*****************************************************************************/
//typedef int (*fifo_router_token_size_ftype)
//        (fifo_router_context_pointer fifo_router);
        
/*****************************************************************************
A pointer to a "fifo_router_reset", which is a function that resets the
 fifo_router
*****************************************************************************/
//typedef void (*fifo_router_reset_ftype)
//        (fifo_router_context_pointer fifo_router);
        
/*****************************************************************************
Deallocate the storage associated with the given fifo_router.
*****************************************************************************/
//typedef void (*fifo_router_free_ftype)
  //      (fifo_router_context_pointer fifo_router);
        
/*****************************************************************************
A pointer to a "fifo_router_set_index", which is a function that set index of
fifo_router in the dataflow graph
*****************************************************************************/
//typedef void (*fifo_router_set_index_ftype)
//        (fifo_router_context_pointer fifo_router, int index);

/*****************************************************************************
A pointer to a "fifo_router_get_index", which is a function that get index of
fifo_router in the dataflow graph
*****************************************************************************/
typedef int (*fifo_router_get_index_ftype)
        (fifo_router_context_pointer fifo_router);

/*****************************************************************************
A pointer to a "fifo_router_get_port_with_index", which is a function that
get port in the fifo_router in the dataflow graph given index of the port
*****************************************************************************/        
typedef fifo_router_port_pointer (*
        fifo_router_get_fifo_router_port_with_index_ftype)
        (fifo_router_context_pointer fifo_router, int index);

/*****************************************************************************
A pointer to a "fifo_router_get_port", which is a function that get
port in the fifo_router in the dataflow graph given group index and group label
*****************************************************************************/
typedef fifo_router_port_pointer (*fifo_router_get_fifo_router_port_ftype)
        (fifo_router_context_pointer fifo_router, int group_label,
        int group_idx);

/*****************************************************************************
A pointer to a "fifo_router_get_port_pointer", which is a function that get
index of fifo_router in the dataflow graph
*****************************************************************************/      
typedef fifo_router_context_pointer (*
        fifo_router_port_get_fifo_router_context_ftype)
        (fifo_router_port_pointer port);

/*****************************************************************************
A pointer to a "fifo_router_port_get_index", which is a function that get
index of port in the dataflow graph 
*****************************************************************************/
typedef int (*fifo_router_port_get_index_ftype)
            (fifo_router_port_pointer port);
     

// Function declaration
/*****************************************************************************
Create fifo_router port pointer
*****************************************************************************/
fifo_router_port_pointer fifo_router_port_new(
        fifo_router_context_pointer fifo_router,
        int group_label, int group_idx, int index);

/*****************************************************************************
Create fifo_router context pointer
*****************************************************************************/
fifo_router_context_pointer fifo_router_context_new(int capacity,
        int token_size, int input_port_count, int output_port_count,
        int index);


/*****************************************************************************
Read method
*****************************************************************************/
void fifo_router_read(fifo_router_context_pointer
        fifo_router, fifo_router_port_pointer port, void *data);

void fifo_router_read_block (fifo_router_context_pointer
        fifo_router, fifo_router_port_pointer port, int num, void *d);

/*****************************************************************************
Peek method
*****************************************************************************/
void fifo_router_peek(fifo_router_port_pointer port, void *data);

/*****************************************************************************
Read advance method
*****************************************************************************/
//void fifo_router_read_advance(fifo_router_port_pointer port);
  
/*****************************************************************************
Block Read method
*****************************************************************************/
//void fifo_router_read_block(fifo_router_port_pointer port,
//                           void *data, int size);
  
/*****************************************************************************
Write method
*****************************************************************************/
void fifo_router_write(fifo_router_context_pointer
        fifo_router, fifo_router_port_pointer port, void *data);

/*****************************************************************************
Write method
*****************************************************************************/
void fifo_router_write_block( fifo_router_context_pointer
        fifo_router, fifo_router_port_pointer port, int num, void *d);
/*****************************************************************************
Return the number of tokens that are currently in the fifo_router.
*****************************************************************************/
int fifo_router_population(fifo_router_context_pointer fifo_router);

/*****************************************************************************
Return the capacity of the fifo_router.
*****************************************************************************/
//int fifo_router_capacity(fifo_router_context_pointer fifo_router);

/*****************************************************************************
Return the number of bytes that are used to store a single token in the fifo_router.
*****************************************************************************/
//int fifo_router_token_size(fifo_router_context_pointer fifo_router);

/*****************************************************************************
Reset the fifo_router so that it contains no tokens --- that is, reset to an
empty fifo_router.
*****************************************************************************/
//void fifo_router_reset(fifo_router_context_pointer fifo_router);

/*****************************************************************************
Deallocate the storage associated with the given fifo_router.
*****************************************************************************/
//void fifo_router_free(fifo_router_context_pointer fifo_router);

/*****************************************************************************
Deallocate the storage associated with the given fifo_router.
*****************************************************************************/
fifo_router_port_pointer fifo_router_get_port(
        fifo_router_context_pointer fifo_router, int group_label, int
        group_idx);
int fifo_router_freespace(fifo_router_port_pointer port);

/*****************************************************************************
Return the number of tokens available that are currently for the port in the 
fifo_router.
*****************************************************************************/
int fifo_router_port_population(fifo_router_port_pointer port);

/*****************************************************************************
Get fifo_router context pointer associated with the fifo_router port given
*****************************************************************************/
fifo_router_context_pointer fifo_router_port_get_fifo_router_context(
        fifo_router_port_pointer port);

/*****************************************************************************
Get fifo_router port pointer associated with the fifo_router port given
*****************************************************************************/
fifo_router_port_pointer fifo_router_port_get_fifo_router_port_pointer(
        fifo_router_context_pointer fifo_router, int index);
                 
/*****************************************************************************
Set index of the fifo_router port
*****************************************************************************/
int fifo_router_port_get_index(fifo_router_port_pointer port);

#endif
