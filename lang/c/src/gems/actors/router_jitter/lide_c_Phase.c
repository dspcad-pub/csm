/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lide_c_Phase.h"
#include "lide_c_util.h"

struct _lide_c_Phase_context_struct {
#include "lide_c_actor_context_type_common.h"
    /* Actor interface ports. */
    /*input sequence*/
    //lide_c_fifo_pointer in1; /* TRall array*/
    //lide_c_fifo_pointer in2; /* TRall_num*/
    //lide_c_fifo_pointer in3; /* CREtime*/
    lide_c_fifo_pointer in4; /* Coefficient a from linear fitting */
    
    router_port_pointer  in1; /* TRall array*/
    router_context_pointer router_in1;
    
    router_port_pointer  in2; /* TRall_num*/
    router_context_pointer router_in2;
    
    router_port_pointer  in3; /* CREtime*/
    router_context_pointer router_in3;
    
    
    /*Output sequence*/
    lide_c_fifo_pointer out1; /* TIE*/

    /* Actor Parameters*/
    int Ws,k,len,Num; /*Window Size*/
    int TRlength;
    double *vol;
    int *state;
    int *TR;
    int *TRindex;
    int Pindex;
    double *TRT;
    double *TRT_all;
    double *d;
    double *phase_all;
    double *phase;
    double *phase_correct;
    double *phase_error;
    double *tie;
    double *TRall;
    int TRall_num;
    double CREtime;
    double a;
};

lide_c_Phase_context_type *lide_c_Phase_new(int Ws, int num, 
                router_port_pointer in1,
                router_port_pointer in2,
                router_port_pointer in3, lide_c_fifo_pointer in4,
                lide_c_fifo_pointer out1){
    int i;
    lide_c_Phase_context_type *context = NULL;
    context = lide_c_util_malloc(sizeof(lide_c_Phase_context_type));
    context->enable = (lide_c_actor_enable_function_type)lide_c_Phase_enable;
    context->invoke = (lide_c_actor_invoke_function_type)lide_c_Phase_invoke;
    //context->in1 = in1;
    //context->in2 = in2;
    //context->in3 = in3;
    context->in4 = in4;
    context->out1 = out1;
    
    context->in1 = in1;
    context->router_in1 = router_port_get_router_context(context->in1);
    
    context->in2 = in2;
    context->router_in2 = router_port_get_router_context(context->in2);
    
    context->in3 = in3;
    context->router_in3 = router_port_get_router_context(context->in3);
    
    context->mode = LIDE_C_Phase_MODE_LENGTH;
    /*Load table and Parameter*/
    context->Ws = Ws;
    context->Num = num;
    context->k = 0;
    context->len = 0;
    context->TRall_num = 0;
    context->tie = (double *)malloc(sizeof(double)*PHS_DATA_MAX);
    context->TRall = (double *)malloc(sizeof(double)*PHS_DATA_MAX);
    /* Initialize array*/
    for (i=0; i<Ws; i++){
        context->tie[i] = 0.0; 
        context->TRall[i] = 0.0;
    }
    return context;
}

bool lide_c_Phase_enable(lide_c_Phase_context_type *context) {
    bool result = false;
    switch (context->mode) {
        case LIDE_C_Phase_MODE_LENGTH:
        //REVISE HERE
            result = (router_port_population(context->in2) >= 1);
        break;
        case LIDE_C_Phase_MODE_PROCESS:
            result = (router_port_population(context->in1) >=
                    context->TRall_num) &&
                    (router_port_population(context->in3) >= 1) &&
                    (lide_c_fifo_population(context->in4) >= 1) &&
                    (lide_c_fifo_population(context->out1) + 
                    context->TRall_num <= lide_c_fifo_capacity(context->out1));
        break;
        default:
            result = false;
        break;
    }
    return result;
}

void lide_c_Phase_invoke(lide_c_Phase_context_type *context) {
    int i,k;
    double *fp;
    int	*intp;
    double a,b,high,low,medium,CREtime,REtime,RREtime,dlen;
    double tmp,phase_offset,fN;
    
    switch (context->mode) {
        case LIDE_C_Phase_MODE_LENGTH:

            router_read (context->in2, &context->TRall_num);
            context->mode = LIDE_C_Phase_MODE_PROCESS;
        break;
        case LIDE_C_Phase_MODE_PROCESS:
            router_read_block (context->in1, context->TRall,
                                    context->TRall_num);
            router_read (context->in3, &context->CREtime);
            lide_c_fifo_read (context->in4, &context->a);
            phase_offset = context->a;
            /*Update the TRTall*/
            context->k += 1;
            for(i = 0; i<context->TRall_num; i++){
                tmp = context->TRall[i]-(double)(((int)(context->TRall[i]
                        /context->CREtime+0.5))*context->CREtime);
                context->tie[i] = tmp-phase_offset;
                /*sub = tmp-phase_offset-context->tie[i];*/
            }
            
            lide_c_fifo_write_block(context->out1, context->tie, 
                                    context->TRall_num);
            context->mode = LIDE_C_Phase_MODE_LENGTH;
        break;
        default:
            context->mode = LIDE_C_Phase_MODE_ERROR;
        break;
    }
    return;
    
}

void lide_c_Phase_terminate(lide_c_Phase_context_type *context){  	
    free(context->tie);
    free(context);
}