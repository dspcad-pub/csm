#ifndef _lide_c_Phase_h
#define _lide_c_Phase_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the Phase (Refined Estimation) actor.
*******************************************************************************/


#include "lide_c_actor.h"
#include "lide_c_fifo.h"
#include "router.h"

/* Actor modes */
#define LIDE_C_Phase_MODE_PROCESS   1
#define LIDE_C_Phase_MODE_LENGTH    2
#define LIDE_C_Phase_MODE_ERROR     3
#define LIDE_C_Phase_MODE_HALT 0

#define PHS_DATA_MAX		15999986
/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/
/* Structure and pointer types associated with add objects. */
struct _lide_c_Phase_context_struct;
typedef struct _lide_c_Phase_context_struct lide_c_Phase_context_type;

/*******************************************************************************
Create a new actor with two input fifos and three output fifos.
*******************************************************************************/
/*
lide_c_Phase_context_type *lide_c_Phase_new(int Ws, int num,
                lide_c_fifo_pointer in1, lide_c_fifo_pointer in2,
                lide_c_fifo_pointer in3, lide_c_fifo_pointer in4,
                lide_c_fifo_pointer out1);
*/

lide_c_Phase_context_type *lide_c_Phase_new(int Ws, int num,
        router_port_pointer in1, router_port_pointer in2,
        router_port_pointer in3, lide_c_fifo_pointer in4,
        lide_c_fifo_pointer out1);

/*******************************************************************************
Enable function checks whether there is enough data for firing the actor
*******************************************************************************/
bool lide_c_Phase_enable(lide_c_Phase_context_type *context);
/*******************************************************************************
Invoke function will Refine the rough estimation.
*******************************************************************************/
void lide_c_Phase_invoke(lide_c_Phase_context_type *context);
/*******************************************************************************
Terminate function will free the actor including all the memory allocated.
*******************************************************************************/
void lide_c_Phase_terminate(lide_c_Phase_context_type *context);

#endif

