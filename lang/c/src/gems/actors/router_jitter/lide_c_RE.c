/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lide_c_RE.h"
#include "lide_c_util.h"

struct _lide_c_RE_context_struct {
#include "lide_c_actor_context_type_common.h"
    /* Actor interface ports. */
    /*input sequence*/
    //lide_c_fifo_pointer in1; /*trt_num*/
    //lide_c_fifo_pointer in2; /*TRall*/
    //lide_c_fifo_pointer in3; /*TRall_num*/
    
    router_port_pointer  in1; /*trt_num*/
    router_context_pointer router_in1;
    
    router_port_pointer  in2; /*TRall*/
    router_context_pointer router_in2;
    
    router_port_pointer  in3; /*TRall_num*/
    router_context_pointer router_in3;
    
    /*Output sequence*/
    lide_c_fifo_pointer out1; /*REtime*/
    lide_c_fifo_pointer out2; /*d array: difference*/

    /* Actor Parameters*/
    int Ws; /*Window Size*/
    int TRlength;
    double *vol;
    int *TR;
    int *TRindex;
    double *TRT,*TRall;
    double *d, *d_cp;
    int TRall_num;
    double REtime;
    int trt_num;
};

int cmp (const void * a, const void * b)
{
    double c = *(double*)a - *(double*)b;
    if (c>0)
        return 1;
    else if (c<0)
        return -1;
    else 
        return 0;

}

lide_c_RE_context_type *lide_c_RE_new(int Ws, router_port_pointer in1,
                        router_port_pointer in2,
                        router_port_pointer in3,
                        lide_c_fifo_pointer out1,lide_c_fifo_pointer out2){
    int i;
    lide_c_RE_context_type *context = NULL;
    context = lide_c_util_malloc(sizeof(lide_c_RE_context_type));
    context->enable = (lide_c_actor_enable_function_type)lide_c_RE_enable;
    context->invoke = (lide_c_actor_invoke_function_type)lide_c_RE_invoke;
    
    /*
    context->in1 = in1;
    context->in2 = in2;
    context->in3 = in3;
    */
    
    context->in1 = in1;
    context->router_in1 = router_port_get_router_context(context->in1);
    
    context->in2 = in2;
    context->router_in2 = router_port_get_router_context(context->in2);
    
    context->in3 = in3;
    context->router_in3 = router_port_get_router_context(context->in3);
    
    context->out1 = out1;
    context->out2 = out2;

    context->mode = LIDE_C_RE_MODE_LENGTH;
    /*Load table and Parameter*/
    context->Ws = Ws;
    /*Allocate memory for sorting*/
    context->d = (double*)malloc(sizeof(double)*RE_DATA_MAX);
    context->d_cp = (double*)malloc(sizeof(double)*RE_DATA_MAX);
    context->TRall = (double*)malloc(sizeof(double)*RE_DATA_MAX);
    /* initialize the array*/
    for (i=0; i<RE_DATA_MAX; i++){   
        context->d[i] = 0.0; 
        context->d_cp[i] = 0.0; 
        context->TRall[i] = 0.0; 
    }
    return context;
}

bool lide_c_RE_enable(lide_c_RE_context_type *context) {
    bool result = false;
    //result =1;
    //return result;
    switch (context->mode) {
        case LIDE_C_RE_MODE_LENGTH:
            result = (router_port_population(context->in1) >= 1) &&
                    (router_port_population(context->in3) >= 1);
        break;
        case LIDE_C_RE_MODE_PROCESS:
            result = (router_port_population(context->in2) >=
                    context->TRall_num) &&
                    (lide_c_fifo_population(context->out1) < 
                    lide_c_fifo_capacity(context->out1)) &&
                    (lide_c_fifo_population(context->out2) + 
                    context->TRall_num <= lide_c_fifo_capacity(context->out2));
        break;
        default:
            result = false;
        break;
    }
    //printf("RE enable: %d; mode:%d\n", result, context->mode);
    return result;
}

void lide_c_RE_invoke(lide_c_RE_context_type *context) {
    int tmp,i,len;
    double *fp;
    int	*intp;
    double high,low,medium,REtime,dlen;
    double *d = NULL;
    //printf("RE start\n");
    switch (context->mode) {
        case LIDE_C_RE_MODE_LENGTH:
            router_read(context->in1, &context->trt_num);
            router_read(context->in3, &context->TRall_num);
            context->mode = LIDE_C_RE_MODE_PROCESS;
            //printf("RE LENGTH MODE END\n");
        break;   
        case LIDE_C_RE_MODE_PROCESS:
            router_read_block(context->in2, context->TRall,
                                    context->TRall_num);
            /*Get the rough estimation*/
            len = context->TRall_num-1;
            for (i = 0; i<context->TRall_num-1; i++){
                context->d[i] = context->TRall[i+1] - context->TRall[i];
                context->d_cp[i] = context->d[i];
            }
            qsort(context->d_cp, len, sizeof(context->d_cp[0]), cmp);

            tmp = (int)((len-1)/4);
            context->REtime = context->d_cp[tmp];
            
            lide_c_fifo_write(context->out1, &context->REtime);
            lide_c_fifo_write_block(context->out2, context->d, len);

            context->mode = LIDE_C_RE_MODE_LENGTH;
            
        break;
        default:
            context->mode = LIDE_C_RE_MODE_ERROR;
        break;
    }

    return;
    
}

void lide_c_RE_terminate(lide_c_RE_context_type *context){  
    free(context->d);  	
    free(context->d_cp);  	
    free(context->TRall);  	
    free(context);
}