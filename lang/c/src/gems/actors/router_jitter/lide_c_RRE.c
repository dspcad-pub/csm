/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lide_c_RRE.h"
#include "lide_c_util.h"

struct _lide_c_RRE_context_struct {
#include "lide_c_actor_context_type_common.h"
    /* Actor interface ports. */
    /*input sequence*/
    //lide_c_fifo_pointer in1; /* trt_num <-- FSM*/
    //lide_c_fifo_pointer in2; /* TRall array <--TRT*/
    //lide_c_fifo_pointer in3; /* TRall_num <--TRT*/
    lide_c_fifo_pointer in4; /* REtime*/
    lide_c_fifo_pointer in5; /* d array*/
    
    router_port_pointer in1; /* trt_num <-- FSM*/
    router_context_pointer router_in1;
    router_port_pointer in2; /* TRall array <--TRT*/
    router_context_pointer router_in2;
    router_port_pointer in3; /* TRall_num <--TRT*/
    router_context_pointer router_in3;
    
    /*Output sequence*/
    lide_c_fifo_pointer out1; /*RREtime*/

    /* Actor Parameters*/
    int Ws,len; /*Window Size*/
    int TRlength;
    double *d;
    int *state;
    int *TR;
    int *TRindex;
    double *TRall;
    double RREtime, REtime;
    int TRall_num, trt_num, d_len;
};

lide_c_RRE_context_type *lide_c_RRE_new(int Ws, router_port_pointer in1,
                router_port_pointer in2,
                router_port_pointer in3,
                lide_c_fifo_pointer in4, lide_c_fifo_pointer in5, 
                lide_c_fifo_pointer out1){
    int i;
    lide_c_RRE_context_type *context = NULL;
    context = lide_c_util_malloc(sizeof(lide_c_RRE_context_type));
    context->enable = (lide_c_actor_enable_function_type)lide_c_RRE_enable;
    context->invoke = (lide_c_actor_invoke_function_type)lide_c_RRE_invoke;
    //context->in1 = in1;
    //context->in2 = in2;
    //context->in3 = in3;
    context->in4 = in4;
    context->in5 = in5;
    
    context->in1 = in1;
    context->router_in1 = router_port_get_router_context(context->in1);
    
    context->in2 = in2;
    context->router_in2 = router_port_get_router_context(context->in2);
    
    context->in3 = in3;
    context->router_in3 = router_port_get_router_context(context->in3);
    
    context->out1 = out1;

    context->mode = LIDE_C_RRE_MODE_LENGTH;
    /*Load table and Parameter*/
    context->Ws = Ws;
    
    /*Allocate memory for sorting*/
    context->d = (double*)malloc(sizeof(double)*RRE_DATA_MAX);
    context->TRall = (double*)malloc(sizeof(double)*RRE_DATA_MAX);
    /* initialize the array*/
    for (i=0; i<RRE_DATA_MAX; i++){   
        context->d[i] = 0.0; 
        context->TRall[i] = 0.0; 
    }

    
    return context;
}

bool lide_c_RRE_enable(lide_c_RRE_context_type *context) {
    bool result = false;
    switch (context->mode) {
        case LIDE_C_RRE_MODE_LENGTH:
            result = (router_port_population(context->in1) >= 1)&&
                    (router_port_population(context->in3) >= 1);
        break;
        case LIDE_C_RRE_MODE_PROCESS:
            result = (router_port_population(context->in2) >=
                    context->TRall_num) &&
                    (lide_c_fifo_population(context->in4) >= 1) &&
                    (lide_c_fifo_population(context->in5) >= 
                    (context->TRall_num - 1)) &&
                    (lide_c_fifo_population(context->out1) < 
                    lide_c_fifo_capacity(context->out1));
        break;
        default:
            result = false;
        break;
    }
    //printf("RRE enable: %d; mode:%d\n", result, context->mode);
    //printf("in2: %d\t %d\n", lide_c_fifo_population(context->in2), context->TRall_num);
    //printf("in4: %d\t %d\n", lide_c_fifo_population(context->in4), 1);
    //printf("in5: %d\t %d\n", lide_c_fifo_population(context->in5), context->TRall_num - 1);
    //printf("out1: %d\t %d\n", lide_c_fifo_population(context->out1), lide_c_fifo_capacity(context->out1));
    return result;
}

void lide_c_RRE_invoke(lide_c_RRE_context_type *context) {
    int i,totalUIs,len;
    double *fp;
    int	*intp;
    double high,low,medium,REtime,RREtime,dlen;
    switch (context->mode) {
        case LIDE_C_RRE_MODE_LENGTH:
            router_read(context->in1, &context->trt_num);
            router_read(context->in3, &context->TRall_num);
            context->d_len = context->TRall_num - 1;
            context->mode = LIDE_C_RRE_MODE_PROCESS;
        break;
        case LIDE_C_RRE_MODE_PROCESS:
            router_read_block(context->in2, context->TRall,
                                    context->TRall_num);
            lide_c_fifo_read(context->in4, &context->REtime);
            lide_c_fifo_read_block(context->in5, context->d, 
                                    context->d_len);
            totalUIs = 0;
            /*Get the refined estimation*/
            for(i = 0; i<context->TRall_num-1; i++){
                totalUIs += ((int)(context->d[i]/context->REtime+0.5));
            }
            context->RREtime = (context->TRall[context->d_len-1]-
                                context->TRall[0])/totalUIs;
            
            lide_c_fifo_write(context->out1, &context->RREtime);
            context->mode = LIDE_C_RRE_MODE_LENGTH;
        break;
        default:
            context->mode = LIDE_C_RRE_MODE_ERROR;
        break;
    }

    return;
    
}

void lide_c_RRE_terminate(lide_c_RRE_context_type *context){  	
    free(context);
}