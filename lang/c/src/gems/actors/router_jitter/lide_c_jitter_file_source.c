/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "lide_c_jitter_file_source.h"
#include "lide_c_util.h"

struct _lide_c_jitter_file_source_context_struct {
#include "lide_c_actor_context_type_common.h"
    FILE *fp;
    char *file;
    double data;
    router_port_pointer out1;
    router_context_pointer router_out1;
    //lide_c_fifo_pointer out1;
    //lide_c_fifo_pointer out2;
    //lide_c_fifo_pointer out3;
};


//lide_c_jitter_file_source_context_type *lide_c_jitter_file_source_new(
//        char *file, lide_c_fifo_pointer out1, lide_c_fifo_pointer out2, 
//        lide_c_fifo_pointer out3) 
lide_c_jitter_file_source_context_type *lide_c_jitter_file_source_new(
        char *file, router_port_pointer out1){

    lide_c_jitter_file_source_context_type *context = NULL;

    context = lide_c_util_malloc(sizeof(lide_c_jitter_file_source_context_type));
    context->enable =
            (lide_c_actor_enable_function_type)lide_c_jitter_file_source_enable;
    context->invoke =
            (lide_c_actor_invoke_function_type)lide_c_jitter_file_source_invoke;
    context->file = file;
    //printf("source file:%s\n", context->file);
    context->fp = lide_c_util_fopen((const char *)context->file, "r");
    if (fscanf(context->fp, "%lf", &context->data) != 1) { 
            /* End of input */
        context->mode = LIDE_C_JITTER_FILE_SOURCE_MODE_ERROR;
        fprintf(stderr, "cannot open file %s\n", context->file);
        context->data = 0.0;
        
    }else{
        //printf("first data:%lf\n", context->data);
        context->mode = LIDE_C_JITTER_FILE_SOURCE_MODE_WRITE;
    }
 
    //context->out1 = out1;
    //context->out2 = out2;
    //context->out3 = out3;
    context->out1 = out1;
    context->router_out1 = router_port_get_router_context(context->out1);
    return context;
}

bool lide_c_jitter_file_source_enable(
        lide_c_jitter_file_source_context_type *context) {
    bool result = false;

    switch (context->mode) {
    case LIDE_C_JITTER_FILE_SOURCE_MODE_WRITE:
    /*
        result = (lide_c_fifo_population(context->out1) < 
                lide_c_fifo_capacity(context->out1)) && 
                (lide_c_fifo_population(context->out2) < 
                lide_c_fifo_capacity(context->out2)) && 
                (lide_c_fifo_population(context->out3) < 
                lide_c_fifo_capacity(context->out3)) ;
     */
        result = (router_population(context->router_out1) <
                router_capacity(context->router_out1));
        break;
    case LIDE_C_JITTER_FILE_SOURCE_MODE_ERROR:
        result = false;
        break;
    default:
        result = false;
        break;
    }
    //printf("jitter source enable: %d\n", result);
    return result;
}

void lide_c_jitter_file_source_invoke(
    lide_c_jitter_file_source_context_type *context) {

    switch (context->mode) {
    case LIDE_C_JITTER_FILE_SOURCE_MODE_WRITE:
        //lide_c_fifo_write(context->out1, &context->data);
        //lide_c_fifo_write(context->out2, &context->data);
        //lide_c_fifo_write(context->out3, &context->data);
        router_write(context->out1, &context->data);
        //printf("data:%lf\n", context->data);
        if (fscanf(context->fp, "%lf", &context->data) != 1) { 
            /* End of input */
            fprintf(stderr, "end of input\n");
            context->mode = LIDE_C_JITTER_FILE_SOURCE_MODE_ERROR;
            return;
        } 
        //printf("new data:%lf\n", context->data);
        context->mode = LIDE_C_JITTER_FILE_SOURCE_MODE_WRITE;
        break;
    case LIDE_C_JITTER_FILE_SOURCE_MODE_ERROR:
        context->mode = LIDE_C_JITTER_FILE_SOURCE_MODE_WRITE;
        break;
    default:
        context->mode = LIDE_C_JITTER_FILE_SOURCE_MODE_WRITE;
        break;
    }
    
}

void lide_c_jitter_file_source_terminate(lide_c_jitter_file_source_context_type *context) {
    fclose(context->fp);
    free(context);
}

