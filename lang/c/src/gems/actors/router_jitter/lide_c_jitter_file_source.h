#ifndef _lide_c_jitter_file_source_h
#define _lide_c_jitter_file_source_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
/* Description: It is a file source actor which reads a file and
store the contents into a port that is connected to a router.
*/
#include "lide_c_actor.h"
#include "lide_c_fifo.h"
#include "router.h"

/* Actor modes */
#define LIDE_C_JITTER_FILE_SOURCE_MODE_WRITE        1
#define LIDE_C_JITTER_FILE_SOURCE_MODE_ERROR        2

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with file source objects. */
struct _lide_c_jitter_file_source_context_struct;
typedef struct _lide_c_jitter_file_source_context_struct
        lide_c_jitter_file_source_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

/*****************************************************************************
Construct function of the lide_c_jitter_file_source actor. Create a new
lide_c_jitter_file_source with the specified file pointer, and the specified output
FIFO pointer. Data format in the input file should be all integers.
*****************************************************************************/
lide_c_jitter_file_source_context_type *lide_c_jitter_file_source_new(
        char *file, router_port_pointer out1);

/*****************************************************************************
Enable function of the lide_c_jitter_file_source actor.
*****************************************************************************/

bool lide_c_jitter_file_source_enable(
        lide_c_jitter_file_source_context_type *context);

/*****************************************************************************
Invoke function of the lide_c_jitter_file_source actor.
*****************************************************************************/

void lide_c_jitter_file_source_invoke(
        lide_c_jitter_file_source_context_type *context);

/*****************************************************************************
Terminate function of the lide_c_jitter_file_source actor.
*****************************************************************************/
void lide_c_jitter_file_source_terminate(
        lide_c_jitter_file_source_context_type *context);

#endif
