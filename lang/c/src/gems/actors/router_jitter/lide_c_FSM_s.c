/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lide_c_FSM_s.h"
#include "lide_c_util.h"

/*
in1: state
out10: FSM->TRT, trt_num
out11: FSM->RE, trt_num
out12: FSM->RRE, trt_num
out13: FSM->CRE, trt_num
out20: FSM->TRT, TRindex
out21: FSM->CRE, TRindex
*/

struct _lide_c_FSM_s_context_struct {
#include "lide_c_actor_context_type_common.h"
    /* Actor interface ports. */
    /*input sequence*/
    lide_c_fifo_pointer in1; /*STR-->FSM, state*/

    /*Output sequence*/
    //lide_c_fifo_pointer out10; /*FSM->TRT, trt_num*/
    //lide_c_fifo_pointer out11; /*FSM->RE, trt_num*/
    //lide_c_fifo_pointer out12; /*FSM->RRE, trt_num*/
    //lide_c_fifo_pointer out13; /*FSM->CRE, trt_num*/
    //lide_c_fifo_pointer out20; /*FSM->TRT, TRindex*/
    //lide_c_fifo_pointer out21; /*FSM->CRE, TRindex*/
    
    router_port_pointer  out1; /*trt_num*/
    router_context_pointer router_out1;
    
    router_port_pointer  out2; /*TRindex*/
    router_context_pointer router_out2;
    
    /* Actor Parameters*/
    int Ws; /*Window Size*/
    double *vol;
    int *state;
    int *TR;
    int *TRindex;
    int trt_num;
};

lide_c_FSM_s_context_type *lide_c_FSM_s_new(int Ws, lide_c_fifo_pointer in1, 
        router_port_pointer out1, router_port_pointer out2){
    int i;
    lide_c_FSM_s_context_type *context = NULL;
    context = lide_c_util_malloc(sizeof(lide_c_FSM_s_context_type));
    context->enable = (lide_c_actor_enable_function_type)lide_c_FSM_s_enable;
    context->invoke = (lide_c_actor_invoke_function_type)lide_c_FSM_s_invoke;
    context->in1 = in1;
    /*REVISE HERE*/
    //context->out10 = out10;
    //context->out11 = out11;
    //context->out12 = out12;
    //context->out13 = out13;
    //context->out20 = out20;
    //context->out21 = out21;
    
    context->out1 = out1;
    context->router_out1 = router_port_get_router_context(context->out1);
    context->out2 = out2;
    context->router_out2 = router_port_get_router_context(context->out2);
    
    context->mode = LIDE_C_FSM_s_MODE_PROCESS;
    
    /*Load table and Parameter*/
    context->Ws = Ws;
    /*Allocate memory for sorting*/
    context->trt_num = 0;
    context->state = (int*)malloc(sizeof(int)*Ws);
    context->TR = (int*)malloc(sizeof(int)*Ws);
    context->TRindex = (int*)malloc(sizeof(int)*Ws);
    /* Initialize the array*/
    for (i=0; i<Ws; i++){   
        context->TR[i] = 0; 
        context->TRindex[i] = 0; 
    }
    return context;
}

bool lide_c_FSM_s_enable(lide_c_FSM_s_context_type *context) {
    bool result = false;
    switch (context->mode) {
        case LIDE_C_FSM_s_MODE_PROCESS:
        /*
            result = (lide_c_fifo_population(context->in1) >= context->Ws) &&
                    (lide_c_fifo_population(context->out10) < 
                    lide_c_fifo_capacity(context->out10)) &&
                    (lide_c_fifo_population(context->out11) < 
                    lide_c_fifo_capacity(context->out11)) &&
                    (lide_c_fifo_population(context->out12) < 
                    lide_c_fifo_capacity(context->out12)) &&
                    (lide_c_fifo_population(context->out13) < 
                    lide_c_fifo_capacity(context->out13)) &&
                    (lide_c_fifo_population(context->out20) + context->Ws <= 
                    lide_c_fifo_capacity(context->out20)) &&
                    (lide_c_fifo_population(context->out21) + context->Ws <= 
                    lide_c_fifo_capacity(context->out21));
        */
        result = (lide_c_fifo_population(context->in1) >= context->Ws) && 
                    (router_population(context->router_out1) <
                    router_capacity(context->router_out1)) &&
                    (router_population(context->router_out2) <
                    router_capacity(context->router_out2));
        break;
        default:
            result = false;
        break;
    }
    return result;
}

void lide_c_FSM_s_invoke(lide_c_FSM_s_context_type *context) {
    int i,j;
    int *p;
    int *q;

    /*Load data from three fifo*/
    lide_c_fifo_read_block(context->in1, context->state, context->Ws);
    p = context->state;    
    q = p+1;
    
    j = 0;

    /*Check transitions and mark them and their index*/
    for (i = 0; i<context->Ws-1; i++){
        context->TR[i] = (context->state[i] + q[i] == 1);
    }
    for (i = 0; i<context->Ws-1; i++){		
        context->TRindex[j] = context->TR[i]  * i;
        j = j + context->TR[i];
    }
    
    context->trt_num = j;

    /*Store data to three fifo*/
    /*
    lide_c_fifo_write(context->out10, &context->trt_num);
    lide_c_fifo_write(context->out11, &context->trt_num);
    lide_c_fifo_write(context->out12, &context->trt_num);
    lide_c_fifo_write(context->out13, &context->trt_num);
    lide_c_fifo_write_block(context->out20, context->TRindex, context->trt_num);
    lide_c_fifo_write_block(context->out21, context->TRindex, context->trt_num);
    */
    router_write(context->out1, &context->trt_num);
    router_write_block(context->out2, context->TRindex, context->trt_num);
    
    return;
    
}

void lide_c_FSM_s_terminate(lide_c_FSM_s_context_type *context){  
    free(context->TR);  
    free(context->TRindex);  		
    free(context);
}