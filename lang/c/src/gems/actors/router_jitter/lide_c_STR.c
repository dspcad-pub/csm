/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lide_c_STR.h"
#include "lide_c_util.h"

struct _lide_c_STR_context_struct {
#include "lide_c_actor_context_type_common.h"
    /* Actor interface ports. */
    /*input sequence*/
    //lide_c_fifo_pointer in1; /*input*/
    //lide_c_fifo_pointer in2; /*medium*/
    router_port_pointer  in1;/*input*/
    router_context_pointer router_in1;
    
    router_port_pointer  in2;/*medium*/
    router_context_pointer router_in2;
    
    /*output sequence*/
    lide_c_fifo_pointer out1;/*state*/

    /* Actor Parameters*/
    double *vol;
    int Ws;
    double high,low;
    int *state;
};

lide_c_STR_context_type *lide_c_STR_new(int Ws, router_port_pointer in1,
                            router_port_pointer in2,
                            lide_c_fifo_pointer out1){
    int i;
    lide_c_STR_context_type *context = NULL;
    context = lide_c_util_malloc(sizeof(lide_c_STR_context_type));
    context->enable = (lide_c_actor_enable_function_type)lide_c_STR_enable;
    context->invoke = (lide_c_actor_invoke_function_type)lide_c_STR_invoke;
    //context->in1 = in1;
    //context->in2 = in2;
    
    context->in1 = in1;
    context->router_in1 = router_port_get_router_context(context->in1);
    
    context->in2 = in2;
    context->router_in2 = router_port_get_router_context(context->in2);
    
    context->out1 = out1;

    context->mode = 1;
    /*Load table and Parameter*/
    context->Ws = Ws;
    context->high = 0.0;
    context->low = 0.0;
    context->vol = NULL;
    /*Allocate memory for sorting*/
    context->vol = (double*)malloc(sizeof(double)*Ws);
    context->state = (int*)malloc(sizeof(int)*Ws);
    /* Initialize the array*/
    for (i=0; i<Ws; i++){   
        context->state[i] = 2; 
        context->vol[i] = 0.0; 
    }
    return context;
}

bool lide_c_STR_enable(lide_c_STR_context_type *context) {
    bool result = false;
    switch (context->mode) {
        case LIDE_C_STR_MODE_PROCESS:
        /*
            result = (lide_c_fifo_population(context->in1) >= context->Ws) &&
                    (lide_c_fifo_population(context->in2) >= 1) &&
                    (lide_c_fifo_population(context->out1) + context->Ws <= 
                    lide_c_fifo_capacity(context->out1));
        */
        
        result = (router_port_population(context->in1) >= context->Ws) &&
                    (router_port_population(context->in2) >= 1) &&
                    (lide_c_fifo_population(context->out1) + context->Ws <= 
                    lide_c_fifo_capacity(context->out1));
        
        
        break;
        default:
            result = false;
        break;
    }
    return result;
}

void lide_c_STR_invoke(lide_c_STR_context_type *context) {

    double medium;
    int i;
    /*Load data from input fifo*/
    //lide_c_fifo_read_block(context->in1, context->vol, context->Ws);
    router_read_block(context->in1, context->vol, context->Ws);
    //lide_c_fifo_read(context->in2, &medium);
    router_read(context->in2, &medium);

    /*Assign the state for every input*/
    for (i = 0; i<context->Ws; i++){
        context->state[i] = (context->vol[i] >= medium);
    }
    /*Store results*/
    lide_c_fifo_write_block(context->out1, context->state, context->Ws);

    return;
}

void lide_c_STR_terminate(lide_c_STR_context_type *context){   
    free(context->state);  		
    free(context->vol);  		
    free(context);
}