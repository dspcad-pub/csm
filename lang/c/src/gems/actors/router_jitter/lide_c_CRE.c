/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "lide_c_CRE.h"
#include "lide_c_util.h"

struct _lide_c_CRE_context_struct {
#include "lide_c_actor_context_type_common.h"
    /* Actor interface ports. */
    /*input sequence*/
    lide_c_fifo_pointer in3; /* TRT array */
    lide_c_fifo_pointer in6; /* RREtime*/
    
    router_port_pointer  in1; /* trt_num */
    router_context_pointer router_in1;
    
    router_port_pointer  in2;/* TRindex array */
    router_context_pointer router_in2;
    
    router_port_pointer  in4;/* TRall array*/
    router_context_pointer router_in4;
    
    router_port_pointer  in5;/* TRall_num*/
    router_context_pointer router_in5;
    
    router_port_pointer  out1; /* CREtime*/
    router_context_pointer router_out1;
    lide_c_fifo_pointer out2; /* coefficient a*/
    
    /* Actor Parameters*/
    int Ws; /*Window Size*/
    int k,len;
    int Num;
    int rpro;
    int TRlength;
    int *TRindex;
    int Pindex;
    int Pmid;
    double *TRT,*TRall;
    double *TRT_all;
    double *d;
    double a, b;
    double *phase_all;
    double *phase;
    double *phase_correct;
    double *phase_error;
    double RREtime, CREtime;
    int trt_num, TRall_num;
};

lide_c_CRE_context_type *lide_c_CRE_new(int Ws, int k, 
                        router_port_pointer in1,
                        router_port_pointer in2,
                        lide_c_fifo_pointer in3, 
                        router_port_pointer in4,
                        router_port_pointer in5,
                        lide_c_fifo_pointer in6, 
                        router_port_pointer out1,
                        lide_c_fifo_pointer out2){
    int i;
    lide_c_CRE_context_type *context = NULL;
    context = lide_c_util_malloc(sizeof(lide_c_CRE_context_type));
    context->enable = (lide_c_actor_enable_function_type)lide_c_CRE_enable;
    context->invoke = (lide_c_actor_invoke_function_type)lide_c_CRE_invoke;
    //context->in1 = in1;
    //context->in2 = in2;
    context->in3 = in3;
    //context->in4 = in4;
    //context->in5 = in5;
    context->in6 = in6;
    //context->out1 = out1;
    //context->out2 = out2;
    //context->out3 = out3;
    
    context->in1 = in1;
    context->router_in1 = router_port_get_router_context(context->in1);
    
    context->in2 = in2;
    context->router_in2 = router_port_get_router_context(context->in2);

    
    context->in4 = in4;
    context->router_in4 = router_port_get_router_context(context->in4);
    
    context->in5 = in5;
    context->router_in5 = router_port_get_router_context(context->in5);
    
    context->out1 = out1;
    context->router_out1 = router_port_get_router_context(context->out1);
    
    context->out2 = out2;

    context->mode = LIDE_C_CRE_MODE_LENGTH;
    /*Load table and Parameter*/
    context->Ws = Ws;
    context->k = k;
    context->Num = 500;
    context->rpro = 200;
    context->Pmid = context->Num/2;
    context->Pindex = 0;
    context->trt_num = 0;
    context->TRall_num = 0;
    context->RREtime = 0.0;
    context->CREtime = 0.0;
    context->a = 0.0;
    context->b = 0.0;
    
    context->TRall = (double *)malloc(sizeof(double)*CRE_DATA_MAX);
    context->TRT = (double *)malloc(sizeof(double)*context->Ws);
    context->TRindex = (int *)malloc(sizeof(int)*context->Ws);
    
    context->phase = (double *)malloc(sizeof(double)*context->Ws);
    context->phase_correct = (double *)malloc(sizeof(double)*context->Num);
    context->phase_error = (double *)malloc(sizeof(double)*context->Num);
    context->phase_all = (double *)malloc(sizeof(double)*context->Num);
    context->TRT_all = (double *)malloc(sizeof(double)*context->Num);
    /* Initialize array*/
    for (i=0; i<context->Ws; i++){
        context->phase[i] = 0.0; 
        context->TRindex[i] = 0;
        context->TRT[i] = 0.0;
    }
    
    for (i=0; i<CRE_DATA_MAX; i++){
        context->TRall[i] = 0.0; 
    }
        
    for (i=0; i<context->Num; i++){
        context->phase_all[i] = 0.0; 
        context->TRT_all[i] = 0.0; 
        context->phase_correct[i] = 0.0; 
        context->phase_error[i] = 0.0; 
    }
    return context;
}

bool lide_c_CRE_enable(lide_c_CRE_context_type *context) {
    bool result = false;
    switch (context->mode) {
        case LIDE_C_CRE_MODE_LENGTH:
            result = (router_port_population(context->in1) >= 1) &&
                    (router_port_population(context->in5) >= 1);
        break;
        
        case LIDE_C_CRE_MODE_PROCESS:
            result = (router_port_population(context->in2) >=
                    context->trt_num) &&
                    (lide_c_fifo_population(context->in3) 
                    >= context->trt_num) &&
                    (router_port_population(context->in4)
                    >= context->TRall_num) &&
                    (lide_c_fifo_population(context->in6) >= 1) && 
                    (router_population(context->router_out1) <
                    router_capacity(context->router_out1)) &&
                    (lide_c_fifo_population(context->out2) < 
                    lide_c_fifo_capacity(context->out2));
        break;
        default:
            result = false;
        break;
    }
    return result;
}

void lide_c_CRE_invoke(lide_c_CRE_context_type *context) {

    int i,N,ptmp = 0;
    int index = 0;
    int *rp;
    int *p;
    double *fp;
    int	*intp;
    double *phase_copy;
    double *TRT_copy;
    double high,low,medium,REtime,RREtime,dlen;
    double a,b,xy,x,y,x2,xavg,yavg,sum,CREtime;
    double fN;
    const int pindex_copy = context->Pindex;
    
    rp = (int *)malloc(sizeof(int)*context->Num);
    p = (int *)malloc(sizeof(int)*context->Num);
    if (rp == NULL || p == NULL){
        fprintf(stderr, "allocate in CRE failed\n");
        exit(-1);
    }
    
    switch (context->mode) {
        case LIDE_C_CRE_MODE_LENGTH:
            router_read(context->in1, &context->trt_num);
            router_read(context->in5, &context->TRall_num);
            context->mode = LIDE_C_CRE_MODE_PROCESS;
        break;
        
        case LIDE_C_CRE_MODE_PROCESS:

            router_read_block(context->in2, context->TRindex,
                                    context->trt_num);
            lide_c_fifo_read_block(context->in3, context->TRT, 
                                    context->trt_num);
            router_read_block(context->in4, context->TRall,
                                    context->TRall_num);
            lide_c_fifo_read(context->in6, &context->RREtime);
            
            /*Revise TRT*/
            ptmp = context->Ws*context->k;
            for(i = 0; i<context->trt_num; i++){
                context->TRT[i] = context->TRT[i] + ptmp;
            }
            /* Compute the phase at current transition*/
            for(i = 0; i<context->trt_num; i++){
                context->phase[i] = context->TRT[i]-((int)(context->TRT[i]/
                                    context->RREtime+0.5))*context->RREtime;
            }
            
            /*Update context->phase_all and context->TRT_all*/
            if (context->Pindex + context->trt_num <= context->Num){
                phase_copy = context->phase_all + pindex_copy;
                TRT_copy = context->TRT_all + pindex_copy;
                for(i = 0; i< context->trt_num; i++){
                    phase_copy[i] = context->phase[i];
                    TRT_copy[i] = context->TRT[i];
                }

                context->Pindex = context->Pindex+context->trt_num;	
            }else if (context->Pindex < context->Num){
                for(i = context->Pindex; i< context->Num; i++){
                    context->phase_all[i] = context->phase[i-context->Pindex];
                    context->TRT_all[i] = context->TRT[i-context->Pindex];
                }
                index = context->Pindex + context->trt_num - context->Num;
                context->Pindex = context->Num;
                
                
            }else{
                index = context->trt_num;
            }
            if (index > 0){
                if (context->trt_num >= context->Pmid){
                    ptmp = context->trt_num - context->Pmid;
                    for(i = 0; i < context->Pmid;i++){
                        context->phase_all[i+context->Pmid] = context->phase[i+ptmp];
                        context->TRT_all[i+context->Pmid] = context->TRT[i+ptmp];
                    }
                    index = 1;
                }else{
                    ptmp = context->Pmid - (context->trt_num);
                    /*Move Array*/
                    for(i = 0; i<ptmp; i++){
                        context->phase_all[i+context->Pmid] = context->phase[i+context->Pmid+context->trt_num];
                        context->TRT_all[i+context->Pmid] = context->TRT[i+context->Pmid+context->trt_num];
                    }
                    /*Update new data*/
                    for(i = 0; i < context->trt_num; i++){
                        context->phase_all[i+ptmp] = context->phase[i];
                        context->TRT_all[i+ptmp] = context->TRT[i];
                    }
                    index = 2;
                }
            }

            N = context->Pindex;
            /*X and Y average; x sum and y sum*/
            /*
                X: Transition Time
                Y: Phase at transition time
            */
            sum = 0.0;
            for (i = 0; i<N; i++){
                sum += context->phase_all[i];
            }
            y = sum;
            yavg = sum/N;

            sum = 0.0;

            for (i = 0; i<N; i++){
                sum += context->TRT_all[i];
            }
            x = sum;
            xavg = sum/N;
            
            /*X square*/
            sum = 0.0;
            for (i = 0; i<N; i++){
                sum += context->TRT_all[i] * context->TRT_all[i];
            }
            x2 = sum;
            /*xy*/
            sum = 0.0;
            for(i = 0; i<N; i++){
                sum += context->TRT_all[i] * context->phase_all[i];
            }
            xy = sum;
            /*Compute a and b*/
            b = (N*xy-x*y)/(N*x2-x*x);
            a = yavg-xavg*b;

            /*Correct phase*/
            for(i = 0; i<N; i++){
                context->phase_correct[i] = a + b*context->TRT_all[i];
            }
            for(i = 0; i<N; i++){
                context->phase_error[i] = context->phase_correct[i] - context->phase[i];		
            }
            context->CREtime = context->RREtime+context->RREtime*b;

            context->k = context->k + 1;
            context->a = a;
            context->b = b;
            
            router_write(context->out1, &context->CREtime);
            //lide_c_fifo_write(context->out2, &context->CREtime);
            //lide_c_fifo_write(context->out3, &context->a);
            lide_c_fifo_write(context->out2, &context->a);
            free(rp);
            free(p);

            context->mode = LIDE_C_CRE_MODE_LENGTH;
        break;
        default:
            context->mode = LIDE_C_CRE_MODE_ERROR;
        break;
    }
    
    return;
    
}

void lide_c_CRE_terminate(lide_c_CRE_context_type *context){  	
    free(context->phase);
    free(context->phase_correct);
    free(context->phase_error);
    free(context->phase_all);
    free(context->TRT_all);
    free(context->TRindex);
    free(context->TRT);
    free(context->TRall);
    free(context);
}