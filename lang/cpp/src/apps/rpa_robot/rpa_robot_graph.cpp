
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "rpa_robot_graph.h"

#define NAME_LENGTH 20

using namespace std;

rpa_robot_graph::rpa_robot_graph(char *in_file, char *policy_file, char *out_file) {

    this->in_file = in_file;
    this->policy_file = policy_file;
    this->out_file = out_file;
    this->actor_count = ACTOR_COUNT;
    this->fifo_count = FIFO_COUNT;

    /* Initialize fifos. */
    int token_size;
    for (int i = 0; i< this->fifo_count; i++){
        this->fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new
                (BUFFER_CAPACITY,sizeof(int),i));
    }
    /* Initialize source array and sink array */
    this->source_array.reserve(this->fifo_count);
    this->sink_array.reserve(this->fifo_count);

    /* Create and connect the actors. */
    cout << "inint"<< endl;

    /* x source actor */
    this->actors.push_back(new file_source_int
            (this->fifos[FIFO_SRC_ROBOT], this->in_file));
    this->descriptors.push_back((char*)"actor initial energy src");
    this->actors[ACTOR_SOURCE]->connect((welt_cpp_graph*)this);
    this->actors[ACTOR_SOURCE]->actor_set_index(ACTOR_SOURCE);
    cout << "inint src"<< endl;

    /* robot actor */
    this->robot_actor =new robot(this->fifos[FIFO_SRC_ROBOT],
                                 this->fifos[FIFO_ROBOT_RPA],
                                 this->fifos[FIFO_ROBOT_SNK]);
    cout << "robot src"<< endl;
    this->actors.push_back(robot_actor);
    this->descriptors.push_back((char*)"actor en");
    (this->actors[ACTOR_ROBOT])->connect((welt_cpp_graph*)this);
    this->actors[ACTOR_ROBOT]->actor_set_index(ACTOR_ROBOT);
    cout << "before sink"<< endl;

    /* sink actor */
    this->actors.push_back(new file_sink_robot
                                   (fifos[FIFO_ROBOT_SNK], this->out_file));
    this->descriptors.push_back((char*)"actor sink");
    this->actors[ACTOR_SNK]->connect((welt_cpp_graph*)this);
    this->actors[ACTOR_SNK]->actor_set_index(ACTOR_SNK);
    cout << "end of actors"<< endl;

    /* rpa_action actor */
    robot_action_actor = new robot_action(this->robot_actor);
    cout << "rpa action"<< endl;

    /* RPA */
    this->actors.push_back(new rpa(fifos[FIFO_ROBOT_RPA], (action*)
    robot_action_actor, this->policy_file));
    this->actors[ACTOR_RPA]->connect((welt_cpp_graph*)this);
    this->actors[ACTOR_RPA]->actor_set_index(ACTOR_RPA);
}

void rpa_robot_graph::scheduler() {
    //welt_cpp_util_simple_scheduler(this->actors, this->actor_count, this->
    //                               descriptors);
    /* static scheduler */
    (this->actors[ACTOR_SOURCE])->invoke();
    //run robot,sink and rpa actors.
    welt_cpp_util_simple_scheduler(this->actors, ACTOR_ROBOT,
            ACTOR_RPA, this->descriptors);

}

/* destructor */
rpa_robot_graph::~rpa_robot_graph() {
    cout << "delete add_cdsg graph" << endl;
}

