/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#ifndef _rpa_robot_graph_h
#define _rpa_robot_graph_h

extern "C" {
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_util.h"
}
#include "action.h"
#include "file_source_int.h"
#include "robot_action.h"
#include "robot.h"
#include "rpa.h"
#include "file_sink_robot.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"

#define BUFFER_CAPACITY (1024)

/* An enumeration of the actors in this application. */

#define ACTOR_SOURCE    (0)
#define ACTOR_ROBOT      (1)
#define ACTOR_SNK        (2)
#define ACTOR_RPA        (3)

#define FIFO_SRC_ROBOT      (0)
#define FIFO_ROBOT_RPA      (1)
#define FIFO_ROBOT_SNK      (2)

/* The total number of actors in the application. */
#define ACTOR_COUNT     (4)
#define FIFO_COUNT      (3)

/* Overview: This is a graph ADT for rpa_robot application.
*/

class rpa_robot_graph : public welt_cpp_graph {
public:
    rpa_robot_graph(char *in_file, char *policy_file, char *out_file);

    ~rpa_robot_graph();

    void scheduler() override;

private:
    char *in_file;
    char *out_file;
    char * policy_file;
    robot* robot_actor;
    robot_action* robot_action_actor;
};

#endif
