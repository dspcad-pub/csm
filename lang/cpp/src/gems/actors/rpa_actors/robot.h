#ifndef _robot_h
#define _robot_h

#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "welt_cpp_graph.h"

#include <string>
#include <iostream>
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
/*******************************************************************************
Overview: This actor consumes token from file source actor and set the energy
constraint for rpa_robot application. Then it decrements the energy level
based on the current policy. It outputs information about current states, the
remaining energy and the inputs to the sink actor.
*******************************************************************************/

/* Actor modes */
#define ROBOT_MODE_SET_CONSTRAINT   (1)
#define ROBOT_MODE_PROCESS   (2)
#define ROBOT_MODE_COMPLETE   (3)

/* Port index*/
#define ROBOT_PORT_X    (0)
#define ROBOT_PORT_OUT_RPA  (1)
#define ROBOT_PORT_OUT  (2)

#define ARM_STILL_ROBOT (3)
#define ARM_SWINING_ROBOT (8)

#define LEG_WALKING_ROBOT (7)
#define LEG_RUNNING_ROBOT (10)

#define ROBOT_MAX_FIFO_COUNT (3)

class robot: public welt_cpp_actor{
public:

    robot(welt_c_fifo_pointer input, welt_c_fifo_pointer out_rpa ,
          welt_c_fifo_pointer out);

    ~robot() override;

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    int get_remaining_energy();

    int get_initial_energy();

    int calculate_energy();

    void set_leg_state(int new_state);

    void set_arm_state(int new_state);

    int get_input();

private:

    int initial_energy;

    int remaining_energy;

    int curr_input;

    int* state;

    /* Input ports. */
    welt_c_fifo_pointer input;

    /* Output port. */
    welt_c_fifo_pointer out;
    welt_c_fifo_pointer out_rpa;

};

#endif
