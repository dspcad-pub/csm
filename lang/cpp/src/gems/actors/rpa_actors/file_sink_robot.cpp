/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include <typeinfo>
#include "file_sink_robot.h"

using namespace std;

file_sink_robot::file_sink_robot (welt_c_fifo_pointer in_fifo, char*
output_file_name) {
    this->in = (welt_c_fifo_pointer)in_fifo;
    this->file_name = output_file_name;
    this->out_rpa= out_rpa;
    reset();
	/* Set maximum port count */
	this->actor_set_max_port_count(FILE_SINK_INT_MAX_FIFO_COUNT);
	/* Set portrefs*/
	this->actor_add_portrefs(&this->in);

}

bool file_sink_robot::enable() {
    bool result = false;
    switch (mode) {
        case FILE_SINK_INT_MODE_PROCESS:
            result = (welt_c_fifo_population(in) > 0);
            break;
        case FILE_SINK_INT_MODE_ERROR:
            /* Modes that don't produce or consume data are always enabled. */
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void file_sink_robot::invoke() {
    switch (mode) {
        case FILE_SINK_INT_MODE_PROCESS: {

            welt_c_fifo_read(this->in, &data);
            outStream << "remaining energy: " << data  << endl;
            welt_c_fifo_read_block(this->in, values, 2);
            outStream << "states arm:" << values[0] <<", leg:" << values[1]
            << endl;
            cout << "states arm:" << values[0] <<", leg:" << values[1]
                      << endl;
            mode = FILE_SINK_INT_MODE_PROCESS;
            break;
        }
        case FILE_SINK_INT_MODE_ERROR: {
            /* Remain in the same mode, and do nothing. */
            break;
        }
        default:
            mode = FILE_SINK_INT_MODE_ERROR;
        break;
    }
}

void file_sink_robot::reset() {
    /* Close the file it is open, and then open/re-open the file so that
    subsequent invocations of the PROCESS MODE write from the beginning of 
    the file. 
    */
    if (outStream.is_open()) {
        outStream.close();
    }
    outStream.open(file_name);
    if (outStream.fail()) {
        cerr << "Could not open file \"" << file_name << "\"" << endl;
        mode = FILE_SINK_INT_MODE_ERROR;
    } else {
        mode = FILE_SINK_INT_MODE_PROCESS;
    }
}

file_sink_robot::~file_sink_robot() {
    outStream.close();
}

void file_sink_robot::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);

}


