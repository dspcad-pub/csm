/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include "robot.h"

robot::robot(welt_c_fifo_pointer input, welt_c_fifo_pointer out_rpa,
             welt_c_fifo_pointer out){

    /* Input port. */
    this->input = input;
    /* Output port. */
    this->out = out;
    this->out_rpa = out_rpa;
    this->state = (int*)malloc(2 * sizeof(int));
    this->mode = ROBOT_MODE_SET_CONSTRAINT;
    this->actor_set_max_port_count(ROBOT_MAX_FIFO_COUNT);
    /* Set portrefs*/
    this->actor_add_portrefs(&this->input);
    this->actor_add_portrefs(&this->out_rpa);
    this->actor_add_portrefs(&this->out);
}

bool robot::enable() {
    bool result = false;
    switch (this->mode) {
        case ROBOT_MODE_SET_CONSTRAINT:
            result = (welt_c_fifo_population(this->input) >= 0);
            break;

        case ROBOT_MODE_PROCESS:
            result = (this->remaining_energy>=0);
            break;

        case ROBOT_MODE_COMPLETE:
            result = false;
            break;

        default:
            result = false;
            break;

    }
    return result;
}

void robot::invoke() {
    int i = 0;
    int sum = 0;
    int x_value = 0;

    switch (this->mode) {
        case ROBOT_MODE_SET_CONSTRAINT:
            welt_c_fifo_read(this->input, &this->initial_energy);
            this->remaining_energy = this->initial_energy;
            this->mode = ROBOT_MODE_PROCESS;
            break;

        case ROBOT_MODE_PROCESS:
            this->remaining_energy -= this->calculate_energy();

            if (this->remaining_energy<0){
                this->mode = ROBOT_MODE_COMPLETE;
                cout << "ROBOT_MODE_COMPLETE" << endl;
            }else{
                this->curr_input = this->get_input();
                welt_c_fifo_write(this->out_rpa, &this->curr_input);
                welt_c_fifo_write(this->out, &this->remaining_energy);
                welt_c_fifo_write_block(this->out, (this->state), 2);

                this->mode = ROBOT_MODE_PROCESS;
            }
            break;
        default:
            this->mode = ROBOT_MODE_PROCESS;
            break;
    }
    return;
}

void robot::reset() {
    this-> mode = ROBOT_MODE_SET_CONSTRAINT ;
}

void robot::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;
    /* Register the port in enclosing graph. */
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = ROBOT_PORT_X;
    graph->add_connection( this, port_index, direction);

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = ROBOT_PORT_OUT_RPA;
    graph->add_connection( this, port_index, direction);

    /* output 2*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = ROBOT_PORT_OUT;
    graph->add_connection( this, port_index, direction);
}

robot::~robot(){

}

int robot::calculate_energy(){
    int arms[2] ={ARM_STILL_ROBOT, ARM_SWINING_ROBOT};
    int legs[2] = {LEG_WALKING_ROBOT, LEG_RUNNING_ROBOT};
    return arms[this->state[0]] + legs[this->state[1]];
}

void robot::set_leg_state(int new_state){
    this->state[1] = new_state;
}

void robot::set_arm_state(int new_state){
    this->state[0] = new_state;
}

int robot::get_remaining_energy(){
    return this->remaining_energy;
}

int robot::get_initial_energy(){
    return this->initial_energy;
}

int robot::get_input(){
    if ((this->remaining_energy)< 0.2 *this->initial_energy){
        return 0;
    }else if((this->remaining_energy)>=0.2*this->initial_energy &&
            (this->remaining_energy)<0.4*this->initial_energy ){
        return 1;
    }else if((this->remaining_energy)>=0.4*this->initial_energy &&
            (this->remaining_energy)<0.6*this->initial_energy ){
        return 2;
    }else if((this->remaining_energy)>=0.6*this->initial_energy &&
            (this->remaining_energy)<0.8*this->initial_energy ){
        return 3;
    }else if((this->remaining_energy)>=0.8*this->initial_energy){
        return 4;
    }
}


