/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "robot_action.h"

using namespace std;

robot_action::robot_action(robot* robot_actor){
    this->robot_actor = robot_actor;
    this->reset();

}

void robot_action::execute(int index){
    this->curr_state = index;
    //this->curr_state=this->stt[this->curr_state][index];
    if (this->curr_state==0){
        this->robot_actor->set_arm_state(ROBOT_STATE_ARM_STILL);
        this->robot_actor->set_leg_state(ROBOT_STATE_LEG_WALKING);
    }else if(this->curr_state==1){
        this->robot_actor->set_arm_state(ROBOT_STATE_ARM_SWINGING);
        this->robot_actor->set_leg_state(ROBOT_STATE_LEG_WALKING);
    }else if(this->curr_state==2){
        this->robot_actor->set_arm_state(ROBOT_STATE_ARM_STILL);
        this->robot_actor->set_leg_state(ROBOT_STATE_LEG_RUNNING);
    }else{
        this->robot_actor->set_arm_state(ROBOT_STATE_ARM_SWINGING);
        this->robot_actor->set_leg_state(ROBOT_STATE_LEG_RUNNING);
    }
    if ((this->robot_actor)->get_remaining_energy()>=0){
        this->mode = ROBOT_RPA_MODE_PROCESS;
    }else{
        this->mode = ROBOT_RPA_MODE_COMPLETE;
    }
}

robot_action::~robot_action() {
}

void robot_action::reset(){
    (this->robot_actor)->set_arm_state(ROBOT_STATE_ARM_STILL);
    (this->robot_actor)->set_leg_state(ROBOT_STATE_LEG_WALKING);
    this->curr_state = 0;
    this->mode = ROBOT_RPA_MODE_LOAD_POLICY;
    return;
}

int robot_action::get_mode(){
    return this->mode;
}

