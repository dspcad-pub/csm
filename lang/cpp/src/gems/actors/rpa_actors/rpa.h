/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _RPA_H
#define _RPA_H

/*******************************************************************************
Overview: This actor (Reconfigure policy actor) reads policy and reconfigures
policy during runtime. It reads policy from txt file that contains the number
of inputs for the policy FSM, the number of states and state transition
matrix.
For example, policy.txt should be
5
4
0 0 1 1
0 1 1 2
1 2 3 3
2 2 3 3
2 3 3 3

This actor reads input through a FIFO and there is no output FIFO.

*******************************************************************************/
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <functional>

extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "welt_cpp_graph.h"
#include "welt_cpp_actor.h"
#include "action.h"

#define RPA_MAX_FIFO_COUNT (1)

class rpa: public welt_cpp_actor{

public:
        rpa(welt_c_fifo_pointer input, action* rpa_action, char* file_name);

        ~rpa() override;

        bool enable() override;

        void invoke() override;

        void reset() override;

        void connect(welt_cpp_graph *graph) override;

private:

        action* rpa_action;

        int **stateTransitionMatrix;

        int data;

        int curr_state;

        int curr_input;

        std::string line;

        ifstream inStream;

        welt_c_fifo_pointer input;

        welt_c_fifo_pointer output;

        char* file_name;

        int num_state;

        int num_input;

};


#endif