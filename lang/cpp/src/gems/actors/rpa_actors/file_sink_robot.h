#ifndef _file_sink_h
#define _file_sink_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/*******************************************************************************
Overview: This actor copies the tokens from fifo and store it into a file for
rpa-robot application.
*******************************************************************************/

#include <fstream>
#include <string>
extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}
#include "welt_cpp_graph.h"
#include "welt_cpp_actor.h"
#define FILE_SINK_INT_MAX_FIFO_COUNT (1)
/* Actor modes */
#define FILE_SINK_INT_MODE_PROCESS (1)
#define FILE_SINK_INT_MODE_COMPLETE (2)
#define FILE_SINK_INT_MODE_ERROR (3)

class file_sink_robot : public welt_cpp_actor{
public:
    /*************************************************************************
    Construct a file sink actor with the specified input FIFO connection
    and the specified output file name. 
    *************************************************************************/
    file_sink_robot(welt_c_fifo_pointer in, char*
    file_name);

    /*Destructor*/
    ~file_sink_robot() override;

    bool enable() override;

    void invoke() override;

    /* Reset the actor so that the output file is re-opened for writing,
    discarding any previously written contents in the file.
    */
    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    char* file_name;
    std::ofstream outStream;
    int data;
    welt_c_fifo_pointer in;
    welt_c_fifo_pointer out_rpa;
    int values[2];
};


#endif
