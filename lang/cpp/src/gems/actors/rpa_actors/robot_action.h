/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _ROBOT_RPA_h
#define _ROBOT_RPA_h

/*******************************************************************************
Overview: This actor enables configuration of policy. It inherits from action
class.


Example of State-transition table
state1={walking, still}
state2={walking, swing}
state3={running, still}
state4={running, swing}

            state0 state1 state2 state3
0<RE<=20    state0 state0 state1 state1
20<RE<=40   state0 state1 state1 state2
40<RE<=60   state1 state2 state3 state3
60<RE<=80   state2 state2 state3 state3
80<RE<=100  state2 state3 state3 state3

*******************************************************************************/
#include <typeinfo>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <functional>
extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}
#include "welt_cpp_graph.h"
#include "welt_cpp_actor.h"
#include "robot.h"
#include "action.h"

#define ROBOT_RPA_MAX_FIFO_COUNT (0)

#define ROBOT_STATE_ARM_STILL (0)
#define ROBOT_STATE_ARM_SWINGING (1)
#define ROBOT_STATE_LEG_WALKING (0)
#define ROBOT_STATE_LEG_RUNNING (1)

/* Actor modes */
#define ROBOT_RPA_MODE_LOAD_POLICY (1)
#define ROBOT_RPA_MODE_PROCESS (2)
#define ROBOT_RPA_MODE_COMPLETE (3)
#define ROBOT_RPA_MODE_ERROR (4)

class robot_action : public action{
public:
    robot_action(robot * robot_actor);

    void execute(int index) override;

    ~robot_action();

    void reset() ;

    int get_mode();

    int get_input();

private:
    int mode;

    robot * robot_actor;
    int remaining_energy;
    int initial_energy;
    int curr_state;
    int **stt;
    int num_state;
    int num_input;


};

#endif
