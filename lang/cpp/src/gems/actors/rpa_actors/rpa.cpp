/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "rpa.h"

rpa::rpa(welt_c_fifo_pointer input, action* rpa_action, char* file_name){
    /* RPA action class */
    this->rpa_action=rpa_action;

    this->input = input;

    this->file_name = file_name;

    this-> reset();
    /* Set max count*/
    this->actor_set_max_port_count(RPA_MAX_FIFO_COUNT);
    /* Set portrefs*/
    this->actor_add_portrefs(&this->input);
}

bool rpa::enable(){
    bool result;
    result = (welt_c_fifo_population(this->input) > 0);
    return result;
}

void rpa::invoke() {
    /* get input */
    welt_c_fifo_read(this->input, &this->curr_input);

    /* execute the action */
    (this->rpa_action)->execute
    (this->stateTransitionMatrix[this->curr_state][this->curr_input]);

}

void rpa::connect(welt_cpp_graph *graph) {

    int port_index;
    int direction;

    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);

}
void rpa::reset() {
    /* Load policy */
    this->inStream.open(this->file_name);
    if (this->inStream.fail()){
        cerr << "Could not open file \"" << file_name << "\"" << endl;
    }
    int i, j;
    int data;
    cout << "rpa actor is loading a policy in the constructor" << endl;

    if (std::getline(this->inStream, this->line)) {
        std::istringstream ss(this->line);
        ss >> this->num_state;
        cout << this->num_state << endl;
        this->stateTransitionMatrix=
                (int **)malloc(this->num_state*sizeof(int*));
    }

    if (std::getline(this->inStream, this->line)) {
        std::istringstream ss(this->line);
        ss >> this->num_input;
        cout << this->num_input << endl;
        for (i = 0; i < this->num_state; i++) {
            this->stateTransitionMatrix[i] = (int *) malloc(this->num_input * sizeof
                    (int));
        }
    }

    for (j = 0; j < this->num_state; j++) {
        if (std::getline(this->inStream, this->line)) {
            std::istringstream ss(this->line);
            for (i = 0; i < this->num_input; i++) {
                ss >> data;
                this->stateTransitionMatrix[i][j] = data;// Add it to the last
                cout << this->stateTransitionMatrix[i][j] << ", ";
            }
        }
        cout << "\n " << endl;
    }
    return;
}
rpa::~rpa(){
}
