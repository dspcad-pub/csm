/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "jitter_file_source.h"



jitter_file_source::jitter_file_source(
        char *file, router_port_pointer out1){

    this->file = file;
    //printf("source file:%s\n", this->file);
    this->fp = welt_c_util_fopen((const char *)this->file, "r");
    if (fscanf(this->fp, "%lf", &this->data) != 1) {
            /* End of input */
        this->mode = JITTER_FILE_SOURCE_MODE_ERROR;
        fprintf(stderr, "cannot open file %s\n", this->file);
        this->data = 0.0;
        
    }else{
        //printf("first data:%lf\n", this->data);
        this->mode = JITTER_FILE_SOURCE_MODE_WRITE;
    }
 
    //this->out1 = out1;
    //this->out2 = out2;
    //this->out3 = out3;
    this->out1 = out1;
    this->router_out1 = router_port_get_router_context(this->out1);

}

bool jitter_file_source::enable() {
    bool result = false;

    switch (this->mode) {
    case JITTER_FILE_SOURCE_MODE_WRITE:
    /*
        result = (welt_c_fifo_population(this->out1) <
                welt_c_fifo_capacity(this->out1)) &&
                (welt_c_fifo_population(this->out2) <
                welt_c_fifo_capacity(this->out2)) &&
                (welt_c_fifo_population(this->out3) <
                welt_c_fifo_capacity(this->out3)) ;
     */
        result = (router_population(this->router_out1) <
                router_capacity(this->router_out1));
        break;
    case JITTER_FILE_SOURCE_MODE_ERROR:
        result = false;
        break;
    default:
        result = false;
        break;
    }
    //printf("jitter source enable: %d\n", result);
    return result;
}

void jitter_file_source::invoke() {

    switch (this->mode) {
    case JITTER_FILE_SOURCE_MODE_WRITE:
        //welt_c_fifo_write(this->out1, &this->data);
        //welt_c_fifo_write(this->out2, &this->data);
        //welt_c_fifo_write(this->out3, &this->data);
        router_write(this->out1, &this->data);
        //printf("data:%lf\n", this->data);
        if (fscanf(this->fp, "%lf", &this->data) != 1) {
            /* End of input */
            fprintf(stderr, "end of input\n");
            this->mode = JITTER_FILE_SOURCE_MODE_ERROR;
            return;
        } 
        //printf("new data:%lf\n", this->data);
        this->mode = JITTER_FILE_SOURCE_MODE_WRITE;
        break;
    case JITTER_FILE_SOURCE_MODE_ERROR:
        this->mode = JITTER_FILE_SOURCE_MODE_WRITE;
        break;
    default:
        this->mode = JITTER_FILE_SOURCE_MODE_WRITE;
        break;
    }
    
}

jitter_file_source::~jitter_file_source() {
    fclose(this->fp);
    free(this);
}

