/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "STR.h"


STR::STR(int Ws, router_port_pointer in1,
                            router_port_pointer in2,
                            welt_c_fifo_pointer out1){
	int i;

    //this->in1 = in1;
	//this->in2 = in2;
    
    this->in1 = in1;
    this->router_in1 = router_port_get_router_context(this->in1);
    
    this->in2 = in2;
    this->router_in2 = router_port_get_router_context(this->in2);
    
	this->out1 = out1;

	this->mode = 1;
	/*Load table and Parameter*/
	this->Ws = Ws;
	this->high = 0.0;
	this->low = 0.0;
	this->vol = NULL;
	/*Allocate memory for sorting*/
	this->vol = (double*)malloc(sizeof(double)*Ws);
	this->state = (int*)malloc(sizeof(int)*Ws);
	/* Initialize the array*/
	for (i=0; i<Ws; i++){   
        this->state[i] = 2;
        this->vol[i] = 0.0;
    }
}

bool STR::enable() {
	bool result = false;
	switch (this->mode) {
		case STR_MODE_PROCESS:
        /*
			result = (welt_c_fifo_population(this->in1) >= this->Ws) &&
					(welt_c_fifo_population(this->in2) >= 1) &&
					(welt_c_fifo_population(this->out1) + this->Ws <=
                    welt_c_fifo_capacity(this->out1));
		*/
        
        result = (router_port_population(this->in1) >= this->Ws) &&
					(router_port_population(this->in2) >= 1) &&
					(welt_c_fifo_population(this->out1) + this->Ws <=
                    welt_c_fifo_capacity(this->out1));
        
        
        break;
		default:
			result = false;
		break;
	}
	return result;
}

void STR::invoke() {

	double medium;
	int i;
	/*Load data from input fifo*/
	//welt_c_fifo_read_block(this->in1, this->vol, this->Ws);
	router_read_block(this->in1, this->vol, this->Ws);
	//welt_c_fifo_read(this->in2, &medium);
	router_read(this->in2, &medium);

	/*Assign the state for every input*/
	for (i = 0; i<this->Ws; i++){
		this->state[i] = (this->vol[i] >= medium);
	}
	/*Store results*/
	welt_c_fifo_write_block(this->out1, this->state, this->Ws);

	return;
}

STR::~STR(){
	free(this->state);
	free(this->vol);
	free(this);
}
