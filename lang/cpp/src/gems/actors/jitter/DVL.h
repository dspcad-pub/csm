#ifndef _DVL_h
#define _DVL_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the DVL (Determine Voltage Level) actor.
*******************************************************************************/


#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
};
#include "router.h"

/* Actor modes */
#define DVL_MODE_PROCESS  1
#define DVL_MODE_HALT 0

class DVL: public welt_cpp_actor{
public:
    DVL(int Ws, float Op, int Tp,
        router_port_pointer in1, router_port_pointer out1);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    ~DVL();

private:
    /* Actor interface ports. */
    router_port_pointer  in1;
    router_context_pointer router_in1;

    /*output sequence*/
    router_port_pointer  out1;
    router_context_pointer router_out1;

    /* Actor Parameters*/
    int Ws; /*Window Size*/
    int vol1;
    int vol99;
    int THVOL;
    double Op; /*Overlap*/
    int Tp; /*Threshold*/
    double *vol;/*Voltage sequence*/
    double *vol_copy;/*Voltage sequence*/
    double high,low,medium;
    int mark;

};


#endif

