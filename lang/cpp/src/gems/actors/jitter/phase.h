#ifndef _phase_h
#define _phase_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the Phase (Refined Estimation) actor.
*******************************************************************************/

#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "router.h"

/* Actor modes */
#define PHASE_MODE_PROCESS   1
#define PHASE_MODE_LENGTH    2
#define PHASE_MODE_ERROR     3
#define PHASE_MODE_HALT 0

#define PHS_DATA_MAX		15999986

class phase:public welt_cpp_actor{
public:
    phase(int Ws, int num,
          router_port_pointer in1,
          router_port_pointer in2,
          router_port_pointer in3, welt_c_fifo_pointer in4,
          welt_c_fifo_pointer out1);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    ~phase();

private:
    /* Actor interface ports. */
    /*input sequence*/
    //welt_c_fifo_pointer in1; /* TRall array*/
    //welt_c_fifo_pointer in2; /* TRall_num*/
    //welt_c_fifo_pointer in3; /* CREtime*/
    welt_c_fifo_pointer in4; /* Coefficient a from linear fitting */

    router_port_pointer  in1; /* TRall array*/
    router_context_pointer router_in1;

    router_port_pointer  in2; /* TRall_num*/
    router_context_pointer router_in2;

    router_port_pointer  in3; /* CREtime*/
    router_context_pointer router_in3;


    /*Output sequence*/
    welt_c_fifo_pointer out1; /* TIE*/

    /* Actor Parameters*/
    int Ws,k,len,Num; /*Window Size*/
    int TRlength;
    double *vol;
    int *state;
    int *TR;
    int *TRindex;
    int Pindex;
    double *TRT;
    double *TRT_all;
    double *d;
    double *phase_all;
    double *phase_context;
    double *phase_correct;
    double *phase_error;
    double *tie;
    double *TRall;
    int TRall_num;
    double CREtime;
    double a;

};

#endif

