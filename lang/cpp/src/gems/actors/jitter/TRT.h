#ifndef _TRT_h
#define _TRT_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the TRT (Computing Transition Time) actor.
*******************************************************************************/


#include "welt_cpp_actor.h"
extern "C"{

#include "welt_c_fifo.h"
#include "router.h"
};

/* Actor modes */
#define TRT_MODE_ERROR     0
#define TRT_MODE_PROCESS     1
#define TRT_MODE_LENGTH      2
#define TRT_MODE_HALT 0
#define TRT_INPUT_THRESHOLD 5
#define TRT_STATE_HIGH	1
#define TRT_STATE_LOW	0
#define TRT_STATE_MID	2
#define TRT_TRAN			1
#define TRT_NON_TRAN		0
#define TRT_DATA_MAX		15999986

class TRT: public welt_cpp_actor{
public:
    TRT(int Ws, router_port_pointer in1,
        router_port_pointer in2,
        router_port_pointer in3,
        router_port_pointer in4, welt_c_fifo_pointer out1,
        router_port_pointer out2,
        router_port_pointer out3);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    ~TRT();

    int TRT_get_trall_num();
    int TRT_get_trt_num();

private:
    /* Actor interface ports. */
    /*input sequence*/

    router_port_pointer  in1;/* input data */
    router_context_pointer router_in1;

    router_port_pointer  in2; /* medium voltage threshold*/
    router_context_pointer router_in2;

    router_port_pointer  in3;/* trt_num*/
    router_context_pointer router_in3;

    router_port_pointer  in4;/* TRindex array*/
    router_context_pointer router_in4;

    /*Output sequence*/
    welt_c_fifo_pointer out1; /* TRT array */

    router_port_pointer  out2;/* TRall: to RE */
    router_context_pointer router_out2;

    router_port_pointer  out3;/* TRall_num: to RE */
    router_context_pointer router_out3;

    /* Actor Parameters*/
    int Ws; /*Window Size*/
    int k,len;
    int TRlength;
    int TRall_num;
    int trt_num;
    double *vol;
    int *state;
    int *TR;
    int *TRindex;
    double *TRT_context,*TRall;
    double medium; /* medium voltage threshold*/
};

#endif

