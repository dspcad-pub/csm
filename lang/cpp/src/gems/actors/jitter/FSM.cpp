/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "FSM.h"

/*
in1: state
out10: FSM->TRT, trt_num
out11: FSM->RE, trt_num
out12: FSM->RRE, trt_num
out13: FSM->CRE, trt_num
out20: FSM->TRT, TRindex
out21: FSM->CRE, TRindex
*/

FSM::FSM(int Ws, welt_c_fifo_pointer in1,
        router_port_pointer out1, router_port_pointer out2){
	int i;

    this->in1 = in1;
    /*REVISE HERE*/
    //this->out10 = out10;
    //this->out11 = out11;
	//this->out12 = out12;
	//this->out13 = out13;
	//this->out20 = out20;
	//this->out21 = out21;
    
    this->out1 = out1;
    this->router_out1 = router_port_get_router_context(this->out1);
    this->out2 = out2;
    this->router_out2 = router_port_get_router_context(this->out2);
    
	this->mode = FSM_S_MODE_PROCESS;
	
    /*Load table and Parameter*/
	this->Ws = Ws;
	/*Allocate memory for sorting*/
    this->trt_num = 0;
	this->state = (int*)malloc(sizeof(int)*Ws);
	this->TR = (int*)malloc(sizeof(int)*Ws);
	this->TRindex = (int*)malloc(sizeof(int)*Ws);
	/* Initialize the array*/
	for (i=0; i<Ws; i++){   
        this->TR[i] = 0;
		this->TRindex[i] = 0;
	}

}

bool FSM::enable() {
	bool result = false;
	switch (this->mode) {
		case FSM_S_MODE_PROCESS:
        /*
			result = (welt_c_fifo_population(this->in1) >= this->Ws) &&
					(welt_c_fifo_population(this->out10) <
                    welt_c_fifo_capacity(this->out10)) &&
                    (welt_c_fifo_population(this->out11) <
                    welt_c_fifo_capacity(this->out11)) &&
                    (welt_c_fifo_population(this->out12) <
                    welt_c_fifo_capacity(this->out12)) &&
                    (welt_c_fifo_population(this->out13) <
                    welt_c_fifo_capacity(this->out13)) &&
					(welt_c_fifo_population(this->out20) + this->Ws <=
                    welt_c_fifo_capacity(this->out20)) &&
                    (welt_c_fifo_population(this->out21) + this->Ws <=
                    welt_c_fifo_capacity(this->out21));
        */
        result = (welt_c_fifo_population(this->in1) >= this->Ws) &&
                    (router_population(this->router_out1) <
                    router_capacity(this->router_out1)) &&
                    (router_population(this->router_out2) <
                    router_capacity(this->router_out2));
		break;
		default:
			result = false;
		break;
	}
	return result;
}

void FSM::invoke() {
	int i,j;
	int *p;
	int *q;

	/*Load data from three fifo*/
	welt_c_fifo_read_block(this->in1, this->state, this->Ws);
	p = this->state;
	q = p+1;
    
	j = 0;
	/*Check transitions and mark them and their index*/
	for (i = 0; i<this->Ws-1; i++){
		this->TR[i] = (this->state[i] + q[i] == 1);
	}
	for (i = 0; i<this->Ws-1; i++){
		this->TRindex[j] = this->TR[i]  * i;
		j = j + this->TR[i];
	}
	
    this->trt_num = j;

	/*Store data to three fifo*/
    /*
	welt_c_fifo_write(this->out10, &this->trt_num);
	welt_c_fifo_write(this->out11, &this->trt_num);
	welt_c_fifo_write(this->out12, &this->trt_num);
	welt_c_fifo_write(this->out13, &this->trt_num);
	welt_c_fifo_write_block(this->out20, this->TRindex, this->trt_num);
	welt_c_fifo_write_block(this->out21, this->TRindex, this->trt_num);
    */
    router_write(this->out1, &this->trt_num);
    router_write_block(this->out2, this->TRindex, this->trt_num);
    
	return;
	
}

FSM::~FSM(){
    free(this->TR);
	free(this->TRindex);
	free(this);
}
