/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "RRE.h"


RRE::RRE(int Ws, router_port_pointer in1,
                router_port_pointer in2,
                router_port_pointer in3,
                welt_c_fifo_pointer in4, welt_c_fifo_pointer in5,
                welt_c_fifo_pointer out1){
    int i;

    //this->in1 = in1;
	//this->in2 = in2;
	//this->in3 = in3;
	this->in4 = in4;
	this->in5 = in5;
    
    this->in1 = in1;
    this->router_in1 = router_port_get_router_context(this->in1);
    
    this->in2 = in2;
    this->router_in2 = router_port_get_router_context(this->in2);
    
    this->in3 = in3;
    this->router_in3 = router_port_get_router_context(this->in3);
    
    this->out1 = out1;

	this->mode = RRE_MODE_LENGTH;
	/*Load table and Parameter*/
	this->Ws = Ws;
	
    /*Allocate memory for sorting*/
	this->d = (double*)malloc(sizeof(double)*RRE_DATA_MAX);
	this->TRall = (double*)malloc(sizeof(double)*RRE_DATA_MAX);
	/* initialize the array*/
	for (i=0; i<RRE_DATA_MAX; i++){   
        this->d[i] = 0.0;
        this->TRall[i] = 0.0;
    }

}

bool RRE::enable() {
	bool result = false;
	switch (this->mode) {
        case RRE_MODE_LENGTH:
            result = (router_port_population(this->in1) >= 1)&&
					(router_port_population(this->in3) >= 1);
        break;
		case RRE_MODE_PROCESS:
			result = (router_port_population(this->in2) >=
                    this->TRall_num) &&
					(welt_c_fifo_population(this->in4) >= 1) &&
                    (welt_c_fifo_population(this->in5) >=
                    (this->TRall_num - 1)) &&
					(welt_c_fifo_population(this->out1) <
                    welt_c_fifo_capacity(this->out1));
		break;
		default:
			result = false;
		break;
	}
    //printf("RRE enable: %d; mode:%d\n", result, this->mode);
    //printf("in2: %d\t %d\n", welt_c_fifo_population(this->in2),
    // this->TRall_num);
    //printf("in4: %d\t %d\n", welt_c_fifo_population(this->in4), 1);
    //printf("in5: %d\t %d\n", welt_c_fifo_population(this->in5),
    // this->TRall_num - 1);
    //printf("out1: %d\t %d\n", welt_c_fifo_population(this->out1),
    // welt_c_fifo_capacity(this->out1));
	return result;
}

void RRE::invoke() {
	int i,totalUIs,len;
	double *fp;
	int	*intp;
	double high,low,medium,REtime,RREtime,dlen;
    switch (this->mode) {
        case RRE_MODE_LENGTH:
            router_read(this->in1, &this->trt_num);
            router_read(this->in3, &this->TRall_num);
            this->d_len = this->TRall_num - 1;
            this->mode = RRE_MODE_PROCESS;
        break;
		case RRE_MODE_PROCESS:
            router_read_block(this->in2, this->TRall,
                                    this->TRall_num);
            welt_c_fifo_read(this->in4, &this->REtime);
            welt_c_fifo_read_block(this->in5, this->d,
                                    this->d_len);
            totalUIs = 0;
            /*Get the refined estimation*/
            for(i = 0; i<this->TRall_num-1; i++){
                totalUIs += ((int)(this->d[i]/this->REtime+0.5));
            }
            this->RREtime = (this->TRall[this->d_len-1]-
                                this->TRall[0])/totalUIs;
            
            welt_c_fifo_write(this->out1, &this->RREtime);
            this->mode = RRE_MODE_LENGTH;
		break;
		default:
			this->mode = RRE_MODE_ERROR;
		break;
	}

	return;
	
}

RRE::~RRE(){
	free(this);
}
