#ifndef _STR_h
#define _STR_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the STR (State Transition) actor.
*******************************************************************************/


#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "router.h"
#include "welt_c_util.h"
};

/* Actor modes */
#define STR_MODE_PROCESS  1
#define STR_MODE_HALT 0
#define STR_INPUT_THRESHOLD 2
#define STR_STATE_HIGH	1
#define STR_STATE_LOW	0
#define STR_STATE_MID	2

class STR: public welt_cpp_actor{
public:
    STR(int Ws, router_port_pointer in1,
        router_port_pointer in2,
        welt_c_fifo_pointer out1);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    ~STR();

private:
    /* Actor interface ports. */
    /*input sequence*/
    //welt_c_fifo_pointer in1; /*input*/
    //welt_c_fifo_pointer in2; /*medium*/
    router_port_pointer  in1;/*input*/
    router_context_pointer router_in1;

    router_port_pointer  in2;/*medium*/
    router_context_pointer router_in2;

    /*output sequence*/
    welt_c_fifo_pointer out1;/*state*/

    /* Actor Parameters*/
    double *vol;
    int Ws;
    double high,low;
    int *state;
};


#endif

