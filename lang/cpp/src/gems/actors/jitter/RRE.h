#ifndef _RRE_h
#define _RRE_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the RRE (Refined Estimation) actor.
*******************************************************************************/


#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
#include "router.h"
};

/* Actor modes */
#define RRE_MODE_LENGTH      2
#define RRE_MODE_PROCESS     1
#define RRE_MODE_ERROR       3
#define RRE_MODE_HALT 0

#define RRE_DATA_MAX		15999986

class RRE: public welt_cpp_actor{
public:
    RRE(int Ws, router_port_pointer in1,
        router_port_pointer in2,
        router_port_pointer in3,
        welt_c_fifo_pointer in4, welt_c_fifo_pointer in5,
        welt_c_fifo_pointer out1);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    ~RRE();

private:
    /* Actor interface ports. */
    /*input sequence*/
    //welt_c_fifo_pointer in1; /* trt_num <-- FSM*/
    //welt_c_fifo_pointer in2; /* TRall array <--TRT*/
    //welt_c_fifo_pointer in3; /* TRall_num <--TRT*/
    welt_c_fifo_pointer in4; /* REtime*/
    welt_c_fifo_pointer in5; /* d array*/

    router_port_pointer in1; /* trt_num <-- FSM*/
    router_context_pointer router_in1;
    router_port_pointer in2; /* TRall array <--TRT*/
    router_context_pointer router_in2;
    router_port_pointer in3; /* TRall_num <--TRT*/
    router_context_pointer router_in3;

    /*Output sequence*/
    welt_c_fifo_pointer out1; /*RREtime*/

    /* Actor Parameters*/
    int Ws,len; /*Window Size*/
    int TRlength;
    double *d;
    int *state;
    int *TR;
    int *TRindex;
    double *TRall;
    double RREtime, REtime;
    int TRall_num, trt_num, d_len;

};

#endif

