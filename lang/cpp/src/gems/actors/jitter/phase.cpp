/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "phase.h"


phase::phase(int Ws, int num,
                router_port_pointer in1,
                router_port_pointer in2,
                router_port_pointer in3, welt_c_fifo_pointer in4,
                welt_c_fifo_pointer out1){
	int i;

    //this->in1 = in1;
	//this->in2 = in2;
	//this->in3 = in3;
	this->in4 = in4;
    this->out1 = out1;
    
    this->in1 = in1;
    this->router_in1 = router_port_get_router_context(this->in1);
    
    this->in2 = in2;
    this->router_in2 = router_port_get_router_context(this->in2);
    
    this->in3 = in3;
    this->router_in3 = router_port_get_router_context(this->in3);
    
	this->mode = PHASE_MODE_LENGTH;
	/*Load table and Parameter*/
	this->Ws = Ws;
	this->Num = num;
	this->k = 0;
	this->len = 0;
    this->TRall_num = 0;
	this->tie = (double *)malloc(sizeof(double)*PHS_DATA_MAX);
	this->TRall = (double *)malloc(sizeof(double)*PHS_DATA_MAX);
	/* Initialize array*/
	for (i=0; i<Ws; i++){
        this->tie[i] = 0.0;
        this->TRall[i] = 0.0;
	}
}

bool phase::enable() {
	bool result = false;
	switch (this->mode) {
        case PHASE_MODE_LENGTH:
            result = (router_port_population(this->in2) >= 1);
        break;
		case PHASE_MODE_PROCESS:
			result = (router_port_population(this->in1) >=
                    this->TRall_num) &&
					(router_port_population(this->in3) >= 1) &&
					(welt_c_fifo_population(this->in4) >= 1) &&
					(welt_c_fifo_population(this->out1) +
                    this->TRall_num <= welt_c_fifo_capacity(this->out1));
		break;
		default:
			result = false;
		break;
	}
	return result;
}

void phase::invoke() {
	int i,k;
	double *fp;
	int	*intp;
	double a,b,high,low,medium,CREtime,REtime,RREtime,dlen;
	double tmp,phase_offset,fN;
    
    switch (this->mode) {
        case PHASE_MODE_LENGTH:

            router_read (this->in2, &this->TRall_num);
            this->mode = PHASE_MODE_PROCESS;
        break;
		case PHASE_MODE_PROCESS:
            router_read_block (this->in1, this->TRall,
                                    this->TRall_num);
            router_read (this->in3, &this->CREtime);
            welt_c_fifo_read (this->in4, &this->a);
            phase_offset = this->a;
            /*Update the TRTall*/
            this->k += 1;
            for(i = 0; i<this->TRall_num; i++){
                tmp = this->TRall[i]-(double)(((int)
                        (this->TRall[i]/this->CREtime+0.5))*this->CREtime);
                this->tie[i] = tmp-phase_offset;
                /*sub = tmp-phase_offset-this->tie[i];*/
            }
            
            welt_c_fifo_write_block(this->out1, this->tie,
                                    this->TRall_num);
            this->mode = PHASE_MODE_LENGTH;
		break;
		default:
			this->mode = PHASE_MODE_ERROR;
		break;
	}
	return;
	
}

phase::~phase(){
	free(this->tie);
	free(this);
}
