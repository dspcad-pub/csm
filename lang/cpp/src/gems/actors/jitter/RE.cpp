/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "RE.h"
#include "welt_c_util.h"


int cmp (const void * a, const void * b)
{
	double c = *(double*)a - *(double*)b;
	if (c>0)
		return 1;
	else if (c<0)
		return -1;
	else 
		return 0;

}

RE::RE(int Ws, router_port_pointer in1,
                        router_port_pointer in2,
                        router_port_pointer in3,
                        welt_c_fifo_pointer out1,welt_c_fifo_pointer out2){
	int i;

    /*
    this->in1 = in1;
	this->in2 = in2;
	this->in3 = in3;
    */
    
    this->in1 = in1;
    this->router_in1 = router_port_get_router_context(this->in1);
    
    this->in2 = in2;
    this->router_in2 = router_port_get_router_context(this->in2);
    
    this->in3 = in3;
    this->router_in3 = router_port_get_router_context(this->in3);
    
    this->out1 = out1;
	this->out2 = out2;

	this->mode = RE_MODE_LENGTH;
	/*Load table and Parameter*/
	this->Ws = Ws;
	/*Allocate memory for sorting*/
	this->d = (double*)malloc(sizeof(double)*RE_DATA_MAX);
	this->d_cp = (double*)malloc(sizeof(double)*RE_DATA_MAX);
	this->TRall = (double*)malloc(sizeof(double)*RE_DATA_MAX);
	/* initialize the array*/
	for (i=0; i<RE_DATA_MAX; i++){   
        this->d[i] = 0.0;
        this->d_cp[i] = 0.0;
        this->TRall[i] = 0.0;
    }

}

bool RE::enable() {
	bool result = false;
	//result =1;
	//return result;
	switch (this->mode) {
        case RE_MODE_LENGTH:
            result = (router_port_population(this->in1) >= 1) &&
					(router_port_population(this->in3) >= 1);
        break;
		case RE_MODE_PROCESS:
			result = (router_port_population(this->in2) >=
                    this->TRall_num) &&
					(welt_c_fifo_population(this->out1) <
                    welt_c_fifo_capacity(this->out1)) &&
					(welt_c_fifo_population(this->out2) +
                    this->TRall_num <= welt_c_fifo_capacity(this->out2));
		break;
		default:
			result = false;
		break;
	}
    //printf("RE enable: %d; mode:%d\n", result, this->mode);
	return result;
}

void RE::invoke() {
	int tmp,i,len;
	double *fp;
	int	*intp;
	double high,low,medium,REtime,dlen;
	double *d = NULL;
    //printf("RE start\n");
    switch (this->mode) {
        case RE_MODE_LENGTH:
            router_read(this->in1, &this->trt_num);
            router_read(this->in3, &this->TRall_num);
            this->mode = RE_MODE_PROCESS;
            //printf("RE LENGTH MODE END\n");
        break;   
        case RE_MODE_PROCESS:
            router_read_block(this->in2, this->TRall,
                                    this->TRall_num);
            /*Get the rough estimation*/
            len = this->TRall_num-1;
            for (i = 0; i<this->TRall_num-1; i++){
                this->d[i] = this->TRall[i+1] - this->TRall[i];
                this->d_cp[i] = this->d[i];
            }
            qsort(this->d_cp, len, sizeof(this->d_cp[0]), cmp);

            tmp = (int)((len-1)/4);
            this->REtime = this->d_cp[tmp];
            
            welt_c_fifo_write(this->out1, &this->REtime);
            welt_c_fifo_write_block(this->out2, this->d, len);

            this->mode = RE_MODE_LENGTH;
            
        break;
        default:
            this->mode = RE_MODE_ERROR;
        break;
	}

	return;
	
}

RE::~RE(){
    free(this->d);
    free(this->d_cp);
    free(this->TRall);
	free(this);
}
