/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "double_file_sink.h"


double_file_sink::double_file_sink(char *file,
        welt_c_fifo_pointer in) {

    this->file = file;
    this->fp = welt_c_util_fopen((const char *)this->file, "w");
    this->mode = DOUBLE_FILE_SINK_MODE_READ;
    this->in = in;

}

bool double_file_sink::enable() {
    bool result = false;

    switch (this->mode) {
    case DOUBLE_FILE_SINK_MODE_READ:
        result = welt_c_fifo_population(this->in) >= 1;
        break;
    default:
        result = false;
        break;
    }
    return result;
}

void double_file_sink::invoke() {
    double value = 0.0;

    switch (this->mode) {
    case DOUBLE_FILE_SINK_MODE_READ:
        welt_c_fifo_read(this->in, &value);
        fprintf(this->fp, "%.16f\n", value);
        this->mode = DOUBLE_FILE_SINK_MODE_READ;
        break;
    default:
        this->mode = DOUBLE_FILE_SINK_MODE_ERROR;
        break;
    }
	
}

double_file_sink::~double_file_sink() {
    fclose(this->fp);
    free(this);
}
