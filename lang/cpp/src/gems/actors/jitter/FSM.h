#ifndef _FSM_h
#define _FSM_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the FSM(FSM_s) actor.
*******************************************************************************/


#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "router.h"

/* Actor modes */
#define FSM_S_MODE_PROCESS  1
#define FSM_S_MODE_HALT 0
#define FSM_INPUT_THRESHOLD 3
#define FSM_STATE_HIGH	1
#define FSM_STATE_LOW	0
#define FSM_STATE_MID	2
#define FSM_TRAN			1
#define FSM_NON_TRAN		0

class FSM:public welt_cpp_actor{
public:
    FSM(int Ws, welt_c_fifo_pointer in1,
    router_port_pointer out1, router_port_pointer out2);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    ~FSM();

private:
    /* Actor interface ports. */
    /*input sequence*/
    welt_c_fifo_pointer in1; /*STR-->FSM, state*/

    /*Output sequence*/
    //welt_c_fifo_pointer out10; /*FSM->TRT, trt_num*/
    //welt_c_fifo_pointer out11; /*FSM->RE, trt_num*/
    //welt_c_fifo_pointer out12; /*FSM->RRE, trt_num*/
    //welt_c_fifo_pointer out13; /*FSM->CRE, trt_num*/
    //welt_c_fifo_pointer out20; /*FSM->TRT, TRindex*/
    //welt_c_fifo_pointer out21; /*FSM->CRE, TRindex*/

    router_port_pointer  out1; /*trt_num*/
    router_context_pointer router_out1;

    router_port_pointer  out2; /*TRindex*/
    router_context_pointer router_out2;

    /* Actor Parameters*/
    int Ws; /*Window Size*/
    double *vol;
    int *state;
    int *TR;
    int *TRindex;
    int trt_num;

};

/*******************************************************************************
Create a new actor with three input fifos and three output fifos.
in1: state
out10: FSM->TRT, trt_num
out11: FSM->RE, trt_num
out12: FSM->RRE, trt_num
out13: FSM->CRE, trt_num
out20: FSM->TRT, TRindex
out21: FSM->CRE, TRindex
*******************************************************************************/


#endif

