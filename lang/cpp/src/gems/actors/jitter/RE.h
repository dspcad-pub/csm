#ifndef _RE_h
#define _RE_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the RE (Rough Estimation) actor.
*******************************************************************************/


#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "router.h"

};

/* Actor modes */
#define RE_MODE_PROCESS  1
#define RE_MODE_LENGTH   2
#define RE_MODE_ERROR    3
#define RE_MODE_HALT 0

#define RE_DATA_MAX		15999986

class RE:public welt_cpp_actor{
public:
    RE(int Ws, router_port_pointer in1,
       router_port_pointer in2,
       router_port_pointer in3,
       welt_c_fifo_pointer out1,welt_c_fifo_pointer out2);


    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    ~RE();

private:
    /* Actor interface ports. */
    /*input sequence*/
    //welt_c_fifo_pointer in1; /*trt_num*/
    //welt_c_fifo_pointer in2; /*TRall*/
    //welt_c_fifo_pointer in3; /*TRall_num*/

    router_port_pointer  in1; /*trt_num*/
    router_context_pointer router_in1;

    router_port_pointer  in2; /*TRall*/
    router_context_pointer router_in2;

    router_port_pointer  in3; /*TRall_num*/
    router_context_pointer router_in3;

    /*Output sequence*/
    welt_c_fifo_pointer out1; /*REtime*/
    welt_c_fifo_pointer out2; /*d array: difference*/

    /* Actor Parameters*/
    int Ws; /*Window Size*/
    int TRlength;
    double *vol;
    int *TR;
    int *TRindex;
    double *TRT,*TRall;
    double *d, *d_cp;
    int TRall_num;
    double REtime;
    int trt_num;

};

#endif

