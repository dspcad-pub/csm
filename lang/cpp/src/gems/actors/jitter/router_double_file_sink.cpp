/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "router_double_file_sink.h"
#include "welt_c_util.h"


router_double_file_sink::router_double_file_sink(
        char *file, router_port_pointer in) {


    this->file = file;
    this->fp = welt_c_util_fopen((const char *)this->file, "w");
    this->mode = ROUTER_DOUBLE_FILE_SINK_MODE_READ;
    //this->in = in;
    this->in = in;
    this->router_in = router_port_get_router_context(this->in);

}

bool router_double_file_sink::enable() {
    bool result = false;

    switch (this->mode) {
    case ROUTER_DOUBLE_FILE_SINK_MODE_READ:
        result = router_port_population(this->in) >= 1;
        break;
    default:
        result = false;
        break;
    }
    return result;
}

void router_double_file_sink::invoke() {
    double value = 0.0;

    switch (this->mode) {
    case ROUTER_DOUBLE_FILE_SINK_MODE_READ:
        router_read(this->in, &value);
        fprintf(this->fp, "%.16f\n", value);
        this->mode = ROUTER_DOUBLE_FILE_SINK_MODE_READ;
        break;
    default:
        this->mode = ROUTER_DOUBLE_FILE_SINK_MODE_ERROR;
        break;
    }
	
}

router_double_file_sink::~router_double_file_sink() {
    fclose(this->fp);
    free(this);
}
