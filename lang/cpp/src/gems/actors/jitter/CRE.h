#ifndef _CRE_h
#define _CRE_h
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


/*******************************************************************************
DESCRIPTION:
This is a header file of the CRE (Refined Estimation) actor.
*******************************************************************************/


#include "welt_cpp_actor.h"

extern "C"{
#include "welt_c_fifo.h"
#include "router.h"
#include "welt_c_util.h"
};

/* Actor modes */
#define CRE_MODE_PROCESS     1
#define CRE_MODE_LENGTH      2
#define CRE_MODE_ERROR       3
#define CRE_MODE_HALT        0

#define CRE_DATA_MAX		15999986

class CRE: public welt_cpp_actor{
public:
    CRE(int Ws, int k,
        router_port_pointer in1,
        router_port_pointer in2,
        welt_c_fifo_pointer in3,
        router_port_pointer in4,
        router_port_pointer in5,
        welt_c_fifo_pointer in6,
        router_port_pointer out1,
        welt_c_fifo_pointer out2);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;



private:
    /* Actor interface ports. */

    welt_c_fifo_pointer in3; /* TRT array */
    welt_c_fifo_pointer in6; /* RREtime*/

    router_port_pointer  in1; /* trt_num */
    router_context_pointer router_in1;

    router_port_pointer  in2;/* TRindex array */
    router_context_pointer router_in2;

    router_port_pointer  in4;/* TRall array*/
    router_context_pointer router_in4;

    router_port_pointer  in5;/* TRall_num*/
    router_context_pointer router_in5;

    router_port_pointer  out1; /* CREtime*/
    router_context_pointer router_out1;
    welt_c_fifo_pointer out2; /* coefficient a*/

    /* Actor Parameters*/
    int Ws; /*Window Size*/
    int k,len;
    int Num;
    int rpro;
    int TRlength;
    int *TRindex;
    int Pindex;
    int Pmid;
    double *TRT,*TRall;
    double *TRT_all;
    double *d;
    double a, b;
    double *phase_all;
    double *phase;
    double *phase_correct;
    double *phase_error;
    double RREtime, CREtime;
    int trt_num, TRall_num;

};

#endif

