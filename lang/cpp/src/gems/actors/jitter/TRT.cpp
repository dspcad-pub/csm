/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "TRT.h"
#include "welt_c_util.h"

TRT::TRT(int Ws, router_port_pointer in1,
                    router_port_pointer in2,
                    router_port_pointer in3,
                    router_port_pointer in4, welt_c_fifo_pointer out1,
                    router_port_pointer out2,
                    router_port_pointer out3){
	int i;

    /*
    this->in1 = in1;
	this->in2 = in2;
	this->in3 = in3;
	this->in4 = in4;
    this->out20 = out20;
    this->out21 = out21;
	this->out22 = out22;
	this->out23 = out23;
    this->out30 = out30;
    this->out31 = out31;
	this->out32 = out32;
	this->out33 = out33;
    */

	this->out1 = out1;

    this->in1 = in1;
    this->router_in1 = router_port_get_router_context(this->in1);
    
    this->in2 = in2;
    this->router_in2 = router_port_get_router_context(this->in2);
    
    this->in3 = in3;
    this->router_in3 = router_port_get_router_context(this->in3);
    
    this->in4 = in4;
    this->router_in4 = router_port_get_router_context(this->in4);
    
    this->out2 = out2;
    this->router_out2 = router_port_get_router_context(this->out2);
    
    this->out3 = out3;
    this->router_out3 = router_port_get_router_context(this->out3);
    
	this->mode = TRT_MODE_LENGTH;
	
    /*Load table and Parameter*/
	this->Ws = Ws;
	this->medium = 0.0;
	this->k = 0;
	this->len = 0;
    this->trt_num = 0;
    this->TRall_num = 0;
	/*Allocate memory for sorting*/
	this->TRindex = (int*)malloc(sizeof(int)*Ws);
	this->TRT_context = (double*)malloc(sizeof(double)*Ws);
	this->TRall = (double*)malloc(sizeof(double)*TRT_DATA_MAX);
	this->vol = (double*)malloc(sizeof(double)*Ws);
	/* Initialize the array*/
	for (i=0; i<Ws; i++){   
        this->TRT_context[i] = 0.0;
        this->vol[i] = 0.0;
        this->TRindex[i] = 0;
    }
	for (i = 0; i<TRT_DATA_MAX; i++)
		this->TRall[i] = 0.0;

}

bool TRT::enable() {
	bool result = false;
	//result =1;
	//return result;
	switch (this->mode) {
        case TRT_MODE_LENGTH:
            result = (router_port_population(this->in3) >= 1);
        break;
		case TRT_MODE_PROCESS:
			result = (router_port_population(this->in1) >= this->Ws) &&
					(router_port_population(this->in2) >= 1) &&
					(router_port_population(this->in4) >=
                    this->trt_num) &&
					(welt_c_fifo_population(this->out1) + this->trt_num <=
                    welt_c_fifo_capacity(this->out1)) &&
					(router_population(this->router_out2) +
					this->TRall_num + this->trt_num <=
                    router_capacity(this->router_out2)) &&
                    (router_population(this->router_out3) <
                    router_capacity(this->router_out3));
		break;
		default:
			result = false;
		break;
	}
    //printf("TRT enable: %d\n", result);
	return result;
}

void TRT::invoke() {
	int tmp,i;
	double trt_p1;
	double trt_p2;
    
    switch (this->mode) {
        case TRT_MODE_LENGTH:
            /*Load data from three fifo*/
            //welt_c_fifo_read (this->in3, &this->trt_num);
            router_read (this->in3, &this->trt_num);
            this->mode = TRT_MODE_PROCESS;
        break;
		case TRT_MODE_PROCESS:
            /*Load data from three fifo*/
            //printf("in1: len1:%d\tlen2:%d\t", this->Ws, welt_c_fifo_population
            // (this->in1));
            //printf("in2: len1:%d\tlen2:%d\t", 1, welt_c_fifo_population
            // (this->in2));
            //printf("in4: len1:%d\tlen2:%d\t", this->trt_num,
            // welt_c_fifo_population(this->in4));
            
            router_read_block (this->in1, this->vol, this->Ws);
            router_read (this->in2, &this->medium);
            router_read_block (this->in4, this->TRindex,
                                    this->trt_num);
            
            //welt_c_fifo_read_block (this->in1, this->vol, this->Ws);
            //welt_c_fifo_read (this->in2, &this->medium);
            //welt_c_fifo_read_block (this->in4, this->TRindex,
            //                        this->trt_num);
                                    
                                    
            /* Compute transition time*/
            for (i = 0; i<this->trt_num; i++){
                trt_p1 = fabs(this->vol[this->TRindex[i]]-this->medium);
                trt_p2 = fabs(this->vol[this->TRindex[i]+1]-
                            this->vol[this->TRindex[i]]);
                this->TRT_context[i] = 1 + trt_p1/trt_p2 + this->TRindex[i];
            }	
            
            tmp = this->Ws*this->k;
            for(i = 0; i<this->trt_num;i++){
                this->TRall[i+this->TRall_num] = this->TRT_context[i]+tmp;
            }
            
            this->TRall_num += this->trt_num;
            this->k += 1;
            
            welt_c_fifo_write_block(this->out1, this->TRT_context, this->trt_num);
            router_write_block(this->out2, this->TRall,
                                    this->TRall_num);
            router_write(this->out3, &this->TRall_num);
            /*
            welt_c_fifo_write_block(this->out20, this->TRall,
                                    this->TRall_num);
            welt_c_fifo_write_block(this->out21, this->TRall,
                                    this->TRall_num);
            welt_c_fifo_write_block(this->out22, this->TRall,
                                    this->TRall_num);
            welt_c_fifo_write_block(this->out23, this->TRall,
                                    this->TRall_num);
            welt_c_fifo_write(this->out30, &this->TRall_num);
            welt_c_fifo_write(this->out31, &this->TRall_num);
            welt_c_fifo_write(this->out32, &this->TRall_num);
            welt_c_fifo_write(this->out33, &this->TRall_num);
            */
            this->mode = TRT_MODE_LENGTH;
            
		break;
		default:
			this->mode = TRT_MODE_ERROR;
		break;
	}
	return;
	
}

TRT::~TRT(){
    free(this->vol);
    free(this->TRindex);
    free(this->TRT_context);
    free(this->TRall);
	free(this);
}

int TRT::TRT_get_trall_num(){
    return this->TRall_num;
}


int TRT::TRT_get_trt_num(){
    return this->trt_num;
}
