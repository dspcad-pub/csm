/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "DVL.h"
#include "welt_c_util.h"

int cmpfunc (const void * a, const void * b)
{
	double c = *(double*)a - *(double*)b;
	if (c>0)
		return 1;
	else if (c<0)
		return -1;
	else 
		return 0;
}

DVL::DVL(int Ws, float Op, int Tp,
    router_port_pointer in1, router_port_pointer out1){
	int i;

    //this->in1 = in1;
    //this->out1 = out1;
	//this->out2 = out2;
    this->in1 = in1;
    this->router_in1 = router_port_get_router_context(this->in1);
    this->out1 = out1;
    this->router_out1 = router_port_get_router_context(this->out1);
    
    
	this->mode = 1;
	/*Load table and Parameter*/
	this->Ws = Ws;
	this->Op = Op;
	this->Tp = Tp;
	this->high = 0.0;
	this->low = 0.0;
	this->medium = 0.0;
	this->mark = 0;
	this->THVOL = 1;
    this->vol1 = (int)(this->Ws/100);
	this->vol99 = (int)(this->Ws - this->vol1);
	/*Allocate memory for sorting*/
	this->vol = (double*)malloc(sizeof(double)*Ws);
	this->vol_copy = (double*)malloc(sizeof(double)*Ws);
	/* Initialize voltage array*/
	for (i=0; i<Ws; i++){  
        this->vol[i] = 0.0;
        this->vol_copy[i] = 0.0;
    }
}

bool DVL::enable() {
	bool result = false;
	//result = 1;
	//return result;
	
	switch (this->mode) {
		case DVL_MODE_PROCESS:
        /*
			result = (welt_c_fifo_population(this->in1) >= this->Tp) &&
					(welt_c_fifo_population(this->out1) <
         welt_c_fifo_capacity
         (this->out1)) &&
					(welt_c_fifo_population(this->out2) <
         welt_c_fifo_capacity
         (this->out2));
		*/
        
        result = (router_port_population(this->in1) >= this->Tp) &&
					(router_population(this->router_out1) <
                    router_capacity(this->router_out1));
        break;
		default:
			result = false;
		break;
	}
	return result;
	
}

void DVL::invoke() {
	int i,numc,numr;
    int size;
    
	/*Assign Value*/
    size = this->Ws * sizeof(double);
	
    
    //welt_c_fifo_read_block(this->in1, this->vol, this->Ws);
    router_read_block(this->in1, this->vol, this->Ws);
    
    memcpy(this->vol_copy, this->vol, size);

    qsort(this->vol, this->Ws, sizeof(this->vol[0]), cmpfunc);

    this->high = this->vol[this->vol99];
    this->low = this->vol[this->vol1];
    this->medium = (this->high + this->low)/2;
    
	/* Consume and move data from the input FIFO*/
	numr = this->Ws * this->Op;
	numc = this->Ws - numr;
    
	//welt_c_fifo_write_block(this->in1, &this->vol[numc], numr);
	
	/* 
		Load result to out FIFO
	*/
	
    router_write(this->out1, &this->medium);
	//welt_c_fifo_write(this->out1, &this->medium);
	//welt_c_fifo_write(this->out2, &this->medium);

	return;
	
}

DVL::~DVL(){
    free(this->vol);
	free(this);
}
