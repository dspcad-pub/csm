/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "CRE.h"


CRE::CRE(int Ws, int k,
                        router_port_pointer in1,
                        router_port_pointer in2,
                        welt_c_fifo_pointer in3,
                        router_port_pointer in4,
                        router_port_pointer in5,
                        welt_c_fifo_pointer in6,
                        router_port_pointer out1,
                        welt_c_fifo_pointer out2){
	int i;

	this->in3 = in3;
	this->in6 = in6;
    
    this->in1 = in1;
    this->router_in1 = router_port_get_router_context(this->in1);
    
    this->in2 = in2;
    this->router_in2 = router_port_get_router_context(this->in2);

    this->in4 = in4;
    this->router_in4 = router_port_get_router_context(this->in4);
    
    this->in5 = in5;
    this->router_in5 = router_port_get_router_context(this->in5);
    
    this->out1 = out1;
    this->router_out1 = router_port_get_router_context(this->out1);
    
    this->out2 = out2;

	this->mode = CRE_MODE_LENGTH;
	/*Load table and Parameter*/
	this->Ws = Ws;
	this->k = k;
	this->Num = 500;
	this->rpro = 200;
	this->Pmid = this->Num/2;
	this->Pindex = 0;
    this->trt_num = 0;
    this->TRall_num = 0;
    this->RREtime = 0.0;
    this->CREtime = 0.0;
    this->a = 0.0;
    this->b = 0.0;
    
    this->TRall = (double *)malloc(sizeof(double)*CRE_DATA_MAX);
    this->TRT = (double *)malloc(sizeof(double)*this->Ws);
    this->TRindex = (int *)malloc(sizeof(int)*this->Ws);
    
	this->phase = (double *)malloc(sizeof(double)*this->Ws);
	this->phase_correct = (double *)malloc(sizeof(double)*this->Num);
	this->phase_error = (double *)malloc(sizeof(double)*this->Num);
	this->phase_all = (double *)malloc(sizeof(double)*this->Num);
	this->TRT_all = (double *)malloc(sizeof(double)*this->Num);

	/* Initialize array*/
	for (i=0; i<this->Ws; i++){
        this->phase[i] = 0.0;
        this->TRindex[i] = 0;
        this->TRT[i] = 0.0;
	}
    
    for (i=0; i<CRE_DATA_MAX; i++){
        this->TRall[i] = 0.0;
	}
        
	for (i=0; i<this->Num; i++){
		this->phase_all[i] = 0.0;
		this->TRT_all[i] = 0.0;
		this->phase_correct[i] = 0.0;
		this->phase_error[i] = 0.0;
	}
}

bool CRE::enable() {
	bool result = false;
	switch (this->mode) {
        case CRE_MODE_LENGTH:
            result = (router_port_population(this->in1) >= 1) &&
					(router_port_population(this->in5) >= 1);
        break;
        
		case CRE_MODE_PROCESS:
			result = (router_port_population(this->in2) >=
                    this->trt_num) &&
                    (welt_c_fifo_population(this->in3)
                    >= this->trt_num) &&
                    (router_port_population(this->in4)
                    >= this->TRall_num) &&
                    (welt_c_fifo_population(this->in6) >= 1) &&
					(router_population(this->router_out1) <
                    router_capacity(this->router_out1)) &&
					(welt_c_fifo_population(this->out2) <
                    welt_c_fifo_capacity(this->out2));
		break;
		default:
			result = false;
		break;
	}
	return result;
}

void CRE::invoke() {

	int i,N,ptmp = 0;
	int index = 0;
	int *rp;
	int *p;
	double *fp;
	int	*intp;
	double *phase_copy;
	double *TRT_copy;
	double high,low,medium,REtime,RREtime,dlen;
	double a,b,xy,x,y,x2,xavg,yavg,sum,CREtime;
	double fN;
	const int pindex_copy = this->Pindex;
	
	rp = (int *)malloc(sizeof(int)*this->Num);
	p = (int *)malloc(sizeof(int)*this->Num);
	if (rp == NULL || p == NULL){
		fprintf(stderr, "allocate in CRE failed\n");
		exit(-1);
	}
    
    switch (this->mode) {
        case CRE_MODE_LENGTH:
            router_read(this->in1, &this->trt_num);
            router_read(this->in5, &this->TRall_num);
            this->mode = CRE_MODE_PROCESS;
        break;
        
		case CRE_MODE_PROCESS:

            router_read_block(this->in2, this->TRindex,
                                    this->trt_num);
            welt_c_fifo_read_block(this->in3, this->TRT,
                                    this->trt_num);
            router_read_block(this->in4, this->TRall,
                                    this->TRall_num);
            welt_c_fifo_read(this->in6, &this->RREtime);
            
            /*Revise TRT*/
            ptmp = this->Ws*this->k;
            for(i = 0; i<this->trt_num; i++){
                this->TRT[i] = this->TRT[i] + ptmp;
            }
            /* Compute the phase at current transition*/
            for(i = 0; i<this->trt_num; i++){
                this->phase[i] = this->TRT[i]-((int)(this->TRT[i]/
                                    this->RREtime+0.5))*this->RREtime;
            }
            
            /*Update this->phase_all and this->TRT_all*/
            if (this->Pindex + this->trt_num <= this->Num){
                phase_copy = this->phase_all + pindex_copy;
                TRT_copy = this->TRT_all + pindex_copy;
                for(i = 0; i< this->trt_num; i++){
                    phase_copy[i] = this->phase[i];
                    TRT_copy[i] = this->TRT[i];
                }

                this->Pindex = this->Pindex+this->trt_num;
            }else if (this->Pindex < this->Num){
                for(i = this->Pindex; i< this->Num; i++){
                    this->phase_all[i] = this->phase[i-this->Pindex];
                    this->TRT_all[i] = this->TRT[i-this->Pindex];
                }
                index = this->Pindex + this->trt_num - this->Num;
                this->Pindex = this->Num;
                
                
            }else{
                index = this->trt_num;
            }
            if (index > 0){
                if (this->trt_num >= this->Pmid){
                    ptmp = this->trt_num - this->Pmid;
                    for(i = 0; i < this->Pmid;i++){
                        this->phase_all[i+this->Pmid] = this->phase[i+ptmp];
                        this->TRT_all[i+this->Pmid] = this->TRT[i+ptmp];
                    }
                    index = 1;
                }else{
                    ptmp = this->Pmid - (this->trt_num);
                    /*Move Array*/
                    for(i = 0; i<ptmp; i++){
                        this->phase_all[i+this->Pmid] =
                                this->phase[i+this->Pmid+this->trt_num];
                        this->TRT_all[i+this->Pmid] =
                                this->TRT[i+this->Pmid+this->trt_num];
                    }
                    /*Update new data*/
                    for(i = 0; i < this->trt_num; i++){
                        this->phase_all[i+ptmp] = this->phase[i];
                        this->TRT_all[i+ptmp] = this->TRT[i];
                    }
                    index = 2;
                }
            }

            N = this->Pindex;
            /*X and Y average; x sum and y sum*/
            /*
                X: Transition Time
                Y: Phase at transition time
            */
            sum = 0.0;
            for (i = 0; i<N; i++){
                sum += this->phase_all[i];
            }
            y = sum;
            yavg = sum/N;

            sum = 0.0;

            for (i = 0; i<N; i++){
                sum += this->TRT_all[i];
            }
            x = sum;
            xavg = sum/N;
            
            /*X square*/
            sum = 0.0;
            for (i = 0; i<N; i++){
                sum += this->TRT_all[i] * this->TRT_all[i];
            }
            x2 = sum;
            /*xy*/
            sum = 0.0;
            for(i = 0; i<N; i++){
                sum += this->TRT_all[i] * this->phase_all[i];
            }
            xy = sum;
            /*Compute a and b*/
            b = (N*xy-x*y)/(N*x2-x*x);
            a = yavg-xavg*b;

            /*Correct phase*/
            for(i = 0; i<N; i++){
                this->phase_correct[i] = a + b*this->TRT_all[i];
            }
            for(i = 0; i<N; i++){
                this->phase_error[i] = this->phase_correct[i] -
                        this->phase[i];
            }
            this->CREtime = this->RREtime+this->RREtime*b;

            this->k = this->k + 1;
            this->a = a;
            this->b = b;
            
            router_write(this->out1, &this->CREtime);
            //welt_c_fifo_write(this->out2, &this->CREtime);
            //welt_c_fifo_write(this->out3, &this->a);
            welt_c_fifo_write(this->out2, &this->a);
            free(rp);
            free(p);

            this->mode = CRE_MODE_LENGTH;
		break;
		default:
			this->mode = CRE_MODE_ERROR;
		break;
	}
    
	return;
	
}
