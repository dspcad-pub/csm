function [model, results, State, Action]  = dpd_mdp_III(varargin)
    if nargin == 0
        choice_save = menu('Save to file?','Yes','No');
        T = input('What is the time horizon?');
        weight = input('What is the weighting vector?');
        choice_P = menu('Choose a transmit power transition matrix?','Yes','No');
        Prob = [];
        fname = [];
        if choice_P == 1
           [fname, path] = uigetfile('*.json');
           fname = [path, fname];
           ret = loadjson(fname);
           Prob = ret.Probability_Transition_Matrix;
           Prob = reshape(Prob.', 5, 5);
        end
    elseif nargin == 1
        choice_save = varargin{1};
        T = input('What is the time horizon?');
        weight = input('What is the weighting vector?');
        choice_P = menu('Choose a transmit power transition matrix?','Yes','No');
        Prob = [];
        fname = [];
        if choice_P == 1
           [fname, path] = uigetfile('*.json');
           fname = [path, fname];
           ret = loadjson(fname);
           Prob = ret.Probability_Transition_Matrix;
           Prob = reshape(Prob.', 5, 5);
        end
    elseif nargin == 2
        choice_save = varargin{1};
        T = varargin{2};
        weight = input('What is the weighting vector?');
        choice_P = menu('Choose a transmit power transition matrix?','Yes','No');
        Prob = [];
        fname = [];
        if choice_P == 1
           [fname, path] = uigetfile('*.json');
           fname = [path, fname];
           ret = loadjson(fname);
           Prob = ret.Probability_Transition_Matrix;
           Prob = reshape(Prob.', 5, 5);
        end
    elseif nargin == 3
        choice_save = varargin{1};
        T = varargin{2};
        weight = varargin{3};
        choice_P = menu('Choose a transmit power transition matrix?','Yes','No');
        Prob = [];
        fname = [];
        if choice_P == 1
           [fname, path] = uigetfile('*.json');
           fname = [path, fname];
           ret = loadjson(fname);
           Prob = ret.Probability_Transition_Matrix;
           Prob = reshape(Prob.', 5, 5);
        end
    elseif nargin == 4
        choice_save = varargin{1};
        T = varargin{2};
        weight = varargin{3};
        fname = varargin{4};
        ret = loadjson(fname);
        Prob = ret.Probability_Transition_Matrix;
        Prob = reshape(Prob.', 5, 5);
    end
    disp('MDP-III model for DPD');
    cprintf('*red','T = %d\n', T);
    load('../results/result_QPSK_MDPII.mat');
    load('Power.mat');
    load('Throughput.mat');
    %Reorganize the results
    ACPR = zeros(5,5,5);
    ACPR_noDPD = zeros(5,5,5);
    for ii = 1 : 1 : 5
      for jj = 1 : 1 : 5
        for zz = 1 : 1 : 5
            ACPR(ii,jj,zz) = mean(res(ii,jj,zz).ACPR_DPD);
            ACPR_noDPD(ii,jj,zz) = mean(res(ii,jj,zz).ACPR_noDPD);
        end
      end
    end
    loaded.ACPR = ACPR;
    loaded.ACPR_noDPD = ACPR_noDPD;
    loaded.ACPR_full(1,:,:,:) = loaded.ACPR;
    loaded.ACPR_full(2,:,:,:) = loaded.ACPR_noDPD;
    loaded.Throughput = Throughput;
    loaded.Throughput_full(1,:) = loaded.Throughput;
    loaded.Throughput_full(2,:) = loaded.Throughput(1) * 1.2;
    loaded.Power = Power;

    % system states: 25 x 5
    % There are 25 combinations of (P,Q)
    % There are 5 TxPower settings
    % P can take values in the range of [1,5]
    % Q can take values in the range of [1,5]
    % Tx power
    % [1] Txpower = 0
    % [2] Txpower = 5
    % [3] Txpower = 10
    % [4] Txpower = 15
    % [5] Txpower = 20
    disp('Defining the state space')
    state_txpower_vec = [0 5 10 15 20];
    state_P_vec = [1 3 5 7 9];
    state_Q_vec = [1 3 5 7 9];
    State = zeros(length(state_txpower_vec) * length(state_P_vec) * length(state_Q_vec), 3);
    n = 1;
    for state_P = state_P_vec
      for state_Q = state_Q_vec
        for state_txpower = state_txpower_vec
                State(n, :) = [state_txpower state_P state_Q];
                n = n + 1;
        end
      end
    end

    % Action space has 25 configurations
    disp('Defining the action space')
    Action = zeros(length(state_P_vec) * length(state_Q_vec), 2);
    n = 1;
    for state_P = state_P_vec
      for state_Q = state_Q_vec
        Action(n, :) = [state_P state_Q];
        n = n + 1;
      end
    end
    % parameters
    delta = 0.95;    % discount factor
    % state/action combinations
    % columns: 1) value of action (DPD configurations)
    %          2) value of state (data rate + queue pop)
    size_state_space = size(State, 1);
    size_action_space = size(Action, 1);

    A = reshape(repmat((1 : 1 : size_action_space), size_state_space, 1),...
                size_state_space * size_action_space, 1);
    S = repmat((1 : 1 : size_state_space).', size_action_space, 1);
    X = [A S];

    DPD_on = 1;
    R = zeros(size_state_space, size_action_space);
    for action = 1 : 1 : size_action_space
             for state = 1 : 1 : size_state_space
                 R(state, action) = reward_mapping_mdp_II(State, Action, state, action,...
                 loaded.ACPR_full, loaded.Throughput_full, loaded.Power, weight, DPD_on);
             end
    end
    %Generate a random matrix
    P = [];
    if isempty(Prob)
        rng(0);
        Prob = abs(randn(length(state_txpower_vec), length(state_txpower_vec)));
        for ii = 1 : 1 : size(Prob, 1)
            Prob(ii, :) = Prob(ii, :) / sum(Prob(ii, :));
        end
        for action = 1 : 1 : size_action_space
            Ptmp = transitionMatrix_mdp_II(State, Action, action, Prob).';
            P = [P Ptmp];
        end
    else
        Prob = Prob.';
        for action = 1 : 1 : size_action_space
            Ptmp = transitionMatrix_mdp_II(State, Action, action, Prob).';
            P = [P Ptmp];
        end
    end
    % set up model structure
    %The reason is: P is not a valid probability transition matrix...
    % load('P_random');
    clear model
    model.T = T;
    model.name = 'MDP-III for DPD';
    model.X = X;
    model.R = R;
    model.d = delta;
    model.P = P;
    results = mdpsolve(model);
    disp('MDP-III for DPD Control Problem')
    disp('State, Optimal Control and Value')
    states_optimal = results.Xopt(:,2);
    action_optimal = results.Xopt(:,1);
    states_optimal_TxPower = State(states_optimal, 1);
    states_optimal_P = State(states_optimal, 2);
    states_optimal_Q = State(states_optimal, 3);
    action_optimal_P = Action(action_optimal, 1);
    action_optimal_Q = Action(action_optimal, 2);
%     for ii = 1 : 1 : size(states_optimal, 1)
%         fprintf('[P_state = %d, Q_state = %d, TxPower_state = %d]\t P_action = %d, Q_action = %d, Value = %.2f\n',...
%         states_optimal_P(ii), states_optimal_Q(ii), ...
%         states_optimal_TxPower(ii), action_optimal_P(ii), action_optimal_Q(ii), results.v(ii));
%     end
    %fprintf('(P = %d, Q = %d  %1i  %8.2f\n',[results.Xopt(:,[2 1]) results.v]');
    if choice_save == 1
      file2save = char(strcat('policy_T', string(model.T)));
      if isempty(fname)
            file2save = [file2save, '_seed', num2str(seed)];
      else
            [~,fname_pure] = fileparts(fname);
            file2save = [file2save, '_', fname_pure];
      end
      save(file2save,'model','results','State','Action');
    end
  end
