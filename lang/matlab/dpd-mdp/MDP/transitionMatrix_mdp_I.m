function P = transitionMatrix(State, Action, action, Prob)
action_on_off = Action(action);
N_states = size(State, 1);
if action_on_off == 1 %Turn on the DPD
    P2_left = [Prob; Prob];
    P2_right = zeros(N_states, N_states / 2);
else
    P2_left = zeros(N_states, N_states / 2);
    P2_right = [Prob; Prob];
end
P = [P2_left P2_right];
