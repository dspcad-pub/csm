function verify_policy_I(varargin)
     % this function verifies the performance of MDP-I
     % of the generated policy via simulations
     disp('Verify policies for MDP_I model')
     load('../results/result_QPSK_MDPI.mat');
     if nargin == 0
          weight = input('What is the weighting vector for MDP 1? Metric 1 - ACPR; Metric 2 - Power:');
     elseif nargin == 1
          weight = varargin{1};
     end
     % normalize the weight to have a summation of 1.0
     % For instance, if weight = [5, 5], then the normalized weight would be [0.5, 0.5]
     weight = weight / sum(weight);
     disp('Solving the MDP problem with infinite horizon');
     disp('Please select the JSON file that contains the transmit power transition matrix!')
     [fname, path] = uigetfile('*.json');
     fname = [path, fname];
     cprintf('*blue*','Using power transition matrix from %s\n', fname);

     % run MDP-I to get the optimal policy and model together with state and action spaces
     % with infinite time horizon
     [model_Tinf, results_Tinf, State, Action] = dpd_mdp_I(0, Inf, weight, fname);
     % the policy is the first two columns of Xopt (first column is state and second column is action)
     policy_Tinf = results_Tinf.Xopt(:,[1 2]);

     disp('Solving the MDP problem with finite horizon');
     % run MDP-I to get the optimal policy and model together with state and action spaces
     % with T=1 as the time horizon
     [~, results_T1, ~, ~] = dpd_mdp_I(0, 1, weight, fname);
     policy_T1= results_T1.Xopt(:,[1 2]);

     % Use the lowest P,Q configuration for the ACPR
     loaded.ACPR = squeeze(ACPR(1,1,:));
     loaded.ACPR_noDPD = squeeze(ACPR_noDPD(1,1,:));
     loaded.ACPR_full(1,:) = loaded.ACPR;
     loaded.ACPR_full(2,:) = loaded.ACPR_noDPD;

     P = model_Tinf.P;

     % Plot the state transition matrix for the state transition matrix
     figure
     imagesc(P);
     colormap('jet');
     colorbar;
     title('The State Transition Matrix')

     T = 1e4;
     state_index_mdp = 1;
     reward_acc_mdp = 0;
     reward_curve_mdp = zeros(1, T);

     state_index_mdp_T1 = 1;
     reward_acc_mdp_T1 = 0;
     reward_curve_T1 = zeros(1, T);

     state_index_always_on = 1;
     reward_acc_always_on = 0;
     reward_curve_always_on = zeros(1, T);

     state_index_always_off = 1;
     reward_acc_always_off = 0;
     reward_curve_always_off = zeros(1, T);

     state_index_threshold = 1;
     reward_acc_threshold = 0;
     reward_curve_threshold = zeros(1, T);

     size_state_space = size(State, 1);
     action_all = [];
     state_all = [];
     ACPR_MDP_output = [];
     power_MDP_output = [];

     ACPR_always_on_output = [];
     power_always_on_output = [];

     ACPR_always_off_output = [];
     power_always_off_output = [];

     ACPR_threshold_output = [];
     power_threshold_output = [];
     % run the simulations for T epoches
     % notice that we need to reset the seed for each method as we need to
     % make sure that the randsample function is using the same seed for each
     % epoch over different methods
     for i = 1 : 1 : T
         rng(i);
         if mod(i, 5e2) == 0
            cprintf('*green*','iteration %d\n', i);
         end
         action_opt = policy_Tinf(state_index_mdp, 1);
         action_all = [action_all action_opt];
         [R, ACPR_output, power_output] = reward_mapping_mdp_I(State, Action, state_index_mdp, action_opt,...
                 loaded.ACPR_full, weight);
         reward_acc_mdp = reward_acc_mdp + R;
         reward_curve_mdp(i) = reward_acc_mdp / i;
         % compute for the next state
         state_index_mdp = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_mdp));
         ACPR_MDP_output = [ACPR_MDP_output ACPR_output];
         power_MDP_output = [power_MDP_output power_output];

         state_all = [state_all state_index_mdp];

         action_opt = policy_T1(state_index_mdp_T1, 1);
         [R, ACPR_output, power_output] = reward_mapping_mdp_I(State, Action, state_index_mdp_T1, action_opt,...
                 loaded.ACPR_full, weight);
         reward_acc_mdp_T1 = reward_acc_mdp_T1 + R;
         reward_curve_mdp_T1(i) = reward_acc_mdp_T1 / i;
         % compute for the next state
         rng(i);
         state_index_mdp_T1 = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_mdp_T1));

         action_opt = 1;
         [R, ACPR_output, power_output] = reward_mapping_mdp_I(State, Action, state_index_always_on, action_opt,...
                 loaded.ACPR_full, weight);
         reward_acc_always_on = reward_acc_always_on + R;
         reward_curve_always_on(i) = reward_acc_always_on / i;
         % compute for the next state
         rng(i);
         state_index_always_on = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_always_on));

         ACPR_always_on_output = [ACPR_always_on_output ACPR_output];
         power_always_on_output = [power_always_on_output power_output];

         action_opt = 2;
         [R, ACPR_output, power_output] = reward_mapping_mdp_I(State, Action, state_index_always_off, action_opt,...
                 loaded.ACPR_full, weight);
         reward_acc_always_off = reward_acc_always_off + R;
         reward_curve_always_off(i) = reward_acc_always_off / i;
         % compute for the next state
         rng(i);
         state_index_always_off = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_always_off));

         ACPR_always_off_output = [ACPR_always_off_output ACPR_output];
         power_always_off_output = [power_always_off_output power_output];

         state_index_threshold_mod = mod(state_index_threshold, size_state_space) + 1;
         if state_index_threshold_mod <= 2
             action_opt = 2;
         else
             action_opt = 1;
         end
         R = reward_mapping_mdp_I(State, Action, state_index_threshold, action_opt,...
                 loaded.ACPR_full, weight);
         reward_acc_threshold = reward_acc_threshold + R;
         reward_curve_threshold(i) = reward_acc_threshold / i;
         % compute for the next state
         rng(i);
         state_index_threshold = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_threshold));

         ACPR_threshold_output = [ACPR_threshold_output ACPR_output];
         power_threshold_output = [power_threshold_output power_output];
     end

     % Plot the simulation results
     figure
     subplot(211)
     histogram(action_all);
     title('Distribution of action (T = +Inf)');
     subplot(212)
     histogram(state_all);
     title('Distribution of state (T = +Inf)');

     figure
     subplot(221)
     histogram(ACPR_MDP_output, 'NumBins', 20);
     title('Histogram of ACPR output with MDP');
     subplot(222)
     histogram(ACPR_always_on_output, 'NumBins', 20);
     title('Histogram of ACPR output with DPD always on');
     subplot(223)
     histogram(ACPR_always_off_output, 'NumBins', 20);
     title('Histogram of ACPR output with DPD always off');
     subplot(224)
     histogram(ACPR_threshold_output, 'NumBins', 20);
     title('Histogram of ACPR output with threshold');

     colors = lines(128);
     figure
     line_fewer_markers(1:1:T-4, reward_curve_mdp(5 : end), 10, 'Color', colors(1, :), 'LineStyle', '-', 'LineWidth', 2, 'Marker', 'o');
     hold on
     line_fewer_markers(1:1:T-4, reward_curve_mdp_T1(5 : end), 10, 'Color', colors(2, :), 'LineStyle', '--', 'LineWidth', 2, 'Marker', 's');
     hold on
     line_fewer_markers(1:1:T-4, reward_curve_always_on(5 : end), 10, 'Color', colors(3, :), 'LineStyle',':', 'LineWidth', 2, 'Marker', 'v');
     hold on
     line_fewer_markers(1:1:T-4, reward_curve_always_off(5 : end), 10, 'Color', colors(4, :), 'LineStyle', '-.', 'LineWidth', 2, 'Marker', 'd');
     hold on
     line_fewer_markers(1:1:T-4, reward_curve_threshold(5 : end), 10, 'Color', colors(5, :), 'LineStyle', '-', 'LineWidth', 2, 'Marker', 'p');
     xlabel('Reconfiguration Round');
     ylabel('Averaged Reward');
     grid on;
     legend('MDP-generated Policy (T = +Inf)',...
     'MDP-generated Policy (T = 1)',...
     'DPD always on',...
     'DPD always off',...
     'Thresholding (Turn off DPD When Tx Power <= 5 dBm)');
end
