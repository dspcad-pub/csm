function [model, results, State, Action] = dpd_mdp_I(varargin)
    % this function calculates the optimal policy, i.e., a mapping from state to action
    % for the first stage MDP (MDP-I)
    % Inputs:
    % - choice_save: whether or not saving the results to a MAT file
    % - T: the time horizon for MDP
    % - weight: the weight for the two metrics considered in MDP-I.
    %   The first dimension is ACPR and the second dimension is power consumption
    %   For instance, you can set weight = [0.5, 0.5] which means that we put a weight of 0.5
    %   on ACPR and a weight of 0.5 on the power consumption. Notice that internally the weight vector
    %   is normalized. For example, if you set weight = [5, 5], then internally it would be set to [0.5, 0.5]
    % - choice_P: whether or not to choose a transmit power transition matrix from a JSON file
    % - fname: the filename that contains the transmit power transition matrix
    % Outputs:
    % model: the solved MDP model
    % results: the complete results of the MDP model
    % For details about the format of the MDPSOLVE, please refer to https://sites.google.com/site/mdpsolve/

    if nargin == 0
        choice_save = menu('Save to file?','Yes','No');
        T = input('What is the time horizon?');
        weight = input('What is the weighting vector?');
        choice_P = menu('Choose a transmit power transition matrix?','Yes','No');
        Prob = [];
        fname = [];
        if choice_P == 1
           [fname, path] = uigetfile('*.json');
           fname = [path, fname];
           ret = loadjson(fname);
           Prob = ret.Probability_Transition_Matrix;
           Prob = reshape(Prob.', 5, 5);
        end
    elseif nargin == 1
        choice_save = varargin{1};
        T = input('What is the time horizon?');
        weight = input('What is the weighting vector?');
        choice_P = menu('Choose a transmit power transition matrix?','Yes','No');
        Prob = [];
        fname = [];
        if choice_P == 1
           [fname, path] = uigetfile('*.json');
           fname = [path, fname];
           ret = loadjson(fname);
           Prob = ret.Probability_Transition_Matrix;
           Prob = reshape(Prob.', 5, 5);
        end
    elseif nargin == 2
        choice_save = varargin{1};
        T = varargin{2};
        weight = input('What is the weighting vector?');
        choice_P = menu('Choose a transmit power transition matrix?','Yes','No');
        Prob = [];
        fname = [];
        if choice_P == 1
           [fname, path] = uigetfile('*.json');
           fname = [path, fname];
           ret = loadjson(fname);
           Prob = ret.Probability_Transition_Matrix;
           Prob = reshape(Prob.', 5, 5);
        end
    elseif nargin == 3
        choice_save = varargin{1};
        T = varargin{2};
        weight = varargin{3};
        choice_P = menu('Choose a transmit power transition matrix?','Yes','No');
        Prob = [];
        fname = [];
        if choice_P == 1
           [fname, path] = uigetfile('*.json');
           fname = [path, fname];
           ret = loadjson(fname);
           Prob = ret.Probability_Transition_Matrix;
           Prob = reshape(Prob.', 5, 5);
        end
    elseif nargin == 4
        choice_save = varargin{1};
        T = varargin{2};
        weight = varargin{3};
        fname = varargin{4};
        ret = loadjson(fname);
        Prob = ret.Probability_Transition_Matrix;
        Prob = reshape(Prob.', 5, 5);
    end
    disp('MDP-I model for DPD');
    cprintf('*red','T = %d\n', T);

    % load the file that contains the results for ACPR under different transmit powers
    % The modulation is QPSK for the simulated ACPR
    load('../results/result_QPSK_MDPI.mat');
    loaded.ACPR = squeeze(ACPR(1, 1, :)); % We use the lowest P,Q combination to get the ACPR performances for MDP-I
    loaded.ACPR_noDPD = squeeze(ACPR_noDPD(1, 1, :));
    loaded.ACPR_full(1, :) = loaded.ACPR; % the first column of loaded.ACPR_full contains the ACPR with DPD
    loaded.ACPR_full(2, :) = loaded.ACPR_noDPD; % the second column of loaded.ACPR_full contains the ACPR without DPD
    % size of the state space: 2 * 3 = 6
    % system states:
    % Tx power
    % [1] Txpower = 0
    % [2] Txpower = 5
    % [3] Txpower = 10
    % [4] Txpower = 15
    % [5] Txpower = 20
    % DPD On/Off
    % [1] DPD on
    % [2] DPD off
    disp('Defining the state space')
    % We define five levels of transmission powers
    state_txpower_vec = [0 5 10 15 20];
    % We define two levels for the DPD state space: on(1) and off(2)
    state_DPD_vec = [1 2];
    State = zeros(length(state_txpower_vec) * length(state_DPD_vec), 2);
    n = 1;
    % Create the state space matrix
    % The first column of the state space matrix is the transmission power
    % The second column of the state space matrix is the DPD on/off status
    for state_DPD_on = state_DPD_vec
        for state_txpower = state_txpower_vec
                State(n, :) = [state_txpower state_DPD_on];
                n = n + 1;
        end
    end
    % size of the action space: 1 x 2
    % [1] Turn on DPD
    % [2] Turn off DPD
    disp('Defining the action space')
    Action = [1 ; 2];
    % parameters
    delta = 0.95;    % discount factor
    % state/action combinations
    % columns: 1) value of action (DPD configurations)
    %          2) value of state (data rate + queue pop)
    size_state_space = size(State, 1);
    size_action_space = size(Action, 1);

    % Formulate the action matrix
    A = reshape(repmat((1 : 1 : size_action_space), size_state_space, 1),...
                size_state_space * size_action_space, 1);
    % Formulate the state matrix
    S = repmat((1 : 1 : size_state_space).', size_action_space, 1);
    % Concatenate the action and state matrix to formulate the action-state matrix
    X = [A S];

    % Calculate the reward matrix R
    R = zeros(size_state_space, size_action_space);
    for action = 1 : 1 : size_action_space
             for state = 1 : 1 : size_state_space
                 R(state, action) = reward_mapping_mdp_I(State, Action, state, action,...
                 loaded.ACPR_full, weight);
             end
    end

    P = [];
    if isempty(Prob)
        % Generate a random matrix for the transition matrix if Prob is not defined
        cprintf('*green*','Generating a transition matrix randomly!');
        rng(0);
        Prob = abs(randn(size_state_space / 2, size_state_space / 2));
        for ii = 1 : 1 : size(Prob, 1)
            Prob(ii, :) = Prob(ii, :) / sum(Prob(ii, :));
        end
        for action = 1 : 1 : size_action_space
            P = [P transitionMatrix_mdp_I(State, Action, action, Prob).'];
        end
    else
        % Generate the transition matrix based on the power transition matrix
        % and concatenate them into a large matrix
        Prob = Prob.';
        for action = 1 : 1 : size_action_space
            P = [P transitionMatrix_mdp_I(State, Action, action, Prob).'];
        end
    end
    % set up model structure
    clear model
    model.T = T;
    model.name = 'MDP-I for DPD';
    model.X = X;
    model.R = R;
    model.d = delta;
    model.P = P;
    results = mdpsolve(model);
    disp('MDP-I for DPD Control Problem')
    disp('State, Optimal Control and Value')
    fprintf('%1i  %1i  %8.2f\n', [results.Xopt(:,[2 1]) results.v]');
    % save the results into a file if choice_save is 1
    if choice_save == 1
      file2save = char(strcat('policy_T', string(model.T)));
      [~,fname_pure] = fileparts(fname);
      file2save = [file2save, '_', fname_pure];
      file2save = [file2save, '.mat'];
      save(file2save,'model','results','State','Action');
    end
end
