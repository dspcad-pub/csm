function [reward, ACPR_output, power_output, throughput_output] = ...
  reward_mapping_mdp_II(State, Action, state, action,...
             ACPR, Throughput, measured_power, weight, DPD_on)
% This is the reward mapping function for MDP II
% P value is the first column of a given row of Action matrix
% Q value is the second column of a given row of Action matrix
action_P = Action(action, 1);
action_Q = Action(action, 2);

% The state of transmit power is the first column of a given row of State matrix
% The state of P is the second column of a given row of State matrix
% The state of Q is the third column of a given row of State matrix
state_txpower = State(state, 1);
state_P = State(state, 2);
state_Q = State(state, 3);
power = 0;
power_reconfig = 10;
% If P or Q changes to different values, then we need to consider the reconfiguration cost
if state_P ~= action_P || state_Q ~= action_Q
    power = power + power_reconfig;
end
index_P = (state_P - 1) / 2 + 1;
index_Q = (state_Q - 1) / 2 + 1;
index_row = (index_P - 1) * 5 + index_Q - 1 + 1;
power_consumption = measured_power(index_row);
power = power + power_consumption;
if DPD_on == 1
  power_output = power;
  power_max = max(measured_power) + power_reconfig;
  power_min = eps; %Using eps here to avoid division by zero in the future
  power = (power - power_min) / (power_max - power_min); % The power is normalized
else % Using eps here to avoid division by zero in the future
  power_output = eps;
  power = eps;
end
index_txpower = state_txpower / 5 + 1;
ACPR_org = ACPR;
Throughput_org = Throughput;
% Normalize ACPR
ACPR = DPD.normalize(ACPR);
% Normalize throughput
Throughput = DPD.normalize(Throughput);
if DPD_on == 1
  % If DPD is turned on, then use the first dimension of ACPR and throughput
  acpr = ACPR(1, index_P, index_Q, index_txpower);
  ACPR_output = ACPR_org(1, index_P, index_Q, index_txpower);
  throughput = Throughput(1, index_row);
  throughput_output = Throughput_org(1, index_row);
else
  % If DPD is turned off, then use the second dimension of ACPR and throughput
  acpr = ACPR(2, index_P, index_Q, index_txpower);
  ACPR_output = ACPR_org(2, index_P, index_Q, index_txpower);
  throughput = Throughput(2, index_row);
  throughput_output = Throughput_org(2, index_row);
end
% Calculate the reward as a linear combination of the three metrics
reward = weight * [acpr, throughput, -power]';
