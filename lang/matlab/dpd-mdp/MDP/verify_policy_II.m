function verify_policy_II(varargin)
  % this function verifies the performance of the generated policy of MDP-II
  % via simulations
    disp('Verify policies for MDP_II model')
    load('../results/result_QPSK_MDPIII.mat');
    load('Power.mat');
    load('Throughput.mat');
    if nargin == 0
          weight = input('What is the weighting vector for MDP 2? Metric 1 - ACPR; Metric 2 - Throughput; Metric 3 - Power:');
    elseif nargin == 1
          weight = varargin{1};
    end
    ACPR = zeros(5,5,5);
    ACPR_noDPD = zeros(5,5,5);
    for ii = 1 : 1 : 5
      for jj = 1 : 1 : 5
        for zz = 1 : 1 : 5
            ACPR(ii,jj,zz) = mean(res(ii,jj,zz).ACPR_DPD);
            ACPR_noDPD(ii,jj,zz) = mean(res(ii,jj,zz).ACPR_noDPD);
        end
      end
    end
    loaded.ACPR = ACPR;
    loaded.ACPR_noDPD = ACPR_noDPD;
    loaded.ACPR_full(1,:,:,:) = loaded.ACPR;
    loaded.ACPR_full(2,:,:,:) = loaded.ACPR_noDPD;

    loaded.Power = Power;
    loaded.Throughput = Throughput;
    loaded.Throughput_full(1,:) = loaded.Throughput;
    loaded.Throughput_full(2,:) = loaded.Throughput(1) * 1.2;

    % normalize the weights to have a summation of 1.0
    % For instance, if weight = [5, 5], then the normalized weight would be [0.5, 0.5]
    weight = weight / sum(weight);
    disp('Solving the MDP problem with infinite horizon');
    disp('Please select the JSON file that contains the transmit power transition matrix!')
    [fname, path] = uigetfile('*.json');
    fname = [path, fname];
    cprintf('*blue*','Using power transition matrix from %s\n', fname);
    [model_Tinf, results_Tinf, State, Action] = dpd_mdp_II(0, Inf, weight, fname);
    policy_Tinf = results_Tinf.Xopt(:,[1 2]);

    disp('Solving the MDP problem with finite horizon');

    cprintf('*blue*','Using power transition matrix from %s\n', fname);
    [~, results_T1, ~, ~] = dpd_mdp_II(0, 1, weight, fname);
    policy_T1= results_T1.Xopt(:,[1 2]);

    P = model_Tinf.P;

    T = 1e4;
    state_index_mdp = 5;
    reward_acc_mdp = 0;
    reward_curve = zeros(1, T);

    state_index_T1 = 5;
    reward_acc_T1 = 0;
    reward_curve_T1 = zeros(1, T);

    state_index_fixed = 1;
    reward_acc_fixed = 0;
    reward_curve_fixed = zeros(1, T);

    state_index_fixed2 = 1;
    reward_acc_fixed2 = 0;
    reward_curve_fixed2 = zeros(1, T);

    size_state_space = size(State, 1);
    % run the simulations for T epoches
    % notice that we need to reset the seed for each method as we need to
    % make sure that the randsample function is using the same seed for each
    % epoch over different methods
    for i = 1 : 1 : T
         rng(i);
         if mod(i, 5e2) == 0
            cprintf('*green*','iteration %d\n', i);
         end
         % add current reward
         action_opt = policy_Tinf(state_index_mdp, 1);
         R = reward_mapping_mdp_III(State, Action, state_index_mdp, action_opt,...
                 loaded.ACPR_full, loaded.Throughput_full, loaded.Power, weight, 1);
         reward_acc_mdp = reward_acc_mdp + R;
         reward_curve_mdp(i) = reward_acc_mdp / i;
         % compute for the next state
         state_index_mdp = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_mdp));

         rng(i);
         action_opt = policy_T1(state_index_T1, 1);
         R = reward_mapping_mdp_III(State, Action, state_index_T1, action_opt,...
                 loaded.ACPR_full, loaded.Throughput_full, loaded.Power, weight, 1);

         reward_acc_T1 = reward_acc_T1 + R;
         reward_curve_T1(i) = reward_acc_T1 / i;
         % compute for the next state
         state_index_T1 = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_T1));

         rng(i);
         action_opt = 1;
         R = reward_mapping_mdp_III(State, Action, state_index_fixed, action_opt,...
                 loaded.ACPR_full, loaded.Throughput_full, loaded.Power, weight, 1);

         reward_acc_fixed = reward_acc_fixed + R;
         reward_curve_fixed(i) = reward_acc_fixed / i;
         % compute for the next state
         state_index_fixed = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_fixed));

         rng(i);
         action_opt = 25;
         R = reward_mapping_mdp_III(State, Action, state_index_fixed2, action_opt,...
                 loaded.ACPR_full, loaded.Throughput_full, loaded.Power, weight, 1);
         reward_acc_fixed2 = reward_acc_fixed2 + R;
         reward_curve_fixed2(i) = reward_acc_fixed2 / i;
         % compute for the next state
         state_index_fixed2 = randsample(1 : size_state_space, 1, ...
         true, P(:, (action_opt - 1) * size_state_space + state_index_fixed2));
    end

     % Plot the results. Ignore the first 4 data points as they are transient
     % responses
     colors = lines(128);
     figure
     line_fewer_markers(1:1:T-4, reward_curve_mdp(5 : end), 10, 'Color', colors(1, :), 'LineStyle', '-', 'LineWidth', 2,'Marker','o');
     hold on
     line_fewer_markers(1:1:T-4, reward_curve_T1(5 : end), 10, 'Color', colors(2, :), 'LineStyle', '--', 'LineWidth', 2,'Marker','s');
     hold on
     line_fewer_markers(1:1:T-4, reward_curve_fixed(5 : end), 10, 'Color', colors(3, :), 'LineStyle', ':', 'LineWidth', 2,'Marker','v');
     hold on
     line_fewer_markers(1:1:T-4, reward_curve_fixed2(5 : end), 10, 'Color', colors(4, :), 'LineStyle', '-.', 'LineWidth', 2,'Marker','d');
     grid on;
     xlabel('Reconfiguration Round');
     ylabel('Averaged Reward');
     legend('MDP-generated Policy (T = +Inf)','MDP-generated Policy (T = 1)',...
     'Fixed policy (P = 1, Q = 1)','Fixed policy (P = 9, Q = 9)');
end
