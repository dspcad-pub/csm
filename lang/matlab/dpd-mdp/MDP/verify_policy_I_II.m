function ret = verify_policy_I_II(varargin)
    disp('Verify policies for MDP_I + MDP_III model')
    load('../results/result_QPSK_MDPI.mat');
    if nargin == 0
      weight1 = input('What is the weighting vector for MDP 1? Metric 1 - ACPR; Metric 2 - Power:');
      weight2 = input('What is the weighting vector for MDP 2? Metric 1 - ACPR; Metric 2 - Throughput; Metric 3 - Power:');
      plot_fig_enable = input('Plot the figure?');
      [fname, path] = uigetfile('*.json');
      fname = [path, fname];
    elseif nargin == 1
      weight1 = varargin{1};
      weight2 = input('What is the weighting vector for MDP 2? Metric 1 - ACPR; Metric 2 - Throughput; Metric 3 - Power:');
      plot_fig_enable = input('Plot the figure?');
      [fname, path] = uigetfile('*.json');
      fname = [path, fname];
    elseif nargin == 2
      weight1 = varargin{1};
      weight2 = varargin{2};
      plot_fig_enable = input('Plot the figure?');
      [fname, path] = uigetfile('*.json');
      fname = [path, fname];
    elseif nargin == 3
      weight1 = varargin{1};
      weight2 = varargin{2};
      plot_fig_enable = varargin{3};
      [fname, path] = uigetfile('*.json');
      fname = [path, fname];
    elseif nargin == 4
      weight1 = varargin{1};
      weight2 = varargin{2};
      plot_fig_enable = varargin{3};
      fname = varargin{4};
    end
    weight1 = weight1 / sum(weight1);
    weight2 = weight2 / sum(weight2);
    loaded_mdp1.ACPR = squeeze(ACPR(1,1,:)); % assume that MDP-I considers the lowest DPD configuration: P = 1, Q = 1
    loaded_mdp1.ACPR_noDPD = squeeze(ACPR_noDPD(1,1,:));
    loaded_mdp1.ACPR_full(1, :) = loaded_mdp1.ACPR; % first dimension is the ACPR with DPD
    loaded_mdp1.ACPR_full(2, :) = loaded_mdp1.ACPR_noDPD; % second dimension is the ACPR without DPD

    % load the results of ACPR, throughput, power for MDP-II
    load('../results/result_QPSK_MDPII.mat');
    load('Power.mat');
    load('Throughput.mat');
    % we consider a total of 5x5x5 = 125 configurations
    ACPR = zeros(5, 5, 5);
    ACPR_noDPD = zeros(5, 5, 5);
    for ii = 1 : 1 : 5
      for jj = 1 : 1 : 5
        for zz = 1 : 1 : 5
            ACPR(ii, jj, zz) = mean(res(ii, jj, zz).ACPR_DPD);
            ACPR_noDPD(ii, jj, zz) = mean(res(ii, jj, zz).ACPR_noDPD);
        end
      end
    end

    loaded_mdp2.ACPR = ACPR;
    loaded_mdp2.ACPR_noDPD = ACPR_noDPD;
    loaded_mdp2.ACPR_full(1,:,:,:) = loaded_mdp2.ACPR; % first dimension is the ACPR with DPD
    loaded_mdp2.ACPR_full(2,:,:,:) = loaded_mdp2.ACPR_noDPD; % second dimension is the ACPR without DPD

    loaded_mdp2.Power = Power;
    loaded_mdp2.Throughput = Throughput;
    loaded_mdp2.Throughput_full(1, :) = loaded_mdp2.Throughput; % first dimension is the throughput with DPD
    loaded_mdp2.Throughput_full(2, :) = loaded_mdp2.Throughput(1) * 1.2; % second dimension is the throughput without DPD
    % assume that the throughput without DPD is about 1.2 times that of the throughput with DPD

    % run MDP-I using weight1 for the weight of ACPR
    % fname is the filepath to the JSON file that contains the transition matrix of the transmit power
    [model_MDP1_Tinf, results_MDP1_Tinf, State_MDP1, Action_MDP1] = dpd_mdp_I(0, Inf, weight1, fname);

     policy_MDP1_Tinf = results_MDP1_Tinf.Xopt(:,[1 2]);
     P1 = model_MDP1_Tinf.P;
     loaded_mdp1.State = State_MDP1;
     loaded_mdp1.Action = Action_MDP1;

     policy_MDP1_AlwaysOn = zeros(size(policy_MDP1_Tinf));
     policy_MDP1_AlwaysOn(:, 2) = policy_MDP1_Tinf(:, 2);
     policy_MDP1_AlwaysOn(:, 1) = 1;

     policy_MDP1_AlwaysOff = zeros(size(policy_MDP1_Tinf));
     policy_MDP1_AlwaysOff(:, 2) = policy_MDP1_Tinf(:, 2);
     policy_MDP1_AlwaysOff(:, 1) = 2;

     [~, results_MDP1_T1, ~, ~] = dpd_mdp_I(0, 1, weight1, fname);
     policy_MDP1_T1 = results_MDP1_T1.Xopt(:,[1 2]);

     [model_MDP2_Tinf, results_MDP2_Tinf, State_MDP2, Action_MDP2] = dpd_mdp_II(0, Inf, weight2, fname);
     policy_MDP2_Tinf = results_MDP2_Tinf.Xopt(:,[1 2]);
     P2 = model_MDP2_Tinf.P;
     loaded_mdp2.State = State_MDP2;
     loaded_mdp2.Action = Action_MDP2;

     policy_MDP2_AlwaysLowestPQ = zeros(size(policy_MDP2_Tinf));
     policy_MDP2_AlwaysLowestPQ(:, 2) = policy_MDP2_Tinf(:, 2);
     policy_MDP2_AlwaysLowestPQ(:, 1) = 1;

     policy_MDP2_AlwaysHighestPQ = zeros(size(policy_MDP2_Tinf));
     policy_MDP2_AlwaysHighestPQ(:, 2) = policy_MDP2_Tinf(:, 2);
     policy_MDP2_AlwaysHighestPQ(:, 1) = 25;

     [~, results_MDP2_T1, ~, ~] = dpd_mdp_II(0, 1, weight2, fname);
     policy_MDP2_T1 = results_MDP2_T1.Xopt(:,[1 2]);

     lambda1 = 1;
     lambda2 = 1;
     T = 1e4;
     [ret.R_avg_Tinf, ret.R1_avg_Tinf, ret.R2_avg_Tinf, ret.action_all_Tinf, ret.ACPR_all_Tinf, ret.Power_all_Tinf, ret.Throughput_all_Tinf] = ...
      RewardToPolicy(policy_MDP1_Tinf, policy_MDP2_Tinf, loaded_mdp1, loaded_mdp2, lambda1, lambda2, ...
          1, 1, T, weight1, weight2, P1);
     ret.ACPR_all_Tinf_stat = GetStat(ret.ACPR_all_Tinf);
     ret.Power_all_Tinf_stat = GetStat(ret.Power_all_Tinf);
     ret.Throughput_all_Tinf_stat = GetStat(ret.Throughput_all_Tinf);

     [ret.R_avg_T1, ret.R1_avg_T1, ret.R2_avg_T1, ret.action_all_T1, ret.ACPR_all_T1, ret.Power_all_T1, ret.Throughput_all_T1] = ...
     RewardToPolicy(policy_MDP1_T1, policy_MDP2_T1, loaded_mdp1, loaded_mdp2, lambda1, lambda2, ...
          1, 1, T, weight1, weight2, P1);
     ret.ACPR_all_T1_stat = GetStat(ret.ACPR_all_T1);
     ret.Power_all_T1_stat = GetStat(ret.Power_all_T1);
     ret.Throughput_all_T1_stat = GetStat(ret.Throughput_all_T1);

     [ret.R_avg_AlwaysOn, ret.R1_avg_AlwaysOn, ret.R2_avg_AlwaysOn, ret.action_all_AlwaysOn, ret.ACPR_all_AlwaysOn, ret.Power_all_AlwaysOn, ret.Throughput_all_AlwaysOn] = ...
     RewardToPolicy(policy_MDP1_AlwaysOn, policy_MDP2_AlwaysHighestPQ, loaded_mdp1, loaded_mdp2, lambda1, lambda2, ...
          1, 1, T, weight1, weight2, P1);
     ret.ACPR_all_AlwaysOn_stat = GetStat(ret.ACPR_all_AlwaysOn);
     ret.Power_all_AlwaysOn_stat = GetStat(ret.Power_all_AlwaysOn);
     ret.Throughput_all_AlwaysOn_stat = GetStat(ret.Throughput_all_AlwaysOn);

     [ret.R_avg_AlwaysOff, ret.R1_avg_AlwaysOff, ret.R2_avg_AlwaysOff, ret.action_all_AlwaysOff, ret.ACPR_all_AlwaysOff, ret.Power_all_AlwaysOff, ret.Throughput_all_AlwaysOff] = ...
     RewardToPolicy(policy_MDP1_AlwaysOff, policy_MDP2_AlwaysLowestPQ, loaded_mdp1, loaded_mdp2, lambda1, lambda2, ...
          1, 1, T, weight1, weight2, P1);
     ret.ACPR_all_AlwaysOff_stat = GetStat(ret.ACPR_all_AlwaysOff);
     ret.Power_all_AlwaysOff_stat = GetStat(ret.Power_all_AlwaysOff);
     ret.Throughput_all_AlwaysOff_stat = GetStat(ret.Throughput_all_AlwaysOff);

    if plot_fig_enable
         figure
         subplot(311)
         plot(ret.ACPR_all_Tinf, '-b', 'LineWidth', 6);
         hold on
         plot(ret.ACPR_all_T1, '-r', 'LineWidth', 2);
         hold on
         plot(ret.ACPR_all_AlwaysOn, '--g', 'LineWidth', 4);
         hold on
         plot(ret.ACPR_all_AlwaysOff, '.m', 'LineWidth', 1);
         hold on
         legend('MDP Generated','Maximum Reward Mapping','DPD Always On','DPD Always Off');
         title('ACPR');
         axis tight;
         grid on;

         subplot(312)
         plot(ret.Power_all_Tinf,'-b', 'LineWidth', 6);
         hold on;
         plot(ret.Power_all_T1,'-r', 'LineWidth', 2);
         hold on
         plot(ret.Power_all_AlwaysOn,'--g', 'LineWidth', 4);
         hold on
         plot(ret.Power_all_AlwaysOff,'.m', 'LineWidth', 1);
         legend('MDP Generated','Maximum Reward Mapping','DPD Always On','DPD Always Off');
         title('Power');
         axis tight;
         grid on;

         subplot(313)
         plot(ret.Throughput_all_Tinf,'-b','LineWidth',6);
         hold on;
         plot(ret.Throughput_all_T1,'-r','LineWidth',2);
         hold on
         plot(ret.Throughput_all_AlwaysOn,'--g','LineWidth',4);
         hold on
         plot(ret.Throughput_all_AlwaysOff,'.m','LineWidth',1);
         legend('MDP Generated','Maximum Reward Mapping','DPD Always On','DPD Always Off');
         title('Throughput');
         axis tight;
         grid on;

         figure
         subplot(411)
         histogram(ret.action_all_Tinf);
         subplot(412)
         histogram(ret.action_all_T1);
         subplot(413)
         histogram(ret.action_all_AlwaysOn);
         subplot(414)
         histogram(ret.action_all_AlwaysOff);

         h1 = figure;
         colors = lines(128);
         line_fewer_markers(1:1:T, ret.R_avg_Tinf, 10, 'LineStyle','-', 'Color', colors(1, :), 'LineWidth', 2, 'Marker', 'o');
         hold on;
         line_fewer_markers(1:1:T, ret.R_avg_T1, 10, 'LineStyle','--', 'Color', colors(2, :), 'LineWidth', 2, 'Marker', 's');
         hold on
         line_fewer_markers(1:1:T, ret.R_avg_AlwaysOn, 10, 'LineStyle','-.', 'Color', colors(3, :), 'LineWidth', 2, 'Marker', 'v');
         hold on
         line_fewer_markers(1:1:T, ret.R_avg_AlwaysOff, 10, 'LineStyle', ':', 'Color', colors(4, :), 'LineWidth',2, 'Marker', 'd');
         axis tight;
         grid on;
         xlabel('Reconfiguration Round');
         ylabel('Averaged Reward');
         legend('MDP-generated Policy (T = +Inf)','Maximum Reward Mapping',...
         'DPD Always On, P = 9, Q = 9','DPD Always Off, P = 1, Q = 1');

         figure
         subplot(311)
         histogram(ret.ACPR_all_Tinf);
         hold on
         histogram(ret.ACPR_all_T1);
         legend('MDP with T = +Inf','MDP with T = 1');
         title('ACPR');

         subplot(312)
         histogram(ret.Power_all_Tinf);
         hold on
         histogram(ret.Power_all_T1);
         legend('MDP with T = +Inf','MDP with T = 1');
         title('Power');

         subplot(313)
         histogram(ret.Throughput_all_Tinf);
         hold on
         histogram(ret.Throughput_all_T1);
         legend('MDP with T = +Inf','MDP with T = 1');
         title('Throughput');

         h2 = figure;
         subplot(211)
         plot(ret.R1_avg_Tinf,'-b','LineWidth',6);
         hold on
         plot(ret.R1_avg_T1,'-r','LineWidth',2);
         hold on
         plot(ret.R1_avg_AlwaysOn,'--g','LineWidth',4);
         hold on
         plot(ret.R1_avg_AlwaysOff,'.m','LineWidth',1);
         title('Reward of MDP-I');
         legend('T = Inf','T = 1','DPD Always On','DPD Always Off');
         axis tight;
         grid on;
         xlabel('Reconfiguration Round');
         ylabel('Averaged Reward');

         subplot(212)
         plot(ret.R2_avg_Tinf,'-b','LineWidth',6);
         hold on
         plot(ret.R2_avg_T1,'-r','LineWidth',2);
         hold on
         plot(ret.R2_avg_AlwaysOn,'--g','LineWidth',4);
         hold on
         plot(ret.R2_avg_AlwaysOff,'.m','LineWidth',1);
         hold on
         title('Reward of MDP-II');
         legend('T = Inf','T = 1','DPD Always On','DPD Always Off');
         axis tight;
         grid on;
         xlabel('Reconfiguration Round');
         ylabel('Averaged Reward');
     end
end

function stat = GetStat(vec)
    vec = vec(:);
    stat.avg = mean(vec);
    stat.std = std(vec);
    stat.med = median(vec);
end

function [R_avg, R1_avg, R2_avg, action_all, ACPR_all, Power_all, Throughput_all] = ...
  RewardToPolicy(policy1, policy2, loaded_mdp1, loaded_mdp2, lambda1, lambda2, ...
  state_init1, state_init2, T, weight1, weight2, P)
     state1 = state_init1;
     state2 = state_init2;
     R = 0;
     R1_sum = 0;
     R2_sum = 0;
     size_state_space = size(loaded_mdp1.State, 1);
     R_avg = zeros(1, T);
     R1_avg = zeros(1, T);
     R2_avg = zeros(1, T);
     action_all = [];
     ACPR_all = zeros(1, T);
     Power_all = zeros(1, T);
     Throughput_all = zeros(1, T);
     rng(0);
     for i = 1 : 1 : T
         if mod(i, 5e2) == 0
            cprintf('*green*','iteration %d\n', i);
         end
         action1 = policy1(state1, 1);
         action_all = [action_all action1];
         %Calculate the reward for MDP1
         [R1, ~, ~] = reward_mapping_mdp_I(loaded_mdp1.State, loaded_mdp1.Action, state1, action1,...
                 loaded_mdp1.ACPR_full, weight1);
         R1_sum = R1_sum + R1;
         R1_avg(i) = R1_sum / i;
         action2 = policy2(state2, 1);
         [R2, ACPR_all(i), Power_all(i), Throughput_all(i)] = reward_mapping_mdp_II(loaded_mdp2.State, loaded_mdp2.Action, ...
         state2, action2, loaded_mdp2.ACPR_full, loaded_mdp2.Throughput_full, loaded_mdp2.Power, weight2, action1);
         P_cur = loaded_mdp2.Action(action2, 1);
         Q_cur = loaded_mdp2.Action(action2, 2);
         TxPower_cur = loaded_mdp1.State(mod(state1 - 1, 5) + 1, 1);
         index1 = find(loaded_mdp2.State(:, 1) == TxPower_cur);
         index2 = find(loaded_mdp2.State(:, 2) == P_cur);
         index3 = find(loaded_mdp2.State(:, 3) == Q_cur);
         index = intersect(index1, index2);
         index = intersect(index, index3);
         state2 = index;
         R2_sum = R2_sum + R2;
         R2_avg(i) = R2_sum / i;
         state1 = randsample(1 : size_state_space, 1, ...
              true, P(:, (action1 - 1) * size_state_space + state1));
         R = R + lambda1 * R1 + lambda2 * R2;
         R_avg(i) = R / i;
    end
end
