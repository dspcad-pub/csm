This folder contains the code to run MDP-DPD experiment

1. The main script is main.m. It goes over all combinations of weight vectors for MDP-I and MDP-II. The results for different
combinations are saved into a hashmap with key using the following format:

    key = strcat(num2str(weight1_x), '_', num2str(weight1_y), '_', num2str(weight2_x), ...
          '_', num2str(weight2_y), '_', num2str(weight2_z));

where weight1_x is the weight for the ACPR for MDP-I, weight1_y is the weight for the power consumption for MDP-II, weight2_x is the
weight for ACPR for MDP-II, weight2_y is the weight for throughput for MDP-II, and weight2_z is the weight for power consumption for MDP-III   

The results would be saved into a MAT file with the following file name: result_yyyy-mm-dd.mat where yyyy-mm-dd is the date that you run main.m

For instance, if you run main.m on 2019-04-01, then the results would be saved into result_yyyy-mm-dd.mat

If you just want to run a single combination of the weight vectors ad-hoc, you can redefine weight1_vec, weight2_x_vec, and weight2_y_vec
as arrays with a single element for each. Notice that only under this case MATLAB would plot figures that illustrates the output of the 
simulation. In order to force the plotting, set plot_fig_enable to be 1.  

The program would ask you to select a power transition matrix (which is used to calculate the overall state transition matrix). An introduction to the files listed in the folder txpower_transition_matrix/.

2. The logics of testing trained MDP models live in the function verify_policy_I_II.m

3. The solver for MDP-I lives in dpd_mdp_I.m while the solver for MDP-II lives in dpd_mdp_II.m

4. The transition matrix generator is presented in transitionMatrix_mdp_I.m and transitionMatrix_mdp_II.m respectively

5. The logic of converting metrics to the reward function lives in reward_mapping_mdp_I.m and reward_mapping_mdp_II.m for MDP-I and MDP-II respectively.

6. The measured power consumption is saved in Power.mat while the measured throughput is saved in Throughput.mat

7. The simulation results that contain ACPR metrics are stored in result_QPSK_MDPI.mat and result_QPSK_MDPII.mat respectively

8. The measured transmit power transition matrix is saved in the folder txpower_transition_matrix. The default transmit power transition matrix is default_PTM_capture_chn11_HT20_AVW.txt.json. An introduction to the power transition matrix are given as follows:
  [1] default_PTM_capture_chn11_HT20_AVW.txt.json: captured in AVW building on channel 11 with 20MHz bandwidth
  [2] PTM_capture_chn11_HT20.txt.json: captured in an office on channel 11 with 20MHz bandwidth
  [3] PTM_capture_chn161_HT20.txt.json: captured in an office on channel 161 with 20Mhz bandwidth
  [4] PTM_capture_chn1_HT20.txt.json: captured in an office on channel 1 with 20MHz bandwidth
  [5] PTM_capture_chn1_HT20_AVW.txt.json: captured in AVW building on channel 1 with 20Mhz bandwidth
  [6] PTM_capture_chn64_HT20_AVW.txt.json: captured in AVW building on channel 64 with 20MHz bandwidth
    
  For the list of WiFi channels, please check https://en.wikipedia.org/wiki/List_of_WLAN_channels 

9. There are some other utility functions such as
    8.1 jsonlab: this folder contains the jsonlab library that is helpful to process JSON files
    8.2 @DPD: this is a private class that contains several functions for DPD. The one used in this project is the function normalize  
    8.3 mdptools and mdputils: these two folders contain the logic of MDPSOLVE
