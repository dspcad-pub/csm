function [reward, ACPR_output, power_output] = reward_mapping_mdp_I(State, Action, state, action,...
             ACPR, weight)
action_on_off = Action(action);
state_txpower = State(state, 1);
state_DPD_on = State(state, 2);
%power_consumption_all = [637.31 581.85 541.92 529.63 441.12 522.74 478.21
%   474.64 498.68 457.01 446.72 440.94 429.28 416.74 411.33 390.70];
ACPR_org = ACPR;
% Normalize the ACPR to be in the range of [0,1]
ACPR = DPD.normalize(ACPR);
power_consumption = 637.31;
% the reconfiguration power is about 1/10 of the power consumption
power_reconfig = power_consumption / 10;
power_max = power_reconfig + power_consumption;
power_min = 0;
power = 0;
if state_DPD_on ==  1
      acpr = ACPR(1, state_txpower / 5 + 1);
      ACPR_output = ACPR_org(1, state_txpower / 5 + 1);
      power = power + power_consumption;
      power_output = power;
else
      acpr = ACPR(2, state_txpower / 5 + 1);
      ACPR_output = ACPR_org(1, state_txpower / 5 + 1);
      if action_on_off == 1
          power = power + power_reconfig;
      end
      power_output = power;
end
power = (power - power_min) / (power_max - power_min);
reward = weight * [acpr, -power]';
