function P = transitionMatrix_mdp_III(State, Action, action, Prob)
  Ns = size(State, 1);
  P = zeros(Ns, Ns);
  action_P = (Action(action, 1) - 1) / 2 + 1;
  action_Q = (Action(action, 2) - 1) / 2 + 1;
  %Decode the matrix
  for row = 1 : 1 : Ns
    for col  = 1 : 1 : Ns
      txpower_index1 = mod(row - 1, 5) + 1;
      txpower_index2 = mod(col - 1, 5) + 1;
      P_Q_group_index1 = floor((row - 1) / 5) + 1;
      P_Q_group_index2 = floor((col - 1) / 5) + 1;
      [P1,Q1] = Mapping(P_Q_group_index1);
      [P2,Q2] = Mapping(P_Q_group_index2);
      if P2 == action_P && Q2 == action_Q
        P(row, col) = Prob(txpower_index1, txpower_index2);
      else
        P(row, col) = 0;
      end
    end
  end
end

function [P,Q] = Mapping(P_Q_group_index)
  P = floor((P_Q_group_index - 1) / 5) + 1;
  Q = mod(P_Q_group_index - 1, 5) + 1;
end
