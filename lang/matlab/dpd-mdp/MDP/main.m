clear
clc
close all
DPD;
% add the necessary paths
addpath('../common/');
addpath('../../../../import/MDPSolve/mdptools/');
addpath('../../../../import/MDPSolve/mdputils/');
addpath('../../../../import/');

if ~exist('loadjson')
    addpath('../../../../import/jsonlab/');
else
    disp('JSON lab installed!');
end

if ~exist('cprintf')
    disp('Please install cprintf into MATLAB!');
    disp('https://www.mathworks.com/matlabcentral/fileexchange/24093-cprintf-display-formatted-colored-text-in-the-command-window');
    return;
else
    disp('cprintf installed!');
end

if ~exist('DPD')
    disp('Please prepare the DPD class!');
    return;
else
    disp('DPD class can be detected!');
end

if ~exist('line_fewer_markers')
    disp('Please install line_fewer_markers into MATLAB!');
    disp('https://www.mathworks.com/matlabcentral/fileexchange/42560-line_fewer_markers');
    return;
else
    disp('line_fewer_markers installed!')
end
% This is the main function for the MDP-DPD simulations
% It goes over some combinations of different weights for MDP-I and MDP-II
% and save the results accordingly.
% For MDP-I, we use weight1_vec which contains two elements: the first element
% stands for the weight of ACPR and the second element stands for the weight
% of the power consumption.
% For MDP-II, we use weight2_vec which contains three elements: the first element
% stands for the weight of ACPR, the second element for the throughput, while
% the third for the power consumption

% To see the results of a specific combination of weight vectors, one can use
% the following:
% weight1_vec = 0;
% weight2_x_vec = 0.1;
% weight2_y_vec = 0.3;
% which means that we use [0,1] weight vector for MDP-I 
% and [0.1,0.3,0.6] weight vector for MDP-II
 weight1_vec = 0 : 0.1 : 1;
 weight2_x_vec = 0 : 0.1 : 1;
 weight2_y_vec = 0 : 0.1 : 1;

% Get the current time and use that as part of the file name to save
% This mainly serves the needs of avoiding saving into the same file name
% Feel free to change the saving file name
timestamp = datestr(now, 'yyyy-mm-dd');
%[fname, path] = uigetfile('*.json');
path='txpower_transition_matrix/'
fname = 'PTM_capture_chn11_HT20.txt.json';
fname = [path, fname];
% Construct a hashmap to keep all results
HashMap = containers.Map();
file2save = strcat('../results/result_', timestamp, '.mat');
% Do not plot all figures if we are running a large number of combinations
% of the weight vectors
%if length(weight1_vec) * length(weight2_x_vec) * length(weight2_y_vec) > 1
%    plot_fig_enable = 0;
%else
%    plot_fig_enable = 1;
%end
plot_fig_enable = 0;
% Here, we iterate through weight1_vec and weight2_vec
% Notice that weight1_x + weight1_y = 1.0
% and weight2_x + weight2_y + weight2_z = 1.0
for weight1_x = weight1_vec
 
    weight1_y = 1 - weight1_x;
    for weight2_x = weight2_x_vec
        for weight2_y = weight2_y_vec
            weight2_z = 1 - weight2_x - weight2_y;
            if (weight2_z < 0 || weight2_z > 1)
                continue;
            end
            s = verify_policy_I_II([weight1_x, weight1_y], [weight2_x, weight2_y, weight2_z], ...
                plot_fig_enable, fname);
            s_json = savejson('', s, 'filename', '../results/result.json');
            key = strcat(num2str(weight1_x), '_', num2str(weight1_y), '_', num2str(weight2_x), ...
            '_', num2str(weight2_y), '_', num2str(weight2_z));
            HashMap(key) = s_json;
            clc;
        end
      end
end
% The hashmap with map key as the weight vectors are saved into file2save
cprintf('*red*', 'save into %s\n', file2save);
save(file2save, 'HashMap');
