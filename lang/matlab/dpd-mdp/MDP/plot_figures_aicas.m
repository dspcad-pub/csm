clear
clc
close all
warning off

%weight1_vec = 0 : 0.1 : 1;
%weight2_x_vec = 0 : 0.1 : 1;
%weight2_y_vec = 0 : 0.1 : 1;
weight1_vec = 0;
weight2_x_vec = 0.1;
weight2_y_vec = 0.3;
cprintf('*red*', 'Please select the result file!');
[fname, path] = uigetfile('*.mat');
ret = load(fname);
mkdir('./figs/')
for weight1_x = weight1_vec
    weight1_y = 1 - weight1_x;
    for weight2_x = weight2_x_vec
        for weight2_y = weight2_y_vec
            weight2_z = 1 - weight2_x - weight2_y;
            if (weight2_z < 0 || weight2_z > 1)
                continue;
            end
            key = strcat(num2str(weight1_x),'_',num2str(weight1_y),'_'...
                ,num2str(weight2_x),'_',num2str(weight2_y),'_',num2str(weight2_z));
            res = loadjson(ret.HashMap(key));
            pause
            fig2save = strcat('./figs/', key, '.fig');
            T = length(res.ACPR_all_Tinf);
            T_max = 1e2;
            T = min(T, T_max);
            colors = lines(128);

            figure
            subplot(311);
            line_fewer_markers(1:1:T, res.ACPR_all_Tinf(1:T), 10, 'LineStyle','-', 'Color', colors(1, :), 'LineWidth', 2, 'Marker', 'o');
            hold on
            y_min = min(res.ACPR_all_Tinf(1:T));
            y_max = max(res.ACPR_all_Tinf(1:T));
            
            line_fewer_markers(1:1:T, res.ACPR_all_T1(1:T), 10, 'LineStyle','--', 'Color', colors(2, :), 'LineWidth', 2, 'Marker', 's');
            hold on
            y_min = min(min(res.ACPR_all_T1(1:T)), y_min);
            y_max = max(max(res.ACPR_all_T1(1:T)), y_max);
            
            line_fewer_markers(1:1:T, res.ACPR_all_AlwaysOn(1:T), 10, 'LineStyle','-.', 'Color', colors(3, :), 'LineWidth', 2, 'Marker', 'v');
            hold on
            y_min = min(min(res.ACPR_all_AlwaysOn(1:T)), y_min);
            y_max = max(max(res.ACPR_all_AlwaysOn(1:T)), y_max);

            line_fewer_markers(1:1:T, res.ACPR_all_AlwaysOn2(1:T), 10, 'LineStyle',':', 'Color', colors(5, :), 'LineWidth', 2, 'Marker', 'p');                  
            y_min = min(min(res.ACPR_all_AlwaysOn2(1:T)), y_min);
            y_max = max(max(res.ACPR_all_AlwaysOn2(1:T)), y_max);
            
            line_fewer_markers(1:1:T, res.ACPR_all_AlwaysOff(1:T), 10, 'LineStyle',':', 'Color', colors(4, :), 'LineWidth', 2, 'Marker', 'd');                  
            y_min = min(min(res.ACPR_all_AlwaysOff(1:T)), y_min);
            y_max = max(max(res.ACPR_all_AlwaysOff(1:T)), y_max);
            
            xlabel('Reconfiguration Round');
            ylabel('ACPR (dBc)');
            axis tight;
            grid on;
            ylim([y_min * 0.6, y_max * 2.0]);
            set(0, 'defaultAxesFontSize', 16);
            legend_str{1} = 'MDP-generated Policy';
            legend_str{2} = 'Maximum Reward Mapping';
            legend_str{3} = 'DPD Always On, P = 9, Q = 9';
            legend_str{4} = 'DPD Always On, P = 1, Q = 1';
            legend_str{5} = 'DPD Always Off';
            leg = legend(legend_str);
            set(leg, 'FontSize', 14);
            set(leg, 'Location', 'Best');
            
            subplot(312);
            line_fewer_markers(1:1:T, res.Power_all_Tinf(1:T) + 10, 10, 'LineStyle','-', 'Color', colors(1, :), 'LineWidth', 2, 'Marker', 'o');
            hold on
            y_min = min(res.Power_all_Tinf(1:T) + 10);
            y_max = max(res.Power_all_Tinf(1:T) + 10);
            
            line_fewer_markers(1:1:T, res.Power_all_T1(1:T), 10, 'LineStyle','--', 'Color', colors(2, :), 'LineWidth', 2, 'Marker', 's');
            hold on
            y_min = min(min(res.Power_all_T1(1:T)), y_min);
            y_max = max(max(res.Power_all_T1(1:T)), y_max);
            
            line_fewer_markers(1:1:T, res.Power_all_AlwaysOn(1:T), 10, 'LineStyle','-.', 'Color', colors(3, :), 'LineWidth', 2, 'Marker', 'v');
            hold on
            y_min = min(min(res.Power_all_AlwaysOn(1:T)), y_min);
            y_max = max(max(res.Power_all_AlwaysOn(1:T)), y_max);

            line_fewer_markers(1:1:T, res.Power_all_AlwaysOn2(1:T), 10, 'LineStyle','-.', 'Color', colors(5, :), 'LineWidth', 2, 'Marker', 'p');
            hold on
            y_min = min(min(res.Power_all_AlwaysOn2(1:T)), y_min);
            y_max = max(max(res.Power_all_AlwaysOn2(1:T)), y_max);
            
            line_fewer_markers(1:1:T, res.Power_all_AlwaysOff(1:T), 10, 'LineStyle',':', 'Color', colors(4, :), 'LineWidth', 2, 'Marker', 'd');
            y_min = min(min(res.Power_all_AlwaysOff(1:T)), y_min);
            y_max = max(max(res.Power_all_AlwaysOff(1:T)), y_max);
            
            xlabel('Reconfiguration Round');
            ylabel('Power (mW)');
            axis tight;
            grid on;
            ylim([y_min * 0.8, y_max * 1.2]);
            set(0,'defaultAxesFontSize', 16)
            
            subplot(313);
            line_fewer_markers(1:1:T, res.Throughput_all_Tinf(1:T), 10, 'LineStyle','-', 'Color', colors(1, :), 'LineWidth', 2, 'Marker', 'o');
            hold on
            y_min = min(res.Throughput_all_Tinf(1:T));
            y_max = max(res.Throughput_all_Tinf(1:T));
            
            line_fewer_markers(1:1:T, res.Throughput_all_T1(1:T), 10, 'LineStyle','--', 'Color', colors(2, :), 'LineWidth', 2, 'Marker', 's');
            hold on
            y_min = min(min(res.Throughput_all_T1(1:T)), y_min);
            y_max = max(max(res.Throughput_all_T1(1:T)), y_max);
            
            line_fewer_markers(1:1:T, res.Throughput_all_AlwaysOn(1:T), 10, 'LineStyle','-.', 'Color', colors(3, :), 'LineWidth', 2, 'Marker', 'v');
            hold on
            y_min = min(min(res.Throughput_all_AlwaysOn(1:T)), y_min);
            y_max = max(max(res.Throughput_all_AlwaysOn(1:T)), y_max);

            line_fewer_markers(1:1:T, res.Throughput_all_AlwaysOn2(1:T), 10, 'LineStyle','-.', 'Color', colors(5, :), 'LineWidth', 2, 'Marker', 'p');
            hold on
            y_min = min(min(res.Throughput_all_AlwaysOn2(1:T)), y_min);
            y_max = max(max(res.Throughput_all_AlwaysOn2(1:T)), y_max);
            
            line_fewer_markers(1:1:T, res.Throughput_all_AlwaysOff(1:T), 10, 'LineStyle',':', 'Color', colors(4, :), 'LineWidth', 2, 'Marker', 'd');
            y_min = min(min(res.Throughput_all_AlwaysOff(1:T)), y_min);
            y_max = max(max(res.Throughput_all_AlwaysOff(1:T)), y_max);
            
            xlabel('Reconfiguration Round');
            ylabel('Throughput (Msps)');
            axis tight;
            grid on;
            ylim([y_min * 0.8, y_max * 1.2]);
            set(0,'defaultAxesFontSize', 16)

            savefig(fig2save);
            pause;
            close all;
           
        end
    end
end
