classdef DPD
    methods (Static)
        % This function converts signals into the dB scale
        function y = PowerdB(x)
            y = 10 * log10(x);
        end
        
        function y = normalize(x)
            x_vec = x(:);
            x_max = max(x_vec);
            x_min = min(x_vec);
            y = (x - x_min) / (x_max - x_min);
        end
        
        function PlotComplexityVersusKernelSpeed(xvec, yvec, MarkerStyle)
            xvec_uniq = unique(xvec);
            xvec_uniq = sort(xvec_uniq, 'ascend');
            throughput = [];
            for ii = xvec_uniq(:).'
                index = find(xvec == ii);
                throughput = [throughput mean(yvec(index))];
            end
            plot(xvec_uniq, throughput, '-', 'LineWidth', 2, 'MarkerSize', 10, 'Marker', MarkerStyle);
            xlabel('Complexity');
            ylabel('Averaged Kernel Throughput (Msps)');
            axis tight;
            grid on;
        end
        
        function PlotComplexityVersusKernelSpeedx2(xvec1, yvec1, xvec2, yvec2) 
            figure;
            DPD.PlotComplexityVersusKernelSpeed(xvec1, yvec1, 'o');
            hold on
            DPD.PlotComplexityVersusKernelSpeed(xvec2, yvec2, 's');
            legend('NVIDIA Geforce 1080', 'NVIDIA Geforce 680');
        end
        % This function calculates the ACLR based on signal X
        function ACLR = CalculateACLR(X, BW, SystemFs, varargin)
            if nargin == 3
                plot_enable = 0;
            else
                plot_enable = varargin{1};
            end
            if BW == 1.4e6
                BW0 = 1.08e6;
            else
                BW0 = 9 / 10 * BW;
            end
            BW_AC = BW0; % bandwidth of adjacent channel
            offset1 = BW; % offset to center of the adjacent channel
            f_edges = [- BW0 / 2, BW0 / 2, offset1 - BW_AC / 2, offset1 + BW_AC / 2, ...
             - offset1 - BW_AC / 2, - offset1 + BW_AC / 2];
            % DFT points corresponding to the edges:

            n_e = round((f_edges + SystemFs / 2) * length(X) / SystemFs + 1);
            P_DPD = abs(fftshift(fft(X))) .^ 2;
            ACLR1h = DPD.PowerdB(mean(P_DPD(n_e(1):n_e(2))) * BW0) - DPD.PowerdB(mean(P_DPD(n_e(3):n_e(4))) * BW_AC);
            ACLR1l = DPD.PowerdB(mean(P_DPD(n_e(1):n_e(2))) * BW0) - DPD.PowerdB(mean(P_DPD(n_e(5):n_e(6))) * BW_AC);
            if plot_enable
                figure
                plot(DPD.PowerdB(P_DPD));
                axis tight
                grid on;
            end
            ACLR = min(ACLR1h, ACLR1l);
        end

        % This function calculates the EVM based on the reference signal X_reference
        % and actual signal X
        function EVM_dB = CalculateEVM(X_reference, X)
            % We need to remove the PA gain when calculating the NMSE_dB performance
            Ref_energy = norm(X_reference);
            X_energy = norm(X);
            scale = Ref_energy / X_energy;
            X = X * scale;

            % Use sliding window to calculate the optimal delay bewteen
            % X and X_reference

            if length(X_reference) < length(X)
                X_short = X_reference(:);
                X_long = X(:);
            else
                X_short = X(:);
                X_long = X_reference(:);
            end

            W_len = length(X_short);

            num_windows = length(X_long) - length(X_short) + 1;

            EVM_den = mean(abs(X_reference) .^ 2);
            EVM_min = 100;
            for w = 1 : 1 : num_windows
                error_vector = X_long(w : w + W_len - 1) - X_short;
                error_vector_magnitude = abs(error_vector);
                error_vector_energy = error_vector_magnitude .^ 2;
                EVM_num = mean(error_vector_energy);
                EVM = 10 * log10(EVM_num / EVM_den);
                if EVM < EVM_min
                    EVM_min = EVM;
                end
            end
            EVM_dB = EVM_min;
        end

        function PHI = basisMatrix(IN, P, Mph, poly_type)
            Mmax = max(Mph); % length of the longest filter
            param_tot = sum(Mph); % total number of parameters
            N = length(IN);

            % Create the "elementary" basis functions:
            X = zeros(N, (P + 1) / 2);
            switch poly_type
                case 'orth'
                    [X, ~, ~] = OrthoPolyUniform(IN, P, 'odd'); % Orthogonal polynomials for
                    % uniformly distributed signals with abs(IN)<=1
                    X = X(1:N, :);
                otherwise
                    % Conventional polynomials (monomials):
                    for k = 1:2:P
                        X(:, (k + 1) / 2) = IN .* abs(IN) .^ (k - 1);
                end
            end

            % Build the polynomial basis matrix PHI from X:
            PHI = zeros(N + Mmax - 1, param_tot);
            for Ip = 1:length(Mph)
                for ll = 1:Mph(Ip)
                    PHI(ll:N + ll - 1, sum(Mph(1:Ip - 1)) + ll) = X(:, Ip);
                end
            end
        end

        function [h_est] = estimateAPH(IN, OUT, P, Q, Mp, Mq, lo_est, N, poly_type)
            % Usage: [h_est]=estimateAPH(IN,OUT,P,Mph,N)
            % Estimate parameters of an Augmented Parallel Hammerstein (APH) nonlinear
            % system with polynomial nonlinearities and FIR filters.
            % INPUTS:
            %    IN   -  input signal to the APH nonlinearity
            %    OUT  -  output signal of the APH nonlinearity ("reference signal")
            %    P    -  polynomial order of the non-conjugate branch (it is assumed
            %            that only odd orders are used)
            %    Q    -  polynomial order of the conjugate branch
            %    Mp   -  filter lengths of the non-conjugate branch FIR filters (vector with (P+1)/2
            %             elements)
            %    Mq   -  filter lengths of the conjugate branch FIR filters (vector with (Q+1)/2
            %             elements)
            %    lo_est - flag which indicates whether LO leakage compensator coefficient
            %             is estimated (1 == estimated; 0 == not estimated)
            %    N    -  estimation block length (default value=min(length(IN),length(REF)))
            %    poly_type - polynomial type: either 'conv' for the conventional monomial
            %                form, or 'orth', which uses a class of orthogonal polynomials
            %                designed for uniformly distributed data
            % OUTPUTS:
            %    h_est -  estimated APH parameters
            %
            % Lauri Anttila / TUT, April 2013
            IN = IN(:);
            OUT = OUT(:);
            Mp = Mp(:);
            Mq = Mq(:);
            Mmax = max([Mp; Mq]); % length of the longest PD filter

            % % SYSTEM IDENTIFICATION
            PHI = basisMatrix(IN(1:N), P, Mp, poly_type); % build polynomial basis matrices
            R_win = PHI(Mmax:N, :); % Covariance windowing method
            if sum(Mq)
                CPHI = basisMatrix(conj(IN(1:N)), Q, Mq, poly_type);
                CPHI_win = CPHI(Mmax:N, :); % Covariance windowing method
                R_win = [R_win CPHI_win];
            end
            R_win = [R_win ones(length(R_win), lo_est)]; % append with a column of all ones
            % to account for LO leakage
            z = OUT(Mmax:N); % Covariance windowing method
            h_est = R_win \ z; % Least Squares estimation
        end

        function [PA_Out, ScalingGain] = PA(PA_InputSignal, PolyCoeffs, Pout_dBm, G_PA, scaling)
            WienerFilter_B = [1; 0.2];
            WienerFilter_A = [1; - 0.1];
            % figure
            % freqz(WienerFilter_B, WienerFilter_A, 1024)
            PAinFiltered = filter(WienerFilter_B, WienerFilter_A, PA_InputSignal);
            % If scaling==1, scale the PA input signal to meet the desired PA output
            if scaling
                SignalPwrdB = 10 * log10(mean(abs(PAinFiltered) .^ 2));
                Pin_dB = Pout_dBm - G_PA;
                ScalingGaindB = Pin_dB - SignalPwrdB;
                ScalingGain = 10 ^ (ScalingGaindB / 20);
            else
                ScalingGain = 1;
            end
            PAinFiltered = PAinFiltered * ScalingGain;

            PolyOrder = 2 * length(PolyCoeffs) - 1;

            FilteredBasisFunctions = zeros(length(PAinFiltered), (PolyOrder + 1) / 2);
            for k = 1:2:PolyOrder
                FilteredBasisFunctions(:, (k + 1) / 2) = PAinFiltered .* abs(PAinFiltered) .^ (k - 1);
            end
            PA_Out = FilteredBasisFunctions * PolyCoeffs;
        end

        function [PA_Out, ScalingGain] = PA_Saturate(PA_InputSignal, Pout_dBm, G_PA, scaling)
            load('PA_coeffs.mat'); % load the polynomial PA coefficients
            in_max = 2.9908; % maximum input (saturation) amplitude of the polynomial PA model
            out_max = 1.99909704; % maximum output (saturation) amplitude of the polynomial PA model

            WienerFilter_B = [1; 0; 0.11]; % [1;0;0.03];  %
            WienerFilter_A = [1; - 0.07]; % [1;-0.015];  %
            A = freqz(WienerFilter_B, WienerFilter_A); % LA 3.11.2015

            PAinFiltered = filter(WienerFilter_B, WienerFilter_A, PA_InputSignal);
            PAinFiltered = 1 / sqrt(mean(abs(A(1:round(length(A) / 5))) .^ 2)) * PAinFiltered; % remove filter gain, assuming about 5-times oversampling for the input signal % LA 3.11.2015

            % HOW TO DO THE SCALING, NOW THAT PA MODEL HAS (ALMOST) UNITY GAIN, AND
            % ACTUAL GAIN IS IMPLEMENTED AFTER THE MODEL?!?

            % If scaling==1, scale the PA input signal to meet the desired PA output
            if scaling
                %     SignalPwrdB = 10*log10(mean(abs(PAinFiltered).^2));
                %     Pin_dB = Pout_dBm - G_PA;
                %     ScalingGaindB = Pin_dB - SignalPwrdB;
                %     ScalingGain = 10^(ScalingGaindB/20);
                SignalPwrdB = 10 * log10(mean(abs(PAinFiltered) .^ 2));
                Pin_dB = Pout_dBm - G_PA - 2;
                ScalingGaindB = Pin_dB - SignalPwrdB;
                ScalingGain = 10 ^ (ScalingGaindB / 20);
            else
                ScalingGain = 1;
            end
            PAinFiltered = PAinFiltered * ScalingGain;

            % % PA nonlinearity
            % FilteredBasisFunctions = zeros(length(PAinFiltered),(PolyOrder+1)/2);
            % for k=1:2:PolyOrder,
            %     FilteredBasisFunctions(:,(k+1)/2) = PAinFiltered.*abs(PAinFiltered).^(k-1);
            % end
            % PA_Out = FilteredBasisFunctions*PolyCoeffs;
            PA_In = PAinFiltered(:);
            R = [PA_In PA_In .* abs(PA_In) .^ 2 PA_In .* abs(PA_In) .^ 4 PA_In .* abs(PA_In) .^ 6 PA_In .* abs(PA_In) .^ 8];
            PA_Out = R * pa_coeffs;

            PA_Out(abs(PA_In) > in_max) = out_max * exp(1i * angle(PA_In(abs(PA_In) > in_max))); % saturation
            PA_Out = 10 ^ (G_PA / 20) * PA_Out;
        end
        
        function PlotACPRUnderDifferentPowerMDPI() 
                load('result_QPSK.mat');
                tx_vec = (0:1:23);
                acpr_DPD_vec = zeros(1, length(tx_vec));
                acpr_noDPD_vec = zeros(1, length(tx_vec));
                evm_DPD_vec = zeros(1, length(tx_vec));
                evm_noDPD_vec = zeros(1, length(tx_vec));
                for ii = 1 : 1 : length(res)
                   acpr_DPD_vec(ii) = mean(res(ii).ACPR_DPD);
                   acpr_noDPD_vec(ii) = mean(res(ii).ACPR_noDPD);
                   evm_DPD_vec(ii) = mean(res(ii).EVM_DPD);
                   evm_noDPD_vec(ii) = mean(res(ii).EVM_noDPD);
                end
                figure
                subplot(211)
                plot(tx_vec, acpr_DPD_vec,'LineWidth',2);
                hold on;
                plot(tx_vec, acpr_noDPD_vec,'LineWidth',2);
                axis tight;
                grid on;
                xlabel('Tx Power (dBm)');
                ylabel('ACPR (dBc)');
                
                subplot(212)
                plot(tx_vec, evm_DPD_vec,'LineWidth',2);
                hold on
                plot(tx_vec, evm_noDPD_vec,'LineWidth',2);
                axis tight;
                grid on;
                xlabel('Tx Power (dBm)');
                ylabel('EVM (dB)');
        end
        
        function RearrangeResultMDPI()
           load('result_QPSK.mat');
           tx_vec = (0:1:23);
           tx_target = [0 5 10 15 20];
           ACPR = zeros(1, 1, length(tx_target));
           ACPR_noDPD = zeros(1, 1, length(tx_target));
           EVM = zeros(1, 1, length(tx_target));
           EVM_noDPD = zeros(1, 1, length(tx_target));
           cnt = 1;
           for tx = tx_target
                idx = find(tx_vec == tx);
                res_new = res(idx);
                ACPR(1,1,cnt) = mean(res_new.ACPR_DPD);
                EVM(1,1,cnt) = mean(res_new.EVM_DPD);
                ACPR_noDPD(1,1,cnt) = mean(res_new.ACPR_noDPD);
                EVM_noDPD(1,1,cnt) = mean(res_new.EVM_noDPD);
                cnt = cnt + 1;
           end
           save('result_QPSK_MDPI.mat', 'ACPR','EVM','ACPR_noDPD','EVM_noDPD');
        end
    end
end
