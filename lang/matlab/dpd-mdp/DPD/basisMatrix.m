function PHI = basisMatrix(IN,P,Mph,poly_type)

% Usage: PHI = basisMatrix(IN,P,Mph,poly_type)
% Generates the polynomial basis matrix to be used in estimation of the
% parameters of a Parallel Hammerstein/Memory Polynomial, or a memoryless
% complex-valued polynomial (when Mph = all ones), nonlinearity.
%
% INPUTS:
%        IN  -  input signal
%         P  -  Polynomial order
%       Mph  -  vector of filter lengths
% poly_type  -  'conv' for conventional (default), 'orth' for orthogonal
% 
% Lauri Anttila / TUT, April 2013

Mmax = max(Mph);  % length of the longest filter
param_tot = sum(Mph); % total number of parameters
N = length(IN);

% Create the "elementary" basis functions:
X = zeros(N,(P+1)/2);
switch poly_type
    case 'orth'
        [X,~,~] = OrthoPolyUniform(IN,P,'odd'); % Orthogonal polynomials for
                            % uniformly distributed signals with abs(IN)<=1
        X = X(1:N,:);
    otherwise
        % Conventional polynomials (monomials):
        for k=1:2:P,
            X(:,(k+1)/2) = IN.*abs(IN).^(k-1);
        end
end

% Build the polynomial basis matrix PHI from X:
PHI = zeros(N+Mmax-1,param_tot);
for Ip = 1:length(Mph)
    for ll = 1:Mph(Ip),
        PHI(ll:N+ll-1,sum(Mph(1:Ip-1))+ll) = X(:,Ip);
    end
end
