% Main program for simulating digital predistortion with multicarrier
% LTE downlink signals. PAPR reduction included. EVM and ACLR calculation
% implemented for the single-carrier case.
%
% Lauri Anttila, TUT / 2016-04-05; modified 2018-02-28 (different PA model)
% modified by Lin Li

function out = run_DPD(modulation, P, Q, P_FO, Q_FO, Pout_dBm, PA_model, plot_enable)
    P_branch = (P + 1) / 2;
    Q_branch = (Q + 1) / 2;
    % rng(301928572) % set random number generator seed
    %%%%%%%%%%%%%%%% Simulation Parameters %%%%%%%%%%%%%%%%
    BW = 20e6; %[10e6 -20e6 10e6]; % define carrier bandwidths; negative value indicates an empty subcarrier
    Nsym = 10;  % number of OFDM symbols to be generated
    SystemFs = 6*sum(abs(BW)); % approximately 6 times oversampling (as an example)
                               % - will be updated inside the signal generation function
                               % - higher oversampling will give more accurate final PAPR
    window = 'RC'; % rectangular ('rect'; default) or raised cosine ('RC')
    allocation = 'full';  % 'full' (default), 'rand', or 'case_x' (user-defined test case #x)
    PAPRlimit = 6.5;  % peak-to-average-power ratio target (final PAPR will be slightly higher)
    %Mod = '64QAM';  % subcarrier modulation

    Mod = modulation;
    %Pout_dBm                            =   23.1; %  % Desired Tx Output Power after the PA in dBm

    PA_GainRatioFB                      =   0.85;  % Used as an attenuation in the feedback path of the PA (typically in the range 0.8-0.9)
    Add_IQ_Imbalance                    =   1;
    Add_LO_Leakage                      =   1;

    % Baseband Equivalent LTE Signal Transmitter
    % NEW OFDM SIGNAL GENERATION WITH PAPR REDUCTION AND WINDOWING INCLUDED: /LA
    [LTE_Signal,SystemFs,center_freqs,sym0] = genMulticarrierOFDMsignal(BW,Nsym,Mod,SystemFs,window,allocation,PAPRlimit);

    PAin = LTE_Signal;

    % I/Q mismatch parameters:
    gm = 1.07; pm = 5/180*pi; % gm = 1.1; pm = 10/180*pi; %
    K1 = 0.5*(1+gm*exp(1i*pm)); K2 = 0.5*(1-gm*exp(1i*pm));
    % scale the mismatch parameters so that signal power is unchanged
    scIQ = 1/sqrt(abs(K1)^(2)+abs(K2)^(2));
    K1 = scIQ*K1; K2 = scIQ*K2;

    % LO leakage parameter:
    lo = 1+1.234i;
    lo = lo/abs(lo); % normalized
    P_LO = -10; % power of LO leakage signal relative to signal power (in [dB])

    % IQ modulator output signal:
    LO = sqrt(10^(P_LO/10)*mean(abs(PAin).^2))*lo; % LO leakage
    if Add_IQ_Imbalance
        PAin = K1*PAin + K2*conj(PAin);
    end
    if Add_LO_Leakage
        PAin = PAin + LO;
    end

    % Wiener Power Amplifier Model
    PolyOrder   = 5;        % PA polynomial order
    G_PA        = 20;       % PA gain in dB
    IIP3        = 17;       % PA IIP3 in dBm
    P_1dBOut_dB = 26.5;     % PA output 1-dB compression point in dBm
    PolyCoeffs = PA_poly_parameters(G_PA,IIP3,P_1dBOut_dB,PolyOrder);
    if PA_model == 2
        [PA_OutputSignal, ScalingGain] = DPD.PA_Saturate(PAin,Pout_dBm,G_PA,1);
    elseif PA_model == 1
        [PA_OutputSignal, ScalingGain] = DPD.PA(PAin, PolyCoeffs, Pout_dBm,G_PA,1);
    end

    % PA_InputSignal = LTE_Signal*ScalingGain;
    PA_InputSignal = LTE_Signal;

    % Plot PA input and output spectra
    NoiseLevel = -61;  % in [dBm/MHz]
    Noise = sqrt(1/2*10^(NoiseLevel/10))*sqrt(SystemFs/1e6)*(randn(size(PA_OutputSignal)) + 1i*randn(size(PA_OutputSignal)));
    % Noise = sqrt(1/2*10^(NoiseLevel/10))*sqrt(SystemFs/1e6)*Noise;

    if plot_enable
        figure(1);
        plotSpecs = {'b','LineWidth',2};
        plotPSD(PA_InputSignal,SystemFs,1e6,plotSpecs); % plot_freqdomain(PA_InputSignal,SystemFs,'','b',UpsamplingFactor);
        hold on;
        plotSpecs = {'r','LineWidth',2};
        plotPSD(PA_OutputSignal+Noise,SystemFs,1e6,plotSpecs); %plot_freqdomain(PA_OutputSignal,SystemFs,'','r',UpsamplingFactor);
        grid on;
        legend('PA Input','PA Output');
        AX = axis;
        axis([-SystemFs/2/1e6 SystemFs/2/1e6 AX(3:4)])

        % Plot PA AM/AM responses
        figure(2);
        plot(abs(PA_InputSignal),abs(PA_OutputSignal),'.');
        title('AM/AM Response');
    end

    % ILA DPD Paramters
    poly_type = 'conv';
    NumIterations = 3;
    PH_FilterLenghts_M_1 = P_FO * ones(1, P_branch);
    PH_FilterLenghts_C_1 = Q_FO * ones(1, Q_branch);  % model filter lengths for the PH conjugate branch
    %PH_FilterLenghts_M_1 = [5 4 3 3 2]; % model filter lengths for the PH main branch
    %PH_FilterLenghts_C_1 = [3 1];   % model filter lengths for the PH conjugate branch
    P_M_1 = length(PH_FilterLenghts_M_1) * 2 - 1;  % model order for the PH main branch
    P_C_1 = length(PH_FilterLenghts_C_1) *2  - 1;  % model order for the PH conjugate branch
    BlockSize = 1e4;

    PA_gain = mean(abs(PA_OutputSignal).^2)/mean(abs(PA_InputSignal).^2);
    DPD_Estimator_In1 = PA_OutputSignal(1:BlockSize)/(PA_GainRatioFB*sqrt(PA_gain));
    DPDout1 = PA_InputSignal(1:BlockSize);
    for Iteration = 1:NumIterations

        % DPD estimation:
        dpd_est1 = estimateAPH(DPD_Estimator_In1,DPDout1,P_M_1,P_C_1,PH_FilterLenghts_M_1,PH_FilterLenghts_C_1,Add_LO_Leakage,BlockSize,poly_type);

        % Predistort a new block of data:
        DPDin = PA_InputSignal((Iteration)*BlockSize+1:(Iteration+1)*BlockSize);
        DPDout1 = predistorterAPH(DPDin,dpd_est1,P_M_1,P_C_1,PH_FilterLenghts_M_1,PH_FilterLenghts_C_1,Add_LO_Leakage,poly_type);

        PAin1 = ScalingGain*DPDout1;

        if Add_IQ_Imbalance
            PAin1 = K1*PAin1 + K2*conj(PAin1);
        end
        if Add_LO_Leakage
            PAin1 = PAin1 + LO;
        end

        if PA_model == 2
            [PA_OutputSignal_DPD] = DPD.PA_Saturate(PAin1,Pout_dBm,G_PA,0);
        else
            [PA_OutputSignal_DPD] = DPD.PA(PAin1,PolyCoeffs,Pout_dBm,G_PA,0);
        end

        DPD_Estimator_In1 = PA_OutputSignal_DPD/(PA_GainRatioFB*sqrt(PA_gain));

    end


    %% Verify DPD operation with a new signal
    out.dpd_est = dpd_est1;
    [DPDin,SystemFs,center_freqs,sym,subc_loc] = genMulticarrierOFDMsignal(BW,Nsym,Mod,SystemFs,window,allocation,PAPRlimit);
    if PA_model == 2
            [PA_out] = DPD.PA_Saturate(DPDin,Pout_dBm,G_PA,0);
    else
            [PA_out] = DPD.PA(DPDin,PolyCoeffs,Pout_dBm,G_PA,0);
    end
    DPDout = predistorterAPH(DPDin,dpd_est1,P_M_1,P_C_1,PH_FilterLenghts_M_1,PH_FilterLenghts_C_1,Add_LO_Leakage,poly_type);
    PAin = ScalingGain*DPDout;
    if Add_IQ_Imbalance
        PAin = K1*PAin + K2*conj(PAin);
    end
    if Add_LO_Leakage
        PAin = PAin + LO;
    end
    if PA_model == 2
        [PA_OutputSignal_DPD] = DPD.PA_Saturate(PAin,Pout_dBm,G_PA,0);
    else
        [PA_OutputSignal_DPD] = DPD.PA(PAin,PolyCoeffs,Pout_dBm,G_PA,0);
    end

    if plot_enable
        figure(3);
        plotSpecs = {'b','LineWidth',2};
        plotPSD(PA_OutputSignal+Noise(1:length(PA_OutputSignal)),SystemFs,1e6,plotSpecs); %plot_freqdomain(PA_OutputSignal,SystemFs,'','b',UpsamplingFactor);
        hold on;
        plotSpecs = {'r','LineWidth',2};
        plotPSD(PA_OutputSignal_DPD(1:length(Noise))+Noise,SystemFs,1e6,plotSpecs); %plot_freqdomain(PA_OutputSignal_DPD,SystemFs,'','r',UpsamplingFactor);
        grid on;
        legend('No DPD','With DPD');
        AX = axis;
        axis([-SystemFs/2/1e6 SystemFs/2/1e6 AX(3:4)])

        % Plot PA AM/AM response
        figure(2); hold on;
        plot(abs(DPDin.'),abs(PA_OutputSignal_DPD(1:length(DPDin))),'r.');
        title('AM/AM Responses');
    end

    %% ACLR1 measurement
    if length(BW)==1 % E-UTRA single carrier case
        %% Define band edges
        % Calculate actual occupied bandwidth
        out.ACLR1_DPD = DPD.CalculateACLR(PA_OutputSignal_DPD, BW, SystemFs, 0);
        out.ACLR1_noDPD = DPD.CalculateACLR(PA_OutputSignal, BW, SystemFs, 0);
    else
        % Continue here...
    end

    %% EVM measurement
    % Remove DC offset
    PA_OutputSignal = PA_OutputSignal - mean(PA_OutputSignal);
    PA_OutputSignal_DPD = PA_OutputSignal_DPD - mean(PA_OutputSignal_DPD);

    % Channel estimation/Equalizer parameters
    Nch = 3; % estimated number of channel taps
    S = 4; % sparsity parameter
    Lest = 1e4; % Estimation block size
    par20 = getLTEparameters(20e6); % fetch LTE parameters for a 20 MHz carrier
%     Rc = convmtx(DPDout(:),Nch*S);
%     Rc = Rc(1:Lest,1:S:end);
%     estim = pinv(Rc)*PA_OutputSignal_DPD(1:Lest);
%     chanEst = zeros(Nch*S-(S-1),1);
%     chanEst(1:S:end) = estim;
%     ChEst = fft(chanEst,SystemFs/par20.Fs*par20.Nfft);
%     EQ_noDPD = 1./ChEst;

    % Calculate frequency domain equalizer for the case WITHOUT DPD:
    Rc = convmtx(DPDin(:),Nch*S);
    Rc = Rc(1:Lest,1:S:end);
    estim = pinv(Rc)*PA_out(1:Lest);
    chanEst = zeros(Nch*S-(S-1),1);
    chanEst(1:S:end) = estim;
    ChEst = fft(chanEst,SystemFs/par20.Fs*par20.Nfft);
    EQ_noDPD = 1./ChEst;
    
    % Calculate frequency domain equalizer WITH DPD:
    Rc = convmtx(DPDin(:),Nch*S);
    Rc = Rc(1:Lest,1:S:end);
    estim = pinv(Rc)*PA_OutputSignal_DPD(1:Lest);
    chanEst = zeros(Nch*S-(S-1),1);
    chanEst(1:S:end) = estim;
    ChEst = fft(chanEst,SystemFs/par20.Fs*par20.Nfft);
    EQ_DPD = 1./ChEst;

    if length(BW)==1

        % Without DPD
        sym0 = sym0{1}; % Original symbols without DPD
        DEM_noDPD = demOFDMsignal(PA_OutputSignal,sym0,BW,SystemFs,subc_loc,EQ_noDPD); % Demodulate
        out.EVM_noDPD = 20 * log10(sqrt(mean(abs(DEM_noDPD(:)-sym0(:)).^2)/mean(abs(sym0(:)).^2)));

        % With DPD
        sym1 = sym{1}; % Original symbols with DPD
        DEM_DPD = demOFDMsignal(PA_OutputSignal_DPD,sym1,BW,SystemFs,subc_loc,EQ_DPD); % Demodulate
        out.EVM_DPD = 20 * log10(sqrt(mean(abs(DEM_DPD(:)-sym1(:)).^2)/mean(abs(sym1(:)).^2)));

        % scatter plots:
        if plot_enable
            figure,plot(DEM_noDPD(:),'k.')
            hold on, plot(DEM_DPD(:),'.')
            plot(sym1(:),'r.')
            title('Scatter plots')
            legend('No DPD','With DPD','Ideal')
        end
    else
        % Continue here ...

        % How to handle equalization in the case of multiple carriers?

    end
end
