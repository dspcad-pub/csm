function [h_est]=estimateAPH(IN,OUT,P,Q,Mp,Mq,lo_est,N,poly_type)

% Usage: [h_est]=estimateAPH(IN,OUT,P,Mph,N)
% Estimate parameters of an Augmented Parallel Hammerstein (APH) nonlinear 
% system with polynomial nonlinearities and FIR filters.
% INPUTS:
%    IN   -  input signal to the APH nonlinearity
%    OUT  -  output signal of the APH nonlinearity ("reference signal")
%    P    -  polynomial order of the non-conjugate branch (it is assumed 
%            that only odd orders are used)
%    Q    -  polynomial order of the conjugate branch
%    Mp   -  filter lengths of the non-conjugate branch FIR filters (vector with (P+1)/2
%             elements)
%    Mq   -  filter lengths of the conjugate branch FIR filters (vector with (Q+1)/2
%             elements)
%    lo_est - flag which indicates whether LO leakage compensator coefficient 
%             is estimated (1 == estimated; 0 == not estimated)
%    N    -  estimation block length (default value=min(length(IN),length(REF)))
%    poly_type - polynomial type: either 'conv' for the conventional monomial
%                form, or 'orth', which uses a class of orthogonal polynomials 
%                designed for uniformly distributed data
% OUTPUTS:
%    h_est -  estimated APH parameters 
%
% Lauri Anttila / TUT, April 2013
IN  = IN(:);
OUT = OUT(:);
Mp = Mp(:);
Mq = Mq(:);
Mmax = max([Mp;Mq]);  % length of the longest PD filter

%% SYSTEM IDENTIFICATION
PHI = basisMatrix(IN(1:N),P,Mp,poly_type); % build polynomial basis matrices
R_win = PHI(Mmax:N,:);  % Covariance windowing method
if sum(Mq)
    CPHI = basisMatrix(conj(IN(1:N)),Q,Mq,poly_type);
    CPHI_win = CPHI(Mmax:N,:);  % Covariance windowing method
    R_win = [R_win CPHI_win];
end
R_win = [R_win ones(length(R_win),lo_est)]; % append with a column of all ones
                                            % to account for LO leakage
z = OUT(Mmax:N);  % Covariance windowing method
h_est = R_win\z;  % Least Squares estimation
