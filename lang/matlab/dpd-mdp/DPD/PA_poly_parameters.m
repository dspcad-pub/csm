function [param] = PA_poly_parameters(G_dB,IIP3_dB,P_1dBOut_dB,PolyOrder)

% PA polynomial parameter calculation based on PA gain, IIP3, and 1-dB compression point

% G_dB = 0; % PA gain in dB; compare with alfa1 below
% IIP3 = 15; % [dBm]
iip3 = 10^(IIP3_dB/10);
% P_1dBOut_dB = 20;  % PA output 1-dB compression point; [dB]
P_1dBOut = (10^(P_1dBOut_dB/10)); 
P_1dBIn = (10^((P_1dBOut_dB-G_dB+1)/10)); 
% P_1dBIn_dB = 10*log10(P_1dBIn);

alfa1 = 10^(G_dB/20);
alfa3 = -alfa1/iip3*exp(1i*0.2986);
alfa5 = (sqrt(P_1dBOut)-alfa1*sqrt(P_1dBIn)-alfa3*sqrt(P_1dBIn)^3)/(sqrt(P_1dBIn)^5);

if PolyOrder == 5
    param = [alfa1;alfa3;alfa5];
else
    param = [alfa1;alfa3];
end
