This folder contains the code to run DPD.

The main scripts are main_DPD_MDPI.m and main_DPD_MDPII.m respectively. Run main_DPD_MDPI to generate results for MDP-I and run
main_DPD_MDPII to generate results for MDP-II.

After running the two scripts, you need to copy the generated MAT file to the folder ../MDP/.
