clear
clc
close all

P = 9; % Use a polynomial order of 9 for the P branch
Q = 9; % Use a polynomial order of 9 for the Q branch
P_FO = 5; % Use a filter order of 5
Q_FO = 5; % Use a filter order of 5

addpath('../common/');
addpath('../../../../import/MDPSolve/mdptools/');
addpath('../../../../import/MDPSolve/mdputils/');
addpath('../../../../import/');

modulation = 'QPSK'; % Use QPSK as the modulation
Pout_dBm_vec = (0 : 1 : 23); % The transmit power vector that we want to measure
PA_model = 2; % Use the new PA model
plot_enable = 0; % Plot the figure or not
ITER_MAX = 1e2; % Maximum number of iterations
file2save = ['../results/result_', modulation, 'MDPI.mat']; % the filename to
        %save into
cnt = 1;
for Pout_dBm = Pout_dBm_vec
    cprintf('Comments','Pout_dBm = %d\n', Pout_dBm);
    ACLR_noDPD_all = [];
    ACLR_DPD_all = [];
    EVM_noDPD_all = [];
    EVM_DPD_all = [];
    for iter = 1 : 1 : ITER_MAX
        if mod(iter, 10) == 0
            cprintf('Comments','Iteration %d\n', iter);
        end
        out_tmp = run_DPD(modulation, P, Q, P_FO, Q_FO, Pout_dBm, PA_model, plot_enable);
        ACLR_noDPD_all = [ACLR_noDPD_all out_tmp.ACLR1_noDPD];
        ACLR_DPD_all = [ACLR_DPD_all out_tmp.ACLR1_DPD];
        EVM_noDPD_all = [EVM_noDPD_all out_tmp.EVM_noDPD];
        EVM_DPD_all = [EVM_DPD_all out_tmp.EVM_DPD];
    end
    res(cnt).ACPR_noDPD = ACLR_noDPD_all;
    res(cnt).ACPR_DPD = ACLR_DPD_all;
    res(cnt).EVM_noDPD = EVM_noDPD_all;
    res(cnt).EVM_DPD = EVM_DPD_all;
    res(cnt).DPD_EST = out_tmp.dpd_est;
    cnt = cnt + 1;
end
save(file2save, 'res');
