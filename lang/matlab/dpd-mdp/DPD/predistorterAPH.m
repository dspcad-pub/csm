function [OUT]=predistorterAPH(IN,param,P,Q,Mp,Mq,lo_comp,poly_type)

% Usage: [OUT]=predistorterAPH(IN,param,P,Q,Mp,Mq,poly_type)
% Parallel Hammerstein (PH) predistorter with polynomial nonlinearities 
% and FIR filters.
% INPUTS:
%    IN   -  input signal to the PH predistorter
%   param -  parameters of the PH predistorter in vector form (the first Mph(1)
%             elements contain the linear filter parameters, the next Mph(2)
%             elements contain the 3rd order polynomial filter parameters, etc.)
%    P    -  polynomial order for the non-conjugate branch (it is assumed that 
%            only odd orders are used)
%    Q    -  polynomial order for the conjugate branch
%    Mp   -  filter lengths of the branch FIR filters (vector with (P+1)/2
%             elements)
%    Mq   -  filter lengths of the branch FIR filters (vector with (Q+1)/2
%             elements)
%    lo_comp - flag which indicates whether LO leakage is compensated 
%              (1 == compensated; 0 == not compensated)
%    poly_type - polynomial type: either 'conv' for the conventional monomial
%                form, or 'orth', which uses a class of orthogonal polynomials 
%                designed for uniformly distributed data
% 
% OUTPUTS:
%    OUT  -  predistorter output signal 
%
% Lauri Anttila / TUT, April 2013
IN  = IN(:);
Mp = Mp(:);
Mq = Mq(:);
Mpmax = max(Mp); Mqmax = max(Mq);
Mmax = max([Mpmax Mqmax]);  % length of the longest PD filter

%% PREDISTORTION:
PHI = basisMatrix(IN,P,Mp,poly_type); % build the polynomial basis matrix
PHI = [PHI;zeros(Mmax-Mpmax,sum(Mp))];
CPHI = basisMatrix(conj(IN),Q,Mq,poly_type); % build the polynomial basis matrix
CPHI = [CPHI;zeros(Mmax-Mqmax,sum(Mq))];
LO = ones(length(PHI),lo_comp);
OUT = [PHI CPHI LO]*param;
