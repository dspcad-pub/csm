clear
clc
close all

% add the common path which contains some utilities
addpath('../common/');

addpath('../../../../import/MDPSolve/mdptools/');
addpath('../../../../import/MDPSolve/mdputils/');
addpath('../../../../import/');
% This script generates the ACPR result for MDP-II
P_vec = [1 3 5 7 9]; % P branch polynomial orders
Q_vec = [1 3 5 7 9]; % Q branch polynomial orders
P_FO = 5; % Filter length for P branch
Q_FO = 5; % Filter length for Q branch
modulation = 'QPSK'; % Use QPSK as the modulation
Pout_dBm_vec = [0 5 10 15 20]; % Transmit power vector
%P_vec = 1;
%Q_vec = 1;
%Pout_dBm_vec = 0;
PA_model = 2; % Use the new PA model
plot_enable = 0; % Plot the figure or not
ITER_MAX = 1e2; % Total number of iterations
file2save = ['../results/result_', modulation, '_MDPII.mat'];
% The file name  to save the simulation results into
% Iterate over all combinations of P, Q, and transmit power
for ii = 1 : 1 : length(P_vec)
    for jj = 1 : 1 : length(Q_vec)
        for uu = 1 : 1 : length(Pout_dBm_vec)
              P = P_vec(ii);
              Q = Q_vec(jj);
              TxPower = Pout_dBm_vec(uu);
              cprintf('Comments','P = %d, Q = %d, Pout_dBm = %d\n', P, Q, TxPower);
              ACLR_noDPD_all = [];
              ACLR_DPD_all = [];
              EVM_noDPD_all = [];
              EVM_DPD_all = [];
              for iter = 1 : 1 : ITER_MAX
                  if mod(iter, 10) == 0
                     cprintf('Comments','Iteration %d\n', iter);
                  end
                  out_tmp = run_DPD(modulation, P, Q, P_FO, Q_FO, TxPower, PA_model, plot_enable);
                  ACLR_noDPD_all = [ACLR_noDPD_all out_tmp.ACLR1_noDPD];
                  ACLR_DPD_all = [ACLR_DPD_all out_tmp.ACLR1_DPD];
                  EVM_noDPD_all = [EVM_noDPD_all out_tmp.EVM_noDPD];
                  EVM_DPD_all = [EVM_DPD_all out_tmp.EVM_DPD];
              end
              res(ii,jj,uu).ACPR_noDPD = ACLR_noDPD_all;
              res(ii,jj,uu).ACPR_DPD = ACLR_DPD_all;
              res(ii,jj,uu).EVM_noDPD = EVM_noDPD_all;
              res(ii,jj,uu).EVM_DPD = EVM_DPD_all;
        end
    end
end
save(file2save, 'res');
