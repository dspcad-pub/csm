CMAKE_MINIMUM_REQUIRED(VERSION 2.8 FATAL_ERROR)

set(CMAKE_C_COMPILER "gcc")
set(CMAKE_CXX_COMPILER "g++")
set(CMAKE_INSTALL_PREFIX $ENV{CSMGEN})


PROJECT(CSM C CXX)

INCLUDE(CheckIncludeFile)
INCLUDE(CheckIncludeFileCXX)
INCLUDE(CheckIncludeFiles)

SET(CMAKE_EXPORT_COMPILE_COMMANDS on)
SET(CMAKE_C_STANDARD 11)
SET(CMAKE_CXX_STANDARD 14)
#SET(CMAKE_CXX_FLAGS "-std=c++1y")

INCLUDE_DIRECTORIES(

        ${PROJECT_SOURCE_DIR}/src/apps/add
        ${PROJECT_SOURCE_DIR}/src/apps/dpd

        ${PROJECT_SOURCE_DIR}/src/gems/actors/dpd

        $ENV{UXDICELANG}/c/src/util
)
ADD_SUBDIRECTORY(lang/c/src/gems/actors/router_jitter)
ADD_SUBDIRECTORY(lang/c/src/apps/router_jitter)
ADD_SUBDIRECTORY(lang/cpp/src/apps)
ADD_SUBDIRECTORY(lang/cpp/src/gems)
ADD_SUBDIRECTORY(lang/cpp/src/utils)
ADD_SUBDIRECTORY(lang/c/src/gems/routers)
ADD_SUBDIRECTORY(lang/c/src/gems/edges)

