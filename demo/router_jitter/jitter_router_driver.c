﻿/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "lide_c_fifo.h"
#include "router.h"
#include "lide_c_jitter_file_source.h"
#include "lide_c_double_file_sink.h"
#include "lide_c_router_double_file_sink.h"
#include "lide_c_FSM_s.h"
#include "lide_c_DVL.h"
#include "lide_c_STR.h"
#include "lide_c_TRT.h"
#include "lide_c_RE.h"
#include "lide_c_RRE.h"
#include "lide_c_CRE.h"
#include "lide_c_Phase.h"
#include "lide_c_util.h"
#include "lide_c_jitter_graph.h"

/* 
Usage: jitter_router_driver.exe parameter_file in_file out1_file out2_file
*/

int main(int argc, char **argv) {
    char *x_file = NULL;
    char *out1_file = NULL; 
    char *out2_file = NULL; 
    char *para_file = NULL;
	FILE *fp = NULL;
    int arg_count = 5;
    /* Check program usage. */
    if (argc != arg_count) { 
        fprintf(stderr, "lide_c_jitter_driver.exe error: arg count");
		fprintf(stderr, "\n argc is%d", argc);
        exit(1);
    }   

	int i = 1;
    /* Get filename for input and output file(s). */
    para_file = argv[i++];  	
	x_file = argv[i++];
    out1_file = argv[i++];
    out2_file = argv[i];

    lide_c_jitter_graph_context_type* jitter_graph= lide_c_jitter_graph_new( \
            x_file, out1_file, out2_file, para_file);
    lide_c_jitter_graph_scheduler(jitter_graph);

    return 0;
}
