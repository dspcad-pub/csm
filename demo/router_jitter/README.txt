This is a jitter application demo for router.[1]

Usage: To compile the demo run `makeme`
to run the demo run `runme`

It has two inputs, input.txt and parameter.txt and the result should be in
out.txt

[1]Y. Lee, Y. Liu, K. Desnos, L. Barford, and S. S. Bhattacharyya.
Passive-active
flowgraphs for efficient modeling and design of signal processing systems.
Journal of Signal Processing Systems, 92(10):1133-1151, October 2020.

