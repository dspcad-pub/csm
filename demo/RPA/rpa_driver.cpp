/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <iostream>

#include "welt_cpp_graph.h"
#include "rpa_robot_graph.h"

int main(int argc, char **argv) {

	char *x_filename;
	char *policy_filename;
	char *out_filename;

	int i = 0;
	int arg_count = 4;

	/* Check program usage. */
	if (argc != arg_count) {
		fprintf(stderr,
				"demo_count_bright_pixels.exe error: arg count\n");
		exit(1);
	}

	/* Open the input and output file(s). */
	i = 1;
	/* Open input and output files. */
    x_filename = argv[i++];
    policy_filename = argv[i++];
	out_filename = argv[i++];

	auto* demo_rpa_graph = new rpa_robot_graph(x_filename, policy_filename,
            out_filename);

	/* Execute the graph. */
    demo_rpa_graph->scheduler();

	/* Terminate graph */
	delete demo_rpa_graph;

	return 0;

}



