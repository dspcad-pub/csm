This is a simple demo for RPA(Reconfiguration Policy Actor)

Each line contains a set of policy in policy.txt.

x.txt contains constraint.

Once it meets the adequate policy set, it does not update the policy.

The out.txt file will print out the state of each invokation of robot actor
and the remaining energy levels at the moment.

Usage: To compile the demo run `makeme`
to run the demo run `runme`
