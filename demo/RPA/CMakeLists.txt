SET(CMAKE_C_STANDARD 11)
SET(CMAKE_CXX_STANDARD 14)

INCLUDE_DIRECTORIES(

        $ENV{UXWELTER}/lang/c/src/gems/actors/basic
        $ENV{UXWELTER}/lang/c/src/gems/actors/common
        $ENV{UXWELTER}/lang/c/src/gems/edges
        $ENV{UXWELTER}/lang/c/src/tools/runtime
        $ENV{UXWELTER}/lang/c/src/tools/graph

        $ENV{UXWELTER}/lang/cpp/src/apps/basic
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/common
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/basic
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/runtime
        $ENV{UXWELTER}/lang/cpp/src/utils

        $ENV{UXCSM}/lang/cpp/src/apps/rpa_robot
        $ENV{UXCSM}/lang/cpp/src/gems/actors/rpa_actors

        $ENV{UXDICELANG}/c/src/util
)

LINK_DIRECTORIES(
        $ENV{WELTERGEN}
        $ENV{CSMGEN}
)

ADD_EXECUTABLE(rpa_driver.exe
        rpa_driver.cpp
        )

TARGET_LINK_LIBRARIES(
        rpa_driver.exe

        $ENV{CSMGEN}/librpa_app.a

        $ENV{CSMGEN}/librpa_actors.a

        $ENV{WELTERGEN}/libwelt_cpp_actors_basic.a
        $ENV{WELTERGEN}/libwelt_cpp_graph_basic.a
        $ENV{WELTERGEN}/libwelt_cpp_runtime.a
        $ENV{WELTERGEN}/libwelt_cpp_utils.a
        $ENV{WELTERGEN}/libwelt_c_runtime.a


        $ENV{DLXGEN}/c/libutil.a

        $ENV{WELTERGEN}/libwelt_cpp_graph.a

        $ENV{WELTERGEN}/libwelt_c_graph_common.a
        $ENV{WELTERGEN}/libwelt_c_actors_basic.a
        $ENV{WELTERGEN}/libwelt_c_actors_common.a
        $ENV{WELTERGEN}/libwelt_cpp_actor.a
        $ENV{WELTERGEN}/libwelt_c_edges.a
        $ENV{WELTERGEN}/libwelt_c_runtime.a

)

INSTALL(TARGETS rpa_driver.exe DESTINATION .)





