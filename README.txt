
CSM Package 

CSM stands for Compact System Level Models

This package provides utilities and application demonstrations
for design and implementation of signal and information
processing systems using Compact System Level Models (CSMs).

The following references provide useful background and motivation
for the CSM concept on which this package is based:

[1] S. S. Bhattacharyya and M. C. Wolf. Research challenges for heterogeneous cyberphysical system design. IEEE Computer Magazine, 53(7):71-75, July 2020.

The base package for the csm package is lide, and the csm package also depends
on the following packages: dice and welter.  Once all of these packages have
been started up, the csm package can be started up by running:

dxloadplugin lide csm

Information about installing DSPCAD Framework Packages, including base and
plug-in pacakges is available at:

[1] https://code.umd.edu/dspcad-pub/dspcadwiki/-/wikis/software/DSPCAD-Framework-Base-Packages

[2] https://code.umd.edu/dspcad-pub/dspcadwiki/-/wikis/software/DSPCAD-Framework-Plug-In-Packages

Release created on Mon Oct 25 23:16:41 EDT 2021
